To install Robo and Robo API

Please Note -
    all value(s) are examples only and should be set to their production values, in particular HelpDeskName and HelpDeskNumber

1. extract the contents of aimsrobo.retrotest.co.za.7z to your iis application directory

2. Configure the web.config in the robo subfolder
   --------------------------------------------------
    These application keys are defined
    
    URL of the Robo.Api
    <add key="RoboAPIBaseUri" value="http://api.robo.retrotest.co.za//api" />
    
    Username to authenticate must match entries in aspuser table in RoboDB
    <add key="RoboAPIusername" value="test2@test.com" />
    
    Password to authenticate
    <add key="RoboAPIpassword" value="Test@1234" />
    
    Maximum Size of files
    <add key="MaxUploadSize" value="750" />
    
    Help desk name for error messages
    <add key="HelpDeskName" value="us" />
    
    Help desk number for error messages
    <add key="HelpDeskNumber" value="086 111 456" />
    
    modify them to suit your environment

 2. Configure the web.config in the robo.api subfolder
    --------------------------------------------------
    
    EMail address and connection details for first email
    Destination mail for where application logs are sent to 
    <add key="MailDestination" value="rolivier@retrorabbit.co.za" />
    
    Reply email address from which emails are sent
    <add key="ReplyEmail" value="virtual_advisor@absa.co.za" />

    Server address or name
    <add key="SMTPServer" value="stmp.gmail.com" />
    
    Smtp port usually 25
    <add key="SMTPPort" value="25" />
    
    Optional user name
    <add key="SMTPUser" value="example" />
    
    Optional password
    <add key="SMTPPass" value="example" />
    
    Directories for creating zip files and attachments for emails
    These folders are included in the archive and should be modified to match your environment
    
    Directory that must contain a Docs folder with these required files - 
    temporary files are written here too including dowloads from smartrand
    [Additional Investment information - WIP.PDF]
    [ts and cs.pdf]
    <add key="DocSource" value="C:\inetpub\wwwroot\aimsrobo.retrotest.co.za\Source" />

    Intermediate directory where pdfs are generated into
    <add key="DocDest" value="C:\inetpub\wwwroot\aimsrobo.retrotest.co.za\Dest" />

    Path where uncompressed documents are assembled
    <add key="DocShare" value="C:\inetpub\wwwroot\aimsrobo.retrotest.co.za\Dest" />

    Path to share where zip files with 6 char name will go to be picked up by Robo Batch service(matches source directory in robo batch copy i.e. c:\robo\src in this document)
    <add key="ShareZip" value="C:\inetpub\wwwroot\aimsrobo.retrotest.co.za\Share" />

    Set the smart rand call to true to enable smart rand document 
    <add key="SmartRand" value="true" />

    The uri to call for the smart rand enging
    <add key="SmartRandURI" value="https://smartrandengine.com/api" />
    
    The user or id in the authentication scheme used for the url
    <add key="SmartRandAPIUsername" value="8adc29ff-7d68-40c3-ab02-799e70f3d4d6"/>

    The password or secret used in the smart rand api
    <add key="SmartRandAPISecret" value="8adc29ff-7d68-40c3-ab02-799e70f3d4d6"/>
    
    Proxy server uri
    <add key="SmartRandAPIProxy" value="http://proxy:8080/url"/>

    Broker code to add to the top of generated pdf lead  report
    <add key="BrokerCode" value="VBROKER" />
    

3. Complete Data definition files
------------------------------

The complete data definition is contained in [CreateTables.sql] which will create the [RoboDB] database and tables


Batch Copies Installation
--------------------------

1. Extract BatchScheduler.7z somewhere
2. Apply [BatchCopies.sql] to RoboDB if you havent applied [CreateTables.sql] yet 
3. run setup.exe inside BatsScheduler extracted dir.
4. configure 'Robo Batch Mover' in services admin program to have valid credentials to access RoboDB
5. change app.config to suit your environment (see spp.config section)
6. start 'Robo Batch Mover'
7. tap windows key and type Event Viewer to see event viewer
8. Open 'Applications and Services Log' this might take a long time to populate
9. open 'RoboBatchNewLog' you should see 'Robo Batch Service start' if installation was successfull
10. The app.config configuration should be in the installation directory

Batch Copies app.config
-----------------------

The following configuration entries are available in app.config in the <appSettings> tag

1.  The source folder from which files are moved
    <add key="SourceFolder" value="c:\robo\src" />
2.  The destination folder to move files too
    <add key="DestinationFolder" value="c:\robo\dst" />
3.  number of files to move at a time
    <add key="NumberOfFiles" value="10" />
4.  minutes between batches
    <add key="PollingFrequencyMinutes" value="1" />
6.  comma seperate list of filetypes which are filtered, default is everything    
    <add key="FileTypes" value="zip" />
 
Leads Email Installation
------------------------

1. Extract LeadsService.7z somewhere
2. Ensure NameEmails table exists in RoboDB use [CreateTables.sql]
3. run setup.exe inside LeadsService extracted dir.
4. configure 'Robo Leads Emailer' in services admin program to have valid credentials to access RoboDB
5. start 'Robo Leads Emailer'
6. tap windows key and type Event Viewer to see event viewer
7. Open 'Applications and Services Log' this might take a long time to populate
8. open 'RoboLeadsNewLog' you should see 'ROBO Leads scheduled to run after: 0 day(s) 23 hour(s) 21 minute(s) 20 seconds(s) {0}' if installation was successfull
9. The initial app.config configuration should be in the installation directory

Leads Email app.config
----------------------

1. Destination mail where leads will be sent to
    <add key="MailDestination" value="rolivier@retrorabbit.co.za" />
2. SMTPServer required  smtp url or ip
    <add key="SMTPServer" value="smtp.gmail.com" />
3. port 25 for non secured 587 or other for secured smtp 
    <add key="SMTPPort" value="25" />
4. optional user name (used during testing)
    <add key="SMTPUser" value="example" />
5. optional password (used during testing)    
    <add key="SMTPPass" value="example" />
6. value set in from field in mails sent    
    <add key="SMTPFromMail" value="VirtualAdvisor@absa.co.za" />
7. subject line in mail sent    
    <add key="MailSubject" value="Absa Virtual Advisor Pending Application" />
8. time during the day in 24h format when mails should be sent    
    <add key ="ScheduledTime" value ="18:41"/>