﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robo.Batch.Service
{
    class Configuration
    {
        private int getIntConfig(string name, int def)
        {
            int value = def;
            int.TryParse(System.Configuration.ConfigurationManager.AppSettings[name], out value);
            return value;

        }
        private string getStringConfig(string name, string def)
        {
            string value = System.Configuration.ConfigurationManager.AppSettings[name];
            if (value == string.Empty) return def;
            return value;

        }
        public Configuration()
        {

            this.SourceFolder = getStringConfig("SourceFolder","");
            this.DestinationFolder = getStringConfig("DestinationFolder", ""); ;
            this.NumberOfFiles = getIntConfig("NumberOfFiles",10);
            this.PollingFrequencyMinutes = getIntConfig("PollingFrequencyMinutes",5);
            string FileTypes = getStringConfig("FileTypes","any");
            this.FileTypes = new List<string>(FileTypes.Split(new char[1]{ ','}));
        }
        public string SourceFolder { get; set; }
        public string DestinationFolder { get; set; }
        public int NumberOfFiles { get; set; }
        public int PollingFrequencyMinutes { get; set; }
        public List<string> FileTypes { get; set; }
}
}
