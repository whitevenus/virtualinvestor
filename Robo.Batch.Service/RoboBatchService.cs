﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Robo.Batch.Service
{
    public partial class RoboBatchService : ServiceBase
    {
        private System.Diagnostics.EventLog eventLog1;
        public RoboBatchService()
        {
            InitializeComponent();
            string source = "RoboBatchSource";
            string logName = "RoboBatchNewLog";
            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists(source))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    source, logName);
            }
            eventLog1.Source = source;
            eventLog1.Log = logName;
        }
        void PerformScheduleWork()
        {
            try
            {
                var batcher = new Batcher();
                batcher.MoveFiles();
                
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("ROBO Batch Error moving files:" + ex.Message,EventLogEntryType.Error);
            }
            
        }
        
        
        private Timer Scheduler;
        public void ScheduleService()
        {
            try
            {
                Scheduler = new Timer(new TimerCallback(SchedulerCallback));
                var config = new Configuration();
                //Set the Default Time.
                DateTime scheduledTime = DateTime.Now;
                PerformScheduleWork();
                //If Scheduled Time is passed set Schedule for the next day.
                scheduledTime = scheduledTime.AddMinutes(config.PollingFrequencyMinutes);
                
                TimeSpan timeSpan = scheduledTime.Subtract(DateTime.Now);
                string schedule = string.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);

                eventLog1.WriteEntry("ROBO Batch Service scheduled to run after: " + schedule + " {0}");

                //Get the difference in Minutes between the Scheduled and Current Time.
                int dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);                
                // Change the Timer's Due Time.
                Scheduler.Change(dueTime, Timeout.Infinite);
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("ROBO Batch Service Error on: " + ex.Message + ex.StackTrace, EventLogEntryType.Error);
                //Stop the Windows Service.
                using (System.ServiceProcess.ServiceController serviceController = new System.ServiceProcess.ServiceController("SimpleService"))
                {
                    serviceController.Stop();
                }
            }
        }

        private void SchedulerCallback(object e)
        {
            eventLog1.WriteEntry("ROBO Batch Service Schedule start" , EventLogEntryType.Information);

            
            this.ScheduleService();
        }
        protected override void OnStart(string[] args)
        {

            eventLog1.WriteEntry("ROBO Batch Service start", EventLogEntryType.Information);

            this.ScheduleService();

        }

        protected override void OnStop()
        {
            this.Scheduler.Dispose();
        }
        internal void TestStartupAndStop(string[] args)
        {
            this.OnStart(args);
            Console.ReadLine();
            this.OnStop();
        }
    }
}
