﻿using RoboData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robo.Batch.Service
{
    class Batcher
    {
        public void MoveFiles()
        {
            RoboDBEntities db = new RoboDBEntities();
            //System.Configuration.ConfigurationManager.AppSettings["MailDestination"]
            Configuration cfg = new Configuration();
            string[] fileEntries = Directory.GetFiles(cfg.SourceFolder);
            int count = cfg.NumberOfFiles;
            foreach(string file in fileEntries)
            {
                if(count == 0)
                {
                    break;
                }
                string fileName = new FileInfo(file).Name;

                if (db.BatchCopies.Where(b => b.TransferredFileName == fileName).Count() == 0)
                {
                    var batch = new BatchCopy
                    {   TransferCompletedDateTime = null
                    ,   TransferredFileName = fileName
                    ,   TransferredDestinationFolder = cfg.DestinationFolder
                    ,   TransferStartedDateTime = DateTime.Now
                    ,   TransferError = false
                    };

                    db.BatchCopies.Add(batch);
                    db.SaveChanges();
                    db.Entry(batch).State = EntityState.Modified;

                    try
                    {
                        Console.WriteLine("file move:" + file + " => " + cfg.DestinationFolder + "\\" + fileName);
                        
                        File.Move(file, cfg.DestinationFolder + "\\" + fileName);

                        batch.TransferCompletedDateTime = DateTime.Now;
                        Console.WriteLine("file moved:" + file);

                    }
                    catch (Exception e)
                    {
                        batch.TransferError = true;
                        batch.TransferErrorReason = e.Message;
                    }
                    db.SaveChanges();
                    --count;
                }else
                {
                    
                }
            }
            
            //List<BatchCopy> Leads = db.NameEmails.Where(l => l.GenerateLead == true).ToList();

        }
    }
}
