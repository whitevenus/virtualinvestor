USE [RoboDB]
GO

drop table AdditionalPassportDetails;
drop table IncomeTaxDetails;

create table AdditionalPassportDetails
(
    Id				int IDENTITY(1,1) primary key,
    PassportNumber  varchar(50) not null,
    CountryOfIssue  varchar(50) not null,
    DateOfIssue		date,
	ClientApplicationId		int not null,
    constraint fk_client_id foreign key (ClientApplicationId) references InvestmentApplications (id)
);

create table IncomeTaxDetails
(
    Id				int IDENTITY(1,1) primary key,
	IncomeTaxNumber varchar(50) NOT NULL,
    CounntryOfIssue varchar(50) NOT NULL,
	ClientApplicationId int not null,
    constraint fk_client_id2 foreign key (ClientApplicationId) references InvestmentApplications (id)
);