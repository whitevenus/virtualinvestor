USE [RoboDB]
GO

/****** Object:  Table [dbo].[InvestmentApplicationFields]    Script Date: 2016/11/16 5:18:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[InvestmentApplications] ADD
	[Income_SalaryWages] bit NULL,
	[Income_Commission] bit NULL,
	[Income_Bonus] bit NULL,
	[Income_Maintenance] bit NULL,
	[Income_Pension] bit NULL,
	[Income_Investments] bit NULL,
	[Income_InsuranceClaim] bit NULL,
	[Income_Allowance] bit NULL,
	[Income_DonationGift] bit NULL,
	[Income_Inheritance] bit NULL,
	[Income_SocialGrant] bit NULL,
	[Income_RetirementAnnuity] bit NULL,
	[Income_Other] bit NULL,
	[Income_OtherDescription] varchar(max) NULL,
	[SourceOfWealth] varchar(max) NULL;

GO