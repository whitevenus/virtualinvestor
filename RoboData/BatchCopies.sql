-- Creating table 'BatchCopies'
CREATE TABLE [dbo].[BatchCopies] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TransferredFileName] nvarchar(max)  NOT NULL,
    [TransferredDestinationFolder] nvarchar(max)  NOT NULL,
    [TransferStartedDateTime] datetime  NULL,
    [TransferCompletedDateTime] datetime  NULL,
    [TransferError] bit  NOT NULL,
    [TransferErrorReason] nvarchar(max)  NULL
);
GO

ALTER TABLE [dbo].[BatchCopies]
ADD CONSTRAINT [PK_BatchCopies]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO