USE [RoboDB]
GO

/****** Object:  Table [dbo].[InvestmentApplicationFields]    Script Date: 2016/10/21 3:18:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[NameEmail] ADD 
    [GenerateLead] [bit] NULL,
    [EmailSent] [DateTime] NULL,
    [ClientReferenceNumber] [nvarchar](6) NOT NULL