USE [RoboDB]
GO

/****** Object:  Table [dbo].[InvestmentApplicationFields]    Script Date: 2016/11/16 5:18:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[InvestmentApplications] DROP COLUMN
	[PostBoxNumber],
	[PostBoxSuburb],
	[PostBoxCode];
GO

ALTER TABLE [dbo].[InvestmentApplications] ADD
	[PostAddress] varchar(250),
	[PostPostalCode] varchar(10),
	[PostCity] varchar(50),
	[PostProvince] varchar(50);
GO