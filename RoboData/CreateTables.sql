
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/11/2017 15:02:56
-- Generated from EDMX file: C:\Users\Vuyani\Documents\ABSA-VA-NEW\absa-virtual-advisor - Copy\RoboData\RoboModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [RoboDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[fk_client_id]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AdditionalPassportDetails] DROP CONSTRAINT [fk_client_id];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[AdditionalPassportDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AdditionalPassportDetails];
GO
IF OBJECT_ID(N'[dbo].[Banks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Banks];
GO
IF OBJECT_ID(N'[dbo].[BatchCopies]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BatchCopies];
GO
IF OBJECT_ID(N'[dbo].[Configurations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Configurations];
GO
IF OBJECT_ID(N'[dbo].[Countries]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Countries];
GO
IF OBJECT_ID(N'[dbo].[IncomeTaxDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[IncomeTaxDetails];
GO
IF OBJECT_ID(N'[dbo].[InvestmentApplications]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InvestmentApplications];
GO
IF OBJECT_ID(N'[dbo].[NameEmails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NameEmails];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Configurations'
CREATE TABLE [dbo].[Configurations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [AIMSEmailAdress] nvarchar(250)  NOT NULL,
    [AIMSEmailSubjectPrefix] nvarchar(50)  NOT NULL,
    [AIMSAdvisorCode] nvarchar(10)  NOT NULL
);
GO

-- Creating table 'NameEmails'
CREATE TABLE [dbo].[NameEmails] (
    [Name] varchar(250)  NOT NULL,
    [Email] varchar(250)  NOT NULL,
    [AcceptTsCs] int  NOT NULL,
    [AcceptTsCsTimeStamp] datetime  NULL,
    [ReadInvestInfo] int  NOT NULL,
    [ReadInvestInfoTimeStamp] datetime  NULL,
    [Id] int IDENTITY(1,1) NOT NULL,
    [GenerateLead] bit  NULL,
    [EmailSent] datetime  NULL,
    [ClientReferenceNumber] nvarchar(6)  NOT NULL
);
GO

-- Creating table 'BatchCopies'
CREATE TABLE [dbo].[BatchCopies] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TransferredFileName] nvarchar(max)  NOT NULL,
    [TransferredDestinationFolder] nvarchar(max)  NOT NULL,
    [TransferStartedDateTime] datetime  NULL,
    [TransferCompletedDateTime] datetime  NULL,
    [TransferError] bit  NOT NULL,
    [TransferErrorReason] nvarchar(max)  NULL
);
GO

-- Creating table 'Countries'
CREATE TABLE [dbo].[Countries] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ISO] char(2)  NOT NULL,
    [FullName] varchar(80)  NOT NULL,
    [PrintableName] varchar(80)  NOT NULL,
    [ISO3] char(3)  NULL,
    [NumberCode] smallint  NULL,
    [PhoneCode] int  NOT NULL
);
GO

-- Creating table 'AdditionalPassportDetails'
CREATE TABLE [dbo].[AdditionalPassportDetails] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PassportNumber] varchar(50)  NOT NULL,
    [CountryOfIssue] varchar(50)  NOT NULL,
    [DateOfIssue] datetime  NULL,
    [ClientApplicationId] int  NOT NULL
);
GO

-- Creating table 'IncomeTaxDetails'
CREATE TABLE [dbo].[IncomeTaxDetails] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IncomeTaxNumber] varchar(50)  NOT NULL,
    [CounntryOfIssue] varchar(50)  NOT NULL,
    [ClientApplicationId] int  NOT NULL
);
GO

-- Creating table 'InvestmentApplications'
CREATE TABLE [dbo].[InvestmentApplications] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ClientReferenceNumber] nvarchar(6)  NOT NULL,
    [RecommendedProductCode] nvarchar(20)  NOT NULL,
    [RecommendedProductName] nvarchar(200)  NOT NULL,
    [RiskReport] varbinary(max)  NOT NULL,
    [RecordOfAdvice] varbinary(max)  NOT NULL,
    [ApplicationRecieved] datetime  NOT NULL,
    [IDCopy] varbinary(max)  NULL,
    [ProofOfAdress] varbinary(max)  NULL,
    [ClientAcceptTCs] bit  NULL,
    [ClientAcceptTCsTimeStamp] datetime  NULL,
    [ClientIndicatesSignature] bit  NULL,
    [ClientFullName] nvarchar(250)  NULL,
    [ClientIndicatesSignatureTimeStamp] datetime  NULL,
    [ApplicationSubmitted] datetime  NULL,
    [ClientType] varchar(50)  NULL,
    [Title] varchar(50)  NULL,
    [Name] varchar(250)  NULL,
    [Surname] varchar(250)  NULL,
    [IDPassportNumber] varchar(50)  NULL,
    [BirthDate] datetime  NULL,
    [TempResidentNumber] varchar(50)  NULL,
    [PermitExpiryDate] datetime  NULL,
    [StreetAdressLine1] varchar(250)  NULL,
    [StreetAdressLine2] varchar(250)  NULL,
    [City] varchar(50)  NULL,
    [Province] varchar(50)  NULL,
    [PhoneHome] varchar(50)  NULL,
    [PhoneWork] varchar(50)  NULL,
    [Mobile] varchar(50)  NULL,
    [EMail] varchar(255)  NULL,
    [KinName] varchar(250)  NULL,
    [KinSurname] varchar(250)  NULL,
    [KinMobile] varchar(50)  NULL,
    [FundManagersConsent] bit  NULL,
    [PostConsent] bit  NULL,
    [SMSConsent] bit  NULL,
    [PhoneConsent] bit  NULL,
    [EMailConsent] bit  NULL,
    [IranConfirm] bit  NULL,
    [SouthSudanConfirm] bit  NULL,
    [NorthKoreaConfirm] bit  NULL,
    [CubaConfirm] bit  NULL,
    [SyriaConfirm] bit  NULL,
    [CrimeaRegionConfirm] bit  NULL,
    [NorthSudanConfirm] bit  NULL,
    [LumpSumInvestment] decimal(19,4)  NULL,
    [MonthlyDebitOrder] decimal(19,4)  NULL,
    [UnitInvestConfirm] bit  NULL,
    [ReinvestAccountHolder] varchar(250)  NULL,
    [ReinvestBank] varchar(50)  NULL,
    [ReinvestAccount] varchar(50)  NULL,
    [Increase10] bit  NULL,
    [Increase15] bit  NULL,
    [Increase20] bit  NULL,
    [DebitOrderAccountHolder] varchar(250)  NULL,
    [DebitOrderBank] varchar(50)  NULL,
    [DebitOrderAccountNumber] varchar(50)  NULL,
    [DebitOrderBranchCode] varchar(10)  NULL,
    [DebitOrderAmount] decimal(19,4)  NULL,
    [DebitOrderDay] int  NULL,
    [DebitOrderFromMonth] varchar(50)  NULL,
    [RedemptionAccountHolder] varchar(250)  NULL,
    [RedemptionBank] varchar(50)  NULL,
    [RedemptionAccountNumber] varchar(50)  NULL,
    [RedemptionBranchCode] varchar(10)  NULL,
    [RedemptionAccountType] varchar(50)  NULL,
    [CashflowplanConfirm] bit  NULL,
    [AcceptTerms] bit  NULL,
    [AcceptSignature] bit  NULL,
    [DeclarationAccept] bit  NULL,
    [Income_SalaryWages] bit  NULL,
    [Income_Commission] bit  NULL,
    [Income_Bonus] bit  NULL,
    [Income_Maintenance] bit  NULL,
    [Income_Pension] bit  NULL,
    [Income_Investments] bit  NULL,
    [Income_InsuranceClaim] bit  NULL,
    [Income_Allowance] bit  NULL,
    [Income_DonationGift] bit  NULL,
    [Income_Inheritance] bit  NULL,
    [Income_SocialGrant] bit  NULL,
    [Income_RetirementAnnuity] bit  NULL,
    [Income_Other] bit  NULL,
    [Income_OtherDescription] varchar(max)  NULL,
    [SourceOfWealth] varchar(max)  NULL,
    [DebitOrderAccountType] varchar(40)  NULL,
    [ReinvestAccountType] varchar(40)  NULL,
    [PostAddress] varchar(250)  NULL,
    [PostPostalCode] varchar(10)  NULL,
    [PostCity] varchar(50)  NULL,
    [PostProvince] varchar(50)  NULL,
    [IDType] varchar(30)  NULL,
    [TempResident] bit  NULL,
    [Gender] varchar(10)  NULL,
    [MaritalStatus] varchar(30)  NULL,
    [SecondaryEMail] varchar(255)  NULL,
    [ReinvestBranchCode] varchar(10)  NULL,
    [SATaxResidentOnly] bit  NULL,
    [Funds_SalaryWages] bit  NULL,
    [Funds_Commission] bit  NULL,
    [Funds_Bonus] bit  NULL,
    [Funds_Maintenance] bit  NULL,
    [Funds_Pension] bit  NULL,
    [Funds_Investments] bit  NULL,
    [Funds_InsuranceClaim] bit  NULL,
    [Funds_Allowance] bit  NULL,
    [Funds_DonationGift] bit  NULL,
    [Funds_Inheritance] bit  NULL,
    [Funds_SocialGrant] bit  NULL,
    [Funds_RetirementAnnuity] bit  NULL,
    [Funds_Other] bit  NULL,
    [Funds_OtherDescription] varchar(max)  NULL,
    [POPIConsent] bit  NULL,
    [Guardian_Surname] varchar(50)  NULL,
    [Guardian_IDNumber] varchar(50)  NULL,
    [Guardian_Telephone_Number] varchar(50)  NULL,
    [Guardian_Name] varchar(50)  NULL,
    [Guardian_Email] varchar(255)  NULL,
    [Increase0] bit  NULL,
    [CashFlowPlanAmount] decimal(19,4)  NULL,
    [CashFlowPlanDate] datetime  NULL,
    [IDFileType] varchar(10)  NULL,
    [POAFileType] varchar(10)  NULL
);
GO

-- Creating table 'Banks'
CREATE TABLE [dbo].[Banks] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [BankName] varchar(50)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Configurations'
ALTER TABLE [dbo].[Configurations]
ADD CONSTRAINT [PK_Configurations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'NameEmails'
ALTER TABLE [dbo].[NameEmails]
ADD CONSTRAINT [PK_NameEmails]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BatchCopies'
ALTER TABLE [dbo].[BatchCopies]
ADD CONSTRAINT [PK_BatchCopies]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Countries'
ALTER TABLE [dbo].[Countries]
ADD CONSTRAINT [PK_Countries]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AdditionalPassportDetails'
ALTER TABLE [dbo].[AdditionalPassportDetails]
ADD CONSTRAINT [PK_AdditionalPassportDetails]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'IncomeTaxDetails'
ALTER TABLE [dbo].[IncomeTaxDetails]
ADD CONSTRAINT [PK_IncomeTaxDetails]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InvestmentApplications'
ALTER TABLE [dbo].[InvestmentApplications]
ADD CONSTRAINT [PK_InvestmentApplications]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Banks'
ALTER TABLE [dbo].[Banks]
ADD CONSTRAINT [PK_Banks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ClientApplicationId] in table 'AdditionalPassportDetails'
ALTER TABLE [dbo].[AdditionalPassportDetails]
ADD CONSTRAINT [fk_client_id]
    FOREIGN KEY ([ClientApplicationId])
    REFERENCES [dbo].[InvestmentApplications]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_client_id'
CREATE INDEX [IX_fk_client_id]
ON [dbo].[AdditionalPassportDetails]
    ([ClientApplicationId]);
GO

-- Creating foreign key on [ClientApplicationId] in table 'IncomeTaxDetails'
ALTER TABLE [dbo].[IncomeTaxDetails]
ADD CONSTRAINT [fk_client_id2]
    FOREIGN KEY ([ClientApplicationId])
    REFERENCES [dbo].[InvestmentApplications]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_client_id2'
CREATE INDEX [IX_fk_client_id2]
ON [dbo].[IncomeTaxDetails]
    ([ClientApplicationId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------