USE [RoboDB]
GO

/****** Object:  Table [dbo].[InvestmentApplicationFields]    Script Date: 2016/11/16 5:18:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[InvestmentApplications] ADD 
    [NoneConfirm] [bit] NULL,
    [AcceptTerms] [bit] NULL,
    [AcceptSignature] [bit] NULL,
    [DeclarationAccept] [bit] NULL