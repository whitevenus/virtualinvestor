USE [RoboDB]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RoboLogs](
	[EventDateTime] [datetime] NOT NULL,
	[UID] [varchar](max) NULL,
	[EventLevel] [varchar](max) NOT NULL,
	[Section] [varchar](max) NULL,
	[Exception] [varchar](max) NOT NULL,
	[InnerException] [varchar](max) NULL,
	[RelatedField] [varchar](max) NULL,
	[CallSite] [varchar](max) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	CONSTRAINT [PK_RoboLogs] PRIMARY KEY CLUSTERED 
    (
        [Id] ASC
    )
)
GO