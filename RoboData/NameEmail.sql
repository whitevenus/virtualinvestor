USE [RoboDB]
GO

/****** Object:  Table [dbo].[NameEmail]    Script Date: 10/21/2016 3:06:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NameEmail](
	[Name] [varchar](250) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[AcceptTsCs] [int] NOT NULL,
	[AcceptTsCsTimeStamp] [datetime] NULL,
	[ReadInvestInfo] [int] NOT NULL,
	[ReadInvestInfoTimeStamp] [datetime] NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_NameEmail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


