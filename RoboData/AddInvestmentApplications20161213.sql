USE [RoboDB]
GO

/****** Object:  Table [dbo].[InvestmentApplicationFields]    Script Date: 2016/11/16 5:18:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[InvestmentApplications] DROP COLUMN
	[Language],
	[NoneConfirm];

ALTER TABLE [dbo].[InvestmentApplications] ADD
	[DebitOrderAccountType] varchar(40) NULL,
	[ReinvestAccountType] varchar(40) NULL;

CREATE TABLE [dbo].[IncomeTaxDetails] (
  [Id] int IDENTITY(1,1) NOT NULL,
  [ClientReferenceNumber] nvarchar(6)  NOT NULL,
  [ApplicationID] int NOT NULL,
  [IncomeTaxNumber] varchar(50) NOT NULL,
  [CounntryOfIssue] varchar(50) NOT NULL,
  CONSTRAINT [PK_IncomeTaxDetails] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO