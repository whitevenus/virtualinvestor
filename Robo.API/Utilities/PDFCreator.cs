﻿using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using RoboData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;

namespace Robo.API.Utilities
{
    public class PDFCreator
    {
        private Dictionary<string,string> fieldNames = new Dictionary<string, string>
        { { "ClientFullName","Client Full Name" }
        , { "Title","Title" }
        , { "ClientReferenceNumber","Client Reference Number" }
        , { "RecommendedProductName","Recommended Product Name" }
        , { "Name","Name" }
        , { "Surname","Surname" }
        , { "DeceasedEstateName","Deceased Estate Name" }
        , { "IDPassportNumber","ID or Passport Number" }
        , { "BirthDate","Birth Date" }
        ,{"Language"   ,"Language" }
        ,{"IncomeTaxNumber",   "Income Tax Number"}
        ,{"PassportNationality",   "Passport Nationality"}
        ,{"PassportCountry"  , "Passport Country"}
        ,{"BirthCountry"  , "Birth Country"}
        ,{"ResidenceCountry" ,  "Residence Country"}
        ,{"TempResidentNumber",   "Temp Resident Number"}
        ,{"PermitExpiryDate"   ,"Permit Expiry Date"}
        ,{"StreetAdressLine1"  , "Street Adress Line1"}
        ,{"StreetAdressLine2"  , "Street Adress Line2"}
        ,{"City"  , "City"}
        ,{"Province" ,  "Province"}
        ,{"PostBoxNumber" ,  "Post Box Number"}
        ,{"PostBoxSuburb"  , "Post Box Suburb"}
        ,{"PostBoxCode" ,  "Post Box Code"}
        ,{"PhoneHome"  , "Phone Home"}
        ,{"PhoneWork" ,  "Phone Work"}
        ,{"Mobile" ,  "Mobile"}
        ,{"EMail"  , "E - Mail"}
        ,{"SecondaryEMail", "Secondary Email"}
        ,{"KinTitle" ,  "Kin Title"}
        ,{"KinName"  , "Kin Name"}
        ,{"KinSurname" ,  "Kin Surname"}
        ,{"KinRelation" ,  "Kin Relation"}
        ,{"KinPhone"  , "Kin Phone"}
        ,{"KinMobile"  , "Kin Mobile"}
        ,{"KinEMail"  , "Kin Email"}
        ,{"IncomeSource" ,  "Income Source"}
        ,{"FundManagersConsent" ,  "Fund Managers Consent"}
        ,{"PostConsent" ,  "Post Consent"}
        ,{"SMSConsent"  , "SMS Consent"}
        ,{"PhoneConsent" ,  "Phone Consent"}
        ,{"EMailConsent"  , "EMail Consent"}
        ,{"LumpSumInvestment" ,  "Lump Sum Investment"}
        ,{"MonthlyDebitOrder"  , "Monthly Debit Order"}
        ,{"UnitInvestConfirm"   ,"Unit Invest Confirm"}
        ,{"NoReinvestConfirm"  , "No Reinvest Confirm"}
        ,{"ReinvestAccountHolder" ,  "Reinvest Account Holder"}
        ,{"ReinvestBank" ,  "Reinvest Bank"}
        ,{"ReinvestAccount"  , "Reinvest Account"}
        ,{"DebitOrderAccountHolder" ,  "Debit Order Account Holder"}
        ,{"DebitOrderBank" ,  "Debit Order Bank"}
        ,{"DebitOrderAccountNumber" ,  "Debit Order Account Number"}
        ,{"DebitOrderBranchCode" ,  "Debit Order Branch Code"}
        ,{"DebitOrderAmount" ,  "Debit Order Amount"}
        ,{"DebitOrderDay"  , "Debit Order Day"}
        ,{"DebitOrderFromMonth" ,  "Debit Order From Month"}
        ,{"RedemptionAccountHolder"  , "Redemption Account Holder"}
        ,{"RedemptionBank" ,  "Redemption Bank"}
        ,{"RedemptionAccountNumber" ,  "Redemption Account Number"}
        ,{"RedemptionBranchCode"  , "Redemption Branch Code"}
        ,{"RedemptionAccountType"  , "Redemption Account Type"}
        , { "ClientIndicatesSignature", "Client Indicates Signature"}
        , { "NoneConfirm", "No Sanctions Clause Confirm" }
        , {"AcceptTerms","Accept Terms" }
        , {"AcceptSignature","Accept Signature" }
        , {"DeclarationAccept","Declaration Accept" }
    };

        public class LayoutHelper
        {
            private readonly PdfDocument _document;
            private readonly XUnit _topPosition;
            private readonly XUnit _bottomMargin;
            private XUnit _currentPosition;
            public LayoutHelper(PdfDocument document, XUnit topPosition, XUnit bottomMargin)
            {
                _document = document;
                _topPosition = topPosition;
                _bottomMargin = bottomMargin;
                // Set a value outside the page - a new page will be created on the first request.
                _currentPosition = bottomMargin + 10000;
            }

            public XUnit GetLinePosition(XUnit requestedHeight)
            {
                return GetLinePosition(requestedHeight, -1f);
            }

            public XUnit GetLinePosition(XUnit requestedHeight, XUnit requiredHeight)
            {
                XUnit required = requiredHeight == -1f ? requestedHeight : requiredHeight;
                if (_currentPosition + required > _bottomMargin)
                    CreatePage();
                XUnit result = _currentPosition;
                _currentPosition += requestedHeight;
                return result;
            }

            public XGraphics Gfx { get; private set; }
            public PdfPage Page { get; private set; }

            void CreatePage()
            {
                Page = _document.AddPage();
                Page.Size = PageSize.A4;
                Gfx = XGraphics.FromPdfPage(Page);
                _currentPosition = _topPosition;
            }
        }
        public string CreateApplicationPDF(string destPath, string brokerCode, InvestmentApplication application)
        {
            PdfDocument document = new PdfDocument();

            // Sample uses DIN A4, page height is 29.7 cm. We use margins of 2.5 cm.
            LayoutHelper helper = new LayoutHelper(document, XUnit.FromCentimeter(2.5), XUnit.FromCentimeter(29.7 - 2.5));
            XUnit left = XUnit.FromCentimeter(2.5);

            // Random generator with seed value, so created document will always be the same.
            Random rand = new Random(42);

            const int headerFontSize = 10;
            const int normalFontSize = 10;

            XFont fontHeader = new XFont("Verdana", headerFontSize, XFontStyle.BoldItalic);
            XFont fontNormal = new XFont("Verdana", normalFontSize, XFontStyle.Regular);
            {   /// draw the heading                
                XUnit top = helper.GetLinePosition(headerFontSize + 5, headerFontSize + 5 + normalFontSize);
                helper.Gfx.DrawString("Absa Virtual Investor - Investment Application Form",
                    fontHeader, XBrushes.Black, left, top, XStringFormats.TopLeft);
                               
            }
            {
                XUnit top = helper.GetLinePosition(normalFontSize + 2, normalFontSize);
                helper.Gfx.DrawString(string.Format("{0}: {1}", "BrokerCode", brokerCode), fontNormal, XBrushes.Black, left, top, XStringFormats.TopLeft);
            }
            

                /// draw each property line by line
            foreach (var prop in application.GetType().GetProperties())
            {
                XUnit top = helper.GetLinePosition(normalFontSize + 2, normalFontSize);
                if (fieldNames.Keys.Contains(prop.Name))
                {
                    var val = prop.GetValue(application, null);
                    if (prop.PropertyType == typeof(DateTime))
                    {
                        val = ((DateTime)val).ToString("yyyy-MM-dd");
                    }
                    if (prop.PropertyType == typeof(DateTime?))
                    {
                        if (val != null && ((DateTime?)val).Value != null)
                            val = ((DateTime?)val).Value.ToString("yyyy-MM-dd");
                        else val = "";
                    }

                    Debug.WriteLine("VAL: "+fieldNames[prop.Name] + " "+ val);
                    helper.Gfx.DrawString(string.Format("{0}: {1}", fieldNames[prop.Name], val), fontNormal, XBrushes.Black, left, top, XStringFormats.TopLeft);
                }
                    
                //Console.WriteLine("{0}:{1}", prop.Name, prop.GetValue(application, null));
            }
            
            bool ConditionsConfirmed = application.ClientIndicatesSignature.HasValue && application.ClientIndicatesSignature.Value;

            {
                XUnit top = helper.GetLinePosition(normalFontSize + 2, normalFontSize);

                helper.Gfx.DrawString(string.Format("{0}: {1}", "Conditions Confirmed(Client Signature)", ConditionsConfirmed), fontNormal, XBrushes.Black, left, top, XStringFormats.TopLeft);

            }
            // Save the document... 
            string filename = destPath + "\\application " + application.IDPassportNumber + ".pdf";
            document.Save(filename);
            return filename; 
        }
    }
}