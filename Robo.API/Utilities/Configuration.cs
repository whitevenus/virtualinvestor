﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Robo.API.Utilities
{
    public class Configuration
    {
        public Configuration()
        {
            this.DocSource = System.Configuration.ConfigurationManager.AppSettings["DocSource"];
            this.DocDest = System.Configuration.ConfigurationManager.AppSettings["DocDest"];
            this.DocShare = System.Configuration.ConfigurationManager.AppSettings["DocShare"];
            this.MailDestination = System.Configuration.ConfigurationManager.AppSettings["MailDestination"];
            this.LeadsDestination = System.Configuration.ConfigurationManager.AppSettings["LeadsDestination"];
            this.ReplyEmail = System.Configuration.ConfigurationManager.AppSettings["ReplyEmail"];
            this.SMTPServer = System.Configuration.ConfigurationManager.AppSettings["SMTPServer"]; 
            this.SMTPPort = System.Configuration.ConfigurationManager.AppSettings["SMTPPort"];
            if (this.SMTPPort == string.Empty)
                this.SMTPPort = "25";
            this.SMTPUser = System.Configuration.ConfigurationManager.AppSettings["SMTPUser"];
            this.SMTPPass = System.Configuration.ConfigurationManager.AppSettings["SMTPPass"];
            this.ShareZip = System.Configuration.ConfigurationManager.AppSettings["ShareZip"];
            this.SmartRand = System.Configuration.ConfigurationManager.AppSettings["SmartRand"];
            this.SmartRandURI = System.Configuration.ConfigurationManager.AppSettings["SmartRandURI"];
            this.BrokerCode = System.Configuration.ConfigurationManager.AppSettings["BrokerCode"];
            int mupload = 750;
            int.TryParse(System.Configuration.ConfigurationManager.AppSettings["MaxUploadSize"], out mupload);
            this.MaxUploadSize = mupload;
        }
        public string DocSource { get; set; }
        public string DocDest { get; set; }
        public string DocShare { get; set; }
        public string MailDestination { get; set; }
        public string LeadsDestination { get; set; }
        public string ReplyEmail { get; set; }
        public string SMTPServer { get; set; }
        public string SMTPPort { get; set; }
        public string SMTPUser { get; set; }
        public string SMTPPass { get; set; }
        public string ShareZip { get; set; }
        public string SmartRand { get; set; }
        public string SmartRandURI { get; set; }
        public string BrokerCode { get; set; }
        public int MaxUploadSize { get; set; }
    }
}