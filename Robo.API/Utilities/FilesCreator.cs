﻿using RoboData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using NLog;
 
namespace Robo.API.Utilities
{
    
    public class FilesCreator
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private string createFileFromBytes(string name, byte[] data)
        {
            if (data == null)
                File.WriteAllBytes(name, new byte[] { });
            else
                File.WriteAllBytes(name, data); // Requires System.IO
            return name;
        }

        public FileNames writeApplicationZip(InvestmentApplication applicationContent, string IDFileType, string POAFileType)
        {
            logger.Info("FP: Starting in writeApplicationZip");
            if (applicationContent.IDPassportNumber == "Docs")
            {
                Console.WriteLine("cannot save with invalid id nr");
                return new FileNames();
            }
            logger.Info("FP:  after IF");
            Robo.API.Utilities.Configuration cfg = new Robo.API.Utilities.Configuration();
            Robo.API.Utilities.PDFCreator pdf = new Robo.API.Utilities.PDFCreator();
            string testRoot = cfg.DocSource;
            string outputRoot = cfg.DocDest;
            DocZip appArchive = new DocZip();
            DirectoryInfo tempFolder = new DirectoryInfo(cfg.DocSource);
            DirectoryInfo sandBox = tempFolder.CreateSubdirectory(applicationContent.ClientReferenceNumber);
            string sandBoxPath = sandBox.FullName;

            logger.Info("FP: After initilasations");

            //Additional Investment information - WIP.PDF
            //app form.pdf
            //files.txt
            //ID.pdf
            //Proof of Residence.pdf
            //Record of Advice. Individual Investor.pdf
            //Risk Tolerance Report.pdf
            //ts and cs.pdf

            logger.Info("FP: before file creations");
            FileNames files = new FileNames
            {   ApplicationForm = pdf.CreateApplicationPDF(sandBoxPath, cfg.BrokerCode, applicationContent)
            ,   CopyOfID = createFileFromBytes(sandBoxPath + "\\ID " + applicationContent.IDPassportNumber + IDFileType, applicationContent.IDCopy)
            ,   InvestmentInfo = testRoot + "\\Docs\\Additional Investment information - WIP.PDF"
            ,   ProofofResidence = createFileFromBytes(sandBoxPath + "\\POR " + applicationContent.IDPassportNumber + POAFileType, applicationContent.ProofOfAdress)
            ,   RecordOfAdvice = createFileFromBytes(sandBoxPath + "\\ROA " + applicationContent.IDPassportNumber + ".pdf", applicationContent.RecordOfAdvice)
            ,   RiskReport = createFileFromBytes(sandBoxPath + "\\RR " + applicationContent.IDPassportNumber + ".pdf", applicationContent.RiskReport)
            ,   TermsConditions = testRoot + "\\Docs\\ts and cs.pdf"
            };
            logger.Info("FP:   after file creations");
            logger.Info("FP:   after file creations "+ ConvertEx.ToString(applicationContent.Id + 93001531, ConvertEx.Bases.Base36));
            
            DocMeta docMeta = new DocMeta
            {   ApplicationReferenceNumber = ConvertEx.ToString(applicationContent.Id + 93001531, ConvertEx.Bases.Base36)
            ,   EmailAddress = applicationContent.EMail
            ,   IdentityNumber = applicationContent.IDPassportNumber
            ,   Name = applicationContent.Name + applicationContent.Surname
            };

            logger.Info("FP: After DocMeta");
            return appArchive.zipApplicationDirectory(outputRoot, testRoot, cfg.DocShare, cfg.ShareZip, docMeta, files);            
        }
    }
}