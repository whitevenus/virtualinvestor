﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Robo.API.Utilities
{
    /// <summary>
    /// Class that provides enhanced functionality for converting between
    /// integers and strings using any base.
    /// </summary>
    static class ConvertEx
    {
        /// <summary>
        /// Predefined conversion bases.
        /// </summary>
        public enum Bases
        {
            Binary = 2,
            Octal = 8,
            Decimal = 10,
            Hexadecimal = 16,
            Base36 = 36,
            Base64 = 64
        }

        // Characters for bases up to 64
        public const string Base64 =
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+/";

        /// <summary>
        /// Converts the given valie to a string using the given radix.
        /// </summary>
        /// <param name="value">Value to convert</param>
        /// <param name="radix">Radix</param>
        /// <returns>The resulting string</returns>
        public static string ToString(int value, Bases radix)
        {
            return ToString(value, (int)radix, Base64);
        }

        /// <summary>
        /// Converts the given valie to a string using the given radix.
        /// </summary>
        /// <param name="value">Value to convert</param>
        /// <param name="radix">Radix</param>
        /// <returns>The resulting string</returns>
        public static string ToString(int value, int radix)
        {
            return ToString(value, radix, Base64);
        }

        /// <summary>
        /// Converts the given valie to a string using the given radix.
        /// </summary>
        /// <param name="value">Value to convert</param>
        /// <param name="radix">Radix</param>
        /// <param name="digits">String of legal digits</param>
        /// <returns>The resulting string</returns>
        public static string ToString(int value, int radix, string digits)
        {
            // Validate radix
            if (radix < 2 || radix > digits.Length)
                throw new ArgumentException();

            // Convert value to characters
            StringBuilder builder = new StringBuilder();
            do
            {
                builder.Insert(0, digits[value % radix]);
                value = value / radix;
            }
            while (value > 0);

            // Return result
            return builder.ToString();
        }

        /// <summary>
        /// Parses the specified string to an integer.
        /// </summary>
        /// <param name="s">String to parse</param>
        /// <param name="radix">Radix</param>
        /// <returns>The parsed integer value</returns>
        public static int Parse(string s, Bases radix)
        {
            return Parse(s, (int)radix, Base64);
        }

        /// <summary>
        /// Parses the specified string to an integer.
        /// </summary>
        /// <param name="s">String to parse</param>
        /// <param name="radix">Radix</param>
        /// <returns>The parsed integer value</returns>
        public static int Parse(string s, int radix)
        {
            return Parse(s, radix, Base64);
        }

        /// <summary>
        /// Parses the specified string to an integer.
        /// </summary>
        /// <param name="s">String to parse</param>
        /// <param name="radix">Radix</param>
        /// <param name="digits">String of legal digits</param>
        /// <returns>The parsed integer value</returns>
        public static int Parse(string s, int radix, string digits)
        {
            int result;
            if (!TryParse(s, radix, digits, out result))
                throw new ArgumentException();
            return result;
        }

        /// <summary>
        /// Attempts to parse the specified string to an integer.
        /// </summary>
        /// <param name="s">String to parse</param>
        /// <param name="radix">Radix</param>
        /// <param name="result">Returns the parsed integer value</param>
        /// <returns>True if string was successfully parsed</returns>
        public static bool TryParse(string s, Bases radix, out int result)
        {
            return TryParse(s, (int)radix, Base64, out result);
        }

        /// <summary>
        /// Attempts to parse the specified string to an integer.
        /// </summary>
        /// <param name="s">String to parse</param>
        /// <param name="radix">Radix</param>
        /// <param name="result">Returns the parsed integer value</param>
        /// <returns>True if string was successfully parsed</returns>
        public static bool TryParse(string s, int radix, out int result)
        {
            return TryParse(s, radix, Base64, out result);
        }

        /// <summary>
        /// Attempts to parse the specified string to an integer.
        /// </summary>
        /// <param name="s">String to parse</param>
        /// <param name="radix">Radix</param>
        /// <param name="digits">String of legal digits</param>
        /// <param name="result">Returns the parsed integer value</param>
        /// <returns>True if string was successfully parsed</returns>
        public static bool TryParse(string s, int radix, string digits, out int result)
        {
            // Validate radix
            if (radix < 2 || radix > digits.Length)
                throw new ArgumentException();

            // Normalize input
            s = s.Trim();

            // If selected radix does not utilize both upper and lower case letters,
            // then convert to upper case so that parsing is not case sensitive
            if (radix <= 36)
                s = s.ToUpper();

            int pos = 0;
            result = 0;

            // Use lookup table to parse string
            while (pos < s.Length && !Char.IsWhiteSpace(s[pos]))
            {
                string digit = s.Substring(pos, 1);
                int i = digits.IndexOf(digit);
                if (i >= 0 && i < radix)
                {
                    result *= radix;
                    result += i;
                    pos++;
                }
                else
                {
                    // Invalid character encountered
                    return false;
                }
            }
            // Return true if any characters processed
            return (pos > 0);
        }
    }
}