﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Xml;

namespace Robo.API.Utilities
{
    public class DocMeta
    {
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string IdentityNumber { get; set; }
        public string ApplicationReferenceNumber { get; set; }
    }
    public class FileNames
    {
        public string CopyOfID { get; set; }
        public string ApplicationForm { get; set; }
        public string ProofofResidence { get; set; }
        public string TermsConditions { get; set; }
        public string InvestmentInfo { get; set; }
        public string RecordOfAdvice { get; set; }
        public string RiskReport { get; set; }
    }
    class ApplicationMetaDoc
    {
        private static void addTextChild(XmlDocument doc, XmlElement parent, string name, string content)
        {
            XmlElement textElement = doc.CreateElement(string.Empty, name, string.Empty);
            XmlText textNode = doc.CreateTextNode(content);
            parent.AppendChild(textElement);
            textElement.AppendChild(textNode);
            parent.AppendChild(textElement);

        }
        private static string GetName(string fullName)
        {
            return new FileInfo(fullName).Name;
        }
        public static void createMetaDataFile(string output, DocMeta meta, FileNames files)
        {
            XmlDocument doc = new XmlDocument();

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            XmlElement top = doc.CreateElement(string.Empty, "VirtualAdvisorMailer", string.Empty);
            doc.AppendChild(top);

            XmlElement clientDetails = doc.CreateElement(string.Empty, "ClientDetails", string.Empty);
            top.AppendChild(clientDetails);
            addTextChild(doc, clientDetails, "Name", meta.Name);
            addTextChild(doc, clientDetails, "EmailAddress", meta.EmailAddress);
            addTextChild(doc, clientDetails, "IdentityNumber", meta.IdentityNumber);
            addTextChild(doc, clientDetails, "ApplicationReferenceNumber", meta.ApplicationReferenceNumber);

            XmlElement documentList = doc.CreateElement(string.Empty, "DocumentList", string.Empty);
            top.AppendChild(documentList);
            addTextChild(doc, documentList, "Name", GetName(files.ApplicationForm));
            addTextChild(doc, documentList, "CopyOfID", GetName(files.CopyOfID));
            addTextChild(doc, documentList, "ProofofResidence", GetName(files.ProofofResidence));
            addTextChild(doc, documentList, "TermsConditions", GetName(files.TermsConditions));
            addTextChild(doc, documentList, "InvestmentInfo", GetName(files.InvestmentInfo));
            addTextChild(doc, documentList, "RecordOfAdvice", GetName(files.RecordOfAdvice));
            addTextChild(doc, documentList, "RiskReport", GetName(files.RiskReport));
            doc.Save(output);
        }
    }


    public class DocZip
    {

        private string nameOf(string full)
        {
            return new FileInfo(full).Name;
        }
        public FileNames zipApplicationDirectory(string outRoot, string root, string finalDestination, string doZip,DocMeta meta, FileNames files)
        {

            DirectoryInfo tempFolder = new DirectoryInfo(outRoot);
            DirectoryInfo referFolder = tempFolder.CreateSubdirectory(meta.ApplicationReferenceNumber);
            DirectoryInfo metaDir = referFolder.CreateSubdirectory("Meta");
            DirectoryInfo docsDir = referFolder.CreateSubdirectory("Docs");
            ApplicationMetaDoc.createMetaDataFile(metaDir.FullName + "\\" + "metadata.xml", meta, files);

            FileNames resultFiles = new FileNames
            {   ApplicationForm = docsDir.FullName + "\\" + nameOf(files.ApplicationForm)
            ,   CopyOfID = docsDir.FullName + "\\" + nameOf(files.CopyOfID)
            ,   InvestmentInfo = docsDir.FullName + "\\" + nameOf(files.InvestmentInfo)
            ,   ProofofResidence = docsDir.FullName + "\\" + nameOf(files.ProofofResidence)
            ,   RecordOfAdvice = docsDir.FullName + "\\" + nameOf(files.RecordOfAdvice)
            ,   RiskReport = docsDir.FullName + "\\" + nameOf(files.RiskReport)
            ,   TermsConditions = docsDir.FullName + "\\" + nameOf(files.TermsConditions)
            };

            List<string> fileList = new List<string>
            {   files.ApplicationForm
            ,   files.CopyOfID
            ,   files.InvestmentInfo
            ,   files.ProofofResidence
            ,   files.RecordOfAdvice
            ,   files.RiskReport
            ,   files.TermsConditions
            };

            List<string> outputFiles = new List<string>();
            foreach (string fileName in fileList)
            {
                FileInfo inputFile = new FileInfo(fileName);
                string outputFileName = docsDir.FullName + "\\" + inputFile.Name;
                try
                {
                    inputFile.CopyTo(outputFileName);

                }
                catch (IOException ioe)
                {
                    Console.WriteLine(ioe.Message);
                }
                outputFiles.Add(outputFileName);
            }
            if(doZip == "true")
                ZipFile.CreateFromDirectory(outRoot + "\\" + meta.ApplicationReferenceNumber, finalDestination + "\\" + meta.ApplicationReferenceNumber + ".zip");
            return resultFiles;
        }
    }
}