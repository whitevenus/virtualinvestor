﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using System.Web.Http;


[assembly: OwinStartup(typeof(Robo.API.Startup))]

namespace Robo.API
{/// <summary>
/// 
/// </summary>
    public partial class Startup
    {/// <summary>
     /// Configuration(IAppBuilder app)
     /// </summary>
     /// <param name="app"></param>
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
          
        }
    }
}
