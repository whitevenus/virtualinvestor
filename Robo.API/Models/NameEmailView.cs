﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Robo.API.Models
{
    public class NameEmailView
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public int AcceptTsCs { get; set; }
        public Nullable<System.DateTime> AcceptTsCsTimeStamp { get; set; }
        public int ReadInvestInfo { get; set; }
        public Nullable<System.DateTime> ReadInvestInfoTimeStamp { get; set; }
        public int Id { get; set; }        
    }
}