﻿using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using System;
using System.Text;
using System.Web.Security;
using Microsoft.Owin;
using System.Net.Http;
using Microsoft.AspNet.Identity.Owin;

namespace Robo.API.Filters
{
    public class IdentityBasicAuthenticationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (true) // set to false to enable authentication
            {
                base.OnActionExecuting(actionContext);
                return;

            }

            if (actionContext.Request.Headers.Authorization == null)
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                
            }
            else
            {

                string authToken = actionContext.Request.Headers.Authorization.Parameter;
                string decodedToken = Encoding.UTF8.GetString(Convert.FromBase64String(authToken));
                string username = decodedToken.Substring(0, decodedToken.IndexOf(":"));
                string password = decodedToken.Substring(decodedToken.IndexOf(":") + 1);
                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                {
                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                }
                else
                {
                    try
                    {
                        IOwinContext context = actionContext.Request.GetOwinContext();
                        ApplicationSignInManager signin =
                        new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
                        var result = signin.PasswordSignIn(username, password, false, false);
                        switch (result)
                        {
                            case SignInStatus.Success:
                                base.OnActionExecuting(actionContext);
                                break;
                            case SignInStatus.LockedOut:
                            case SignInStatus.RequiresVerification:
                            case SignInStatus.Failure:
                            default:
                                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                                break;
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                    }
                }

            }
        }
    }
}