﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RoboData;
using Robo.API.Filters;
using Robo.API.Utilities;
using System.IO;
using System.Net.Mail;
using System.Diagnostics;
using NLog;
using System.Data.Entity.Validation;

namespace Robo.API.Controllers
{/// <summary>
 /// 
 /// </summary>
   
    [IdentityBasicAuthenticationAttribute]
    //[Authorize]
    //[AllowAnonymous]
    public class InvestmentApplicationsController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private RoboDBEntities db = new RoboDBEntities();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        // GET: api/InvestmentApplications
        //public IQueryable<InvestmentApplication> GetInvestmentApplications()
        //{
        //    return db.InvestmentApplications;
        //}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/InvestmentApplications/5
        //[Route("api/InvestmentApplicationsById/{id:int}")]
        //[ResponseType(typeof(InvestmentApplication))]
        private async Task<IHttpActionResult> GetInvestmentApplication(int id)
        {
            InvestmentApplication investmentApplication = await db.InvestmentApplications.FindAsync(id);
            if (investmentApplication == null)
            {
                return NotFound();
            }

            return Ok(investmentApplication);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ClientRef"></param>
        /// <returns></returns>
        // GET: api/InvestmentApplications/0X1Y2Z
        [Route("api/InvestmentApplications/{ClientRef}")]
        [ResponseType(typeof(InvestmentApplication))]
        public async Task<IHttpActionResult> GetInvestmentApplication(string ClientRef)
        {

            try
            {
                InvestmentApplication investmentApplication =
                db.InvestmentApplications.SingleOrDefault(s => s.ClientReferenceNumber.ToLower() == ClientRef.ToLower().Replace(" ", ""));
                if (investmentApplication == null)
                {
                    return NotFound();
                }
                logger.Info("GetInvestmentApplication: Testing smart rand:" + investmentApplication.RecommendedProductCode);
                Robo.API.Utilities.Configuration cfg = new Robo.API.Utilities.Configuration();

                if (cfg.SmartRand == "true" && investmentApplication.RecommendedProductCode == "00000")
                {
                    logger.Info("GetInvestmentApplication: Starting smart rand");
                    var sm = new SmartRandAPIController();
                    logger.Info("GetInvestmentApplication: Calling smart rand");
                    try
                    {
                        investmentApplication = await sm.UpdateInvestmentApplicationFiles(investmentApplication);

                    }catch(Exception e)
                    {

                        logger.Error(e);

                        throw e;
                    }
                    logger.Info("GetInvestmentApplication: Completed Calling smart rand");

                }
                //if (investmentApplication.InvestmentApplicationFields.Count == 0)
                //{
                //    investmentApplication.InvestmentApplicationFields.Add(new InvestmentApplicationField());
                //    await db.SaveChangesAsync();
                //}

                return Ok(investmentApplication);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error accessing database (GetInvestmentApplication)" + e.Message);
            }
            return BadRequest();
           
        }
        
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="investmentApplication"></param>
        /// <returns></returns>
        // PUT: api/InvestmentApplications/5
        //[HttpPut]
        //[Route("api/InvestmentApplications/{id}")]
        //[ResponseType(typeof(void))]
        private async Task<IHttpActionResult> PutInvestmentApplication(int id, InvestmentApplication investmentApplication)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != investmentApplication.Id)
            {
                return BadRequest();
            }


            db.Entry(investmentApplication).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InvestmentApplicationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="investmentApplication"></param>
        /// <returns></returns>
        // POST: api/InvestmentApplications
        [Route("api/PostInvestmentApplications")]
        [ResponseType(typeof(InvestmentApplication))]
        public async Task<IHttpActionResult> PostInvestmentApplication(InvestmentApplication investmentApplication)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                Robo.API.Utilities.Configuration cfg = new Robo.API.Utilities.Configuration();
                logger.Info("PostInvestmentApplication: Testing smart rand:" + investmentApplication.RecommendedProductCode);
                investmentApplication.ApplicationRecieved = null;
                investmentApplication.ApplicationSubmitted = null;
                if (cfg.SmartRand == "true" && investmentApplication.RecommendedProductCode == "00000")
                {
                    logger.Info("PostInvestmentApplication: Starting smart rand");
                    var sm = new SmartRandAPIController();
                    logger.Info("PostInvestmentApplication: calling smart rand from PostInvestmentApplication");
                    investmentApplication = await sm.UpdateInvestmentApplicationFiles(investmentApplication);
                    logger.Info("PostInvestmentApplication: complete smart rand call");
                }

                
                if (!InvestmentApplicationExists(investmentApplication.Id))
                {
                    db.InvestmentApplications.Add(investmentApplication);
                }
                else
                {
                    db.Entry(investmentApplication).State = EntityState.Modified;
                }

                foreach(var item in investmentApplication.IncomeTaxDetails)
                {
                    if (db.IncomeTaxDetails.Select(x => x.Id).Contains(item.Id))
                        db.Entry(item).State = EntityState.Modified;
                    else
                        db.IncomeTaxDetails.Add(item);
                }

                foreach (var item in investmentApplication.AdditionalPassportDetails)
                {
                    if (db.AdditionalPassportDetails.Select(x => x.Id).Contains(item.Id))
                        db.Entry(item).State = EntityState.Modified;
                    else
                        db.AdditionalPassportDetails.Add(item);
                }

                //DATE CONVERSIONS TO datetime INSTEAD OF datetime2:
                DateTime? DB_BirthDate = DateTime.Today;
                string db_BirthDate_string = "";
                bool updateDOB = false;
                if(investmentApplication.BirthDate != null)
                {
                    updateDOB = true;
                    DB_BirthDate = investmentApplication.BirthDate;
                    investmentApplication.BirthDate = null;
                    db_BirthDate_string = DB_BirthDate.Value.Year + "-" + DB_BirthDate.Value.Month + "-" + DB_BirthDate.Value.Day + " 00:00:00.000"; 
                }
                //END DATE CONVERSIONS
                db.SaveChanges();

                //if (investmentApplication.ClientAcceptTCsTimeStamp != null)
                //{
                    db.Database.ExecuteSqlCommand("UPDATE InvestmentApplications SET ClientAcceptTCsTimeStamp = GETDATE() WHERE Id=" + investmentApplication.Id + ";");
                //}
                //if(investmentApplication.ClientIndicatesSignatureTimeStamp != null)
                //{
                    db.Database.ExecuteSqlCommand("UPDATE InvestmentApplications SET ClientIndicatesSignatureTimeStamp = GETDATE() WHERE Id=" + investmentApplication.Id + ";");
                //}
                if (updateDOB)
                {
                    db.Database.ExecuteSqlCommand("UPDATE InvestmentApplications SET BirthDate = '"+ db_BirthDate_string + "' WHERE Id=" + investmentApplication.Id + ";");
                }

                db.Database.ExecuteSqlCommand("UPDATE InvestmentApplications SET ApplicationRecieved = GETDATE() WHERE Id=" + investmentApplication.Id + ";");
                db.Database.ExecuteSqlCommand("UPDATE InvestmentApplications SET ApplicationSubmitted = '1901-01-01 12:00:00.000' WHERE Id=" + investmentApplication.Id + ";");
                db.SaveChanges();

                //RESTORE CONVFERSIONS:1917-3-1 00:00:00.000
                investmentApplication.BirthDate = DB_BirthDate;
            }
            catch(Exception ex)
            {
                GlobalDiagnosticsContext.Set("uid", investmentApplication.ClientReferenceNumber);
                GlobalDiagnosticsContext.Set("section", "PostInvestmentApplication:");
                GlobalDiagnosticsContext.Set("exception", ex.Message);
                GlobalDiagnosticsContext.Set("innerException", ex.InnerException != null ? ex.ToString() : "None");
                GlobalDiagnosticsContext.Set("relatedField", "Trying to save investment application");
                logger.Error(ex.Message);
                Console.WriteLine(ex.Message);
            }
            return Ok(investmentApplication); // CreatedAtRoute("DefaultApi", new { id = investmentApplication.Id }, investmentApplication);
        }
        //[Route("api/PostInvestmentApplications")]
        //[ResponseType(typeof(InvestmentApplication))]
        public async Task<IHttpActionResult> PostInitialInvestmentApplication(InvestmentApplication investmentApplication)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                
                if (!InvestmentApplicationExists(investmentApplication.Id))
                {
                    db.InvestmentApplications.Add(investmentApplication);
                }
                else
                {
                    db.Entry(investmentApplication).State = EntityState.Modified;
                }

                db.SaveChanges();
                db.Database.ExecuteSqlCommand("UPDATE InvestmentApplications SET ApplicationRecieved = GETDATE() WHERE Id="+ investmentApplication.Id+";");
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                GlobalDiagnosticsContext.Set("uid", investmentApplication.ClientReferenceNumber+"-");
                GlobalDiagnosticsContext.Set("section", "None");
                GlobalDiagnosticsContext.Set("exception", e.Message);
                GlobalDiagnosticsContext.Set("innerException", e.InnerException != null ? e.ToString() : "None");
                GlobalDiagnosticsContext.Set("relatedField", "None");
                logger.Error(e.Message);
            }
            

       
            return Ok(investmentApplication); // CreatedAtRoute("DefaultApi", new { id = investmentApplication.Id }, investmentApplication);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/DeleteInvestmentApplications/5
        [Route("api/DeleteInvestmentApplications")]
        [ResponseType(typeof(InvestmentApplication))]
        public async Task<IHttpActionResult> DeleteInvestmentApplication(int id)
        {
            InvestmentApplication investmentApplication = await db.InvestmentApplications.FindAsync(id);
            if (investmentApplication == null)
            {
                return NotFound();
            }

            db.InvestmentApplications.Remove(investmentApplication);
            await db.SaveChangesAsync();
            return Ok(investmentApplication);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InvestmentApplicationExists(int id)
        {
            return db.InvestmentApplications.Count(e => e.Id == id) > 0;
        }

        private static string GenerateMail(string clientName, string referenceNumber, string mailAddress, string mailType)
        {
            string body = "";

            if (mailType == "SupportingDocuments")
            {
                body += "<h2>Absa Virtual Investor Investment Request</h2>";
                body += "<p>Please find the supporting documentation attached. The user's details are as follows:</p>";
                body += "<b>User E-mail Address: </b>" + mailAddress + "<br />";
                body += "<b>User Reference Number: </b>" + referenceNumber + "<br />";
            }
            else
            {
                body += "<h2>Absa Virtual Investor Generated Lead</h2>";
                body += "<p>Generated lead:</p>";
                body += "<b>User Name: </b>" + clientName + "<br />";
                body += "<b>User E-mail Address: </b>" + mailAddress + "<br />";
            }

            body += "<br /><i>Absa Virtual Investor<i>";

            return body;
        }

        private static string GenerateContactInfoMail(Dictionary<string, string> contactInfo)
        {
            string body = $"<h2>Absa Virtual Investor - Request For Contact by an Agent</h2>" +
                          $"<p>The following client has shown an interest in completing the 'Wealth and Investment Management Application' after being redirected " +
                          $"to the online form by the Absa Virtual Investor site, but has elected to complete the form by having an Absa Agent calling him/her:</p>" +
                          $"<b>Client Reference Number: </b>{contactInfo["ClientReferenceNumber"]}<br />" +
                          $"<b>Suggested Product: </b>{contactInfo["ProductName"]}<br />" +
                          $"<b>Name: </b>{contactInfo["Name"]}<br />" +
                          $"<b>Surname: </b>{contactInfo["Surname"]}<br />" +
                          $"<b>Cell-phone Number: </b>{contactInfo["CountryCodeMobile"]} {contactInfo["Mobile"]}<br />" +
                          $"<b>Work Number: </b>{contactInfo["CountryCodeWork"]} {contactInfo["PhoneWork"]}<br />" +
                          $"<b>Home Number: </b>{contactInfo["CountryCodeHome"]} {contactInfo["PhoneHome"]}<br />" +
                          $"<b>E-mail: </b>{contactInfo["EMail"]}<br />" +
                          $"<br /><i>Absa Virtual Investor<i>";

            return body;
        }

        [Route("api/CountryList")]
        [ResponseType(typeof(Dictionary<string, string>))]
        public async Task<IHttpActionResult> GetListOfCountries()
        {
            return Ok(db.Countries.ToDictionary(c => c.PrintableName, c => c.PhoneCode));
        }

        [Route("api/BankNames")]
        [ResponseType(typeof(Dictionary<string, string>))]
        public async Task<IHttpActionResult> GetBankNames()
        {
            return Ok(db.Banks.ToDictionary(c => c.BankName, c => c.BankName));
        }

        [Route("api/SendContactDetails")]
        [ResponseType(typeof(Dictionary<string, string>))]
        public async Task<IHttpActionResult> SendContactDetails(Dictionary<string, string> contactInfo)
        {
            logger.Info("Attempting to send contact information for client with reference number: " + contactInfo["ClientReferenceNumber"]);
            try
            {
                Utilities.Configuration config = new Utilities.Configuration();
                string body = GenerateContactInfoMail(contactInfo);

                SendMail(config.ReplyEmail, config.LeadsDestination, string.Empty, string.Empty, "Absa Virtual Investor - Request For Contact by an Agent - UID: "+ contactInfo["ClientReferenceNumber"], body, config.SMTPServer, Int32.Parse(config.SMTPPort), config.SMTPUser, config.SMTPPass, true, null);

                logger.Info("Finished sending contact information for client with reference number: " + contactInfo["ClientReferenceNumber"]);
                return Ok(contactInfo);
            }
            catch(Exception ex)
            {
                logger.Info("Failed to send contact information for client with reference number: " + contactInfo["ClientReferenceNumber"]);
                return InternalServerError(ex);
            }
        }

        // POST: api/InvestmentApplications
        [Route("api/FinalInvestmentApplications")]
        [ResponseType(typeof(InvestmentApplication))]
        public async Task<IHttpActionResult> PostFinalInvestmentApplication(InvestmentApplication investmentApplication)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {

                if (!InvestmentApplicationExists(investmentApplication.Id))
                {
                    db.InvestmentApplications.Add(investmentApplication);
                }
                else
                {
                    db.Entry(investmentApplication).State = EntityState.Modified;
                }

                //DATE CONVERSIONS TO datetime INSTEAD OF datetime2:
                DateTime? DB_BirthDate = DateTime.Today;
                string db_BirthDate_string = "";
                bool updateDOB = false;
                if (investmentApplication.BirthDate != null)
                {
                    updateDOB = true;
                    DB_BirthDate = investmentApplication.BirthDate;
                    investmentApplication.BirthDate = null;
                    db_BirthDate_string = DB_BirthDate.Value.Year + "-" + DB_BirthDate.Value.Month + "-" + DB_BirthDate.Value.Day + " 00:00:00.000";
                }
                //END DATE CONVERSIONS
                db.SaveChanges();
                //if (investmentApplication.ClientAcceptTCsTimeStamp != null)
                //{
                    db.Database.ExecuteSqlCommand("UPDATE InvestmentApplications SET ClientAcceptTCsTimeStamp = GETDATE() WHERE Id=" + investmentApplication.Id + ";");
                //}
                //if (investmentApplication.ClientIndicatesSignatureTimeStamp != null)
                //{
                    db.Database.ExecuteSqlCommand("UPDATE InvestmentApplications SET ClientIndicatesSignatureTimeStamp = GETDATE() WHERE Id=" + investmentApplication.Id + ";");
                //}
                if (updateDOB)
                {
                    db.Database.ExecuteSqlCommand("UPDATE InvestmentApplications SET BirthDate = '" + db_BirthDate_string + "' WHERE Id=" + investmentApplication.Id + ";");
                }
                db.Database.ExecuteSqlCommand("UPDATE InvestmentApplications SET ApplicationRecieved = GETDATE() WHERE Id=" + investmentApplication.Id + ";");
                db.Database.ExecuteSqlCommand("UPDATE InvestmentApplications SET ApplicationSubmitted = GETDATE() WHERE Id=" + investmentApplication.Id + ";");
                db.SaveChanges();

                //RESTORE CONVFERSIONS:
                investmentApplication.BirthDate = DB_BirthDate;

                logger.Info("posting final application data for " + investmentApplication.ClientReferenceNumber);
                Robo.API.Utilities.Configuration conf = new Robo.API.Utilities.Configuration();

                string body = GenerateMail(investmentApplication.ClientFullName, investmentApplication.ClientReferenceNumber, investmentApplication.EMail, "SupportingDocuments");
                List<string> documents = new List<string>();
                FilesCreator creator = new FilesCreator();
                FileNames files = creator.writeApplicationZip(investmentApplication, investmentApplication.IDFileType, investmentApplication.POAFileType);
                documents.Add(files.ApplicationForm);

                documents.Add(files.RecordOfAdvice);

                documents.Add(files.RiskReport);

                documents.Add(files.ProofofResidence);

                documents.Add(files.TermsConditions);

                documents.Add(files.InvestmentInfo);

                documents.Add(files.CopyOfID);

                SendMail(conf.ReplyEmail, conf.MailDestination, string.Empty, string.Empty, "Absa Virtual Investor Investment Request - UID: "+ investmentApplication.ClientReferenceNumber, body, conf.SMTPServer, Int32.Parse(conf.SMTPPort), conf.SMTPUser, conf.SMTPPass, true, documents);

                //return CreatedAtRoute("DefaultApi", new { id = investmentApplication.Id }, investmentApplication);

                //update NameEmail: Set GenerateLead to false i.e do not send an Email to Unit Trust Lead Address.
                NameEmail lead = db.NameEmails.FirstOrDefault(l => l.ClientReferenceNumber == investmentApplication.ClientReferenceNumber);
                if (lead != null)
                {
                    lead.GenerateLead = false;
                    await db.SaveChangesAsync();
                }

            }catch(Exception ex)
            {
                logger.Error(ex, "posting final application data for " + investmentApplication.ClientReferenceNumber);

                throw ex;
            }
            return Ok(investmentApplication);
        }

        private string SendMail(string emailFrom, string emailTo, string cc, string bcc, string subject, string body, string host, int port, string username, string password, bool enableSSL, List<string> files)
        {
            //declare objects
            MailMessage message = new MailMessage();
            SmtpClient smtp = new SmtpClient(host, port) { DeliveryMethod = SmtpDeliveryMethod.Network};

            //add try exception
            try
            {
                //Add email address, cc
                message.From = new MailAddress(emailFrom);
                message.To.Add(new MailAddress(emailTo));
                if (cc != string.Empty)
                {
                    message.CC.Add(new MailAddress(cc));
                }

                if (bcc != string.Empty)
                {
                    message.Bcc.Add(new MailAddress(bcc));
                }

                //Add subject, body and attachment
                message.Subject = subject;
                message.Body = body;

                if (files != null)
                {
                    foreach(var file in files)
                    {
                        var attachment = new Attachment(file);
                        if (attachment != null)
                        {
                            message.Attachments.Add(attachment);
                        }
                    }

                }

                //Email Authentication
                if (username.Trim() != string.Empty)
                {
                    //enable SSL
                    smtp.EnableSsl = enableSSL;

                    NetworkCredential credentials = new NetworkCredential(username, password);
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credentials;
                    
                }


                //smtp port
                if (port > 0) { smtp.Port = port; }

                //check if html format is needed
                message.IsBodyHtml = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls12;
                //Send the email
                logger.Info(String.Format("sending email to {0}:{1} from {2} (smtp {3}:{4})", emailTo, emailFrom, subject, host, port));
                
                smtp.Send(message);
                logger.Info(String.Format("confirm email to {0}:{1} from {2} (smtp {3}:{4})", emailTo, emailFrom, subject, host, port));

            }
            catch (Exception ex)
            {
                logger.Info(String.Format("Failure sending email to {0}:{1} from {2} message: {3}", emailTo, emailFrom, subject, ex.Message));
                logger.Error(ex);
                return ex.Message.ToString();
            }

            return "";
        }
    }
}