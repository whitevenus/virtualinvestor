﻿using Newtonsoft.Json;
using RoboData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using NLog;

namespace Robo.API.Controllers
{

    class SmartRandResult
    {
        public bool result { get; set; }
        public string message { get; set; }
        public string uid { get; set; }
        public int score { get; set; }
        public string Investment { get; set; }
        public string RecommendedProductCode { get; set; }
        public string RecommendedProductName { get; set; }
        public string record { get; set; }
        public string profile { get; set; }
        public DateTime datetime { get; set; }
        public int A1 { get; set; }
        public int A2 { get; set; }
        public int A3 { get; set; }
        public int A4 { get; set; }
        public int A5 { get; set; }
        public int A6 { get; set; }
        public int A7 { get; set; }
        public int A8 { get; set; }
        public int A9 { get; set; }
        public int A10 { get; set; }
        public int A11 { get; set; }
        public int A12 { get; set; }
    }
    public class DownloadClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest w = base.GetWebRequest(uri);
            w.Timeout = 20 * 60 * 1000;
            var smUser = System.Configuration.ConfigurationManager.AppSettings["SmartRandAPIUsername"];
            var smSecret = System.Configuration.ConfigurationManager.AppSettings["SmartRandAPISecret"];
            //w.Headers.Add("X-IBM-Client-Id", smUser);
            //w.Headers.Add("X-IBM-Client-Secret", smSecret);
            
            return w;
        }
    }
    public class SmartRandAPIController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        HttpClient client;
        //The URL of the WEB API Service
        string url = System.Configuration.ConfigurationManager.AppSettings["SmartRandURI"];


        public SmartRandAPIController()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls12;
            
            client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var smUser = System.Configuration.ConfigurationManager.AppSettings["SmartRandAPIUsername"];
            var smSecret = System.Configuration.ConfigurationManager.AppSettings["SmartRandAPISecret"];
            client.Timeout = TimeSpan.FromMinutes(30);
            logger.Info("Smartrand credential " + smUser + ":" + smSecret);
            if(smSecret != string.Empty)
            {
                //no auth var credentials = Encoding.ASCII.GetBytes(smUser + ":" + smSecret);
                //no auth client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));
                client.DefaultRequestHeaders.Add("X-IBM-Client-Id", smUser);
                client.DefaultRequestHeaders.Add("X-IBM-Client-Secret", smSecret);
            }
            
        }

        private  byte[] GetFileData(string fileUrl)
        {
            logger.Info("Downloading from " + fileUrl);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls12;

            using (DownloadClient webClient = new DownloadClient())
            {
                Uri uri = new Uri(fileUrl);
                var smProxy = System.Configuration.ConfigurationManager.AppSettings["SmartRandAPIProxy"];
                if(smProxy != string.Empty)
                {
                    Uri proxyEndpoint = WebRequest.GetSystemWebProxy().GetProxy(uri);
                    webClient.Proxy = new WebProxy(smProxy, true);
                    webClient.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                }

                return webClient.DownloadData(uri);
            }
                
            try
            {

            }
            catch (Exception e)
            {
                Console.WriteLine("Error GetFileData:" + fileUrl + " : " + e.Message);
                logger.Info("Error Downloading from " + fileUrl);

                GlobalDiagnosticsContext.Set("uid", "None");
                GlobalDiagnosticsContext.Set("section", "GetFileData");
                GlobalDiagnosticsContext.Set("exception", "Error GetFileData:" + fileUrl + " : " + e.Message);
                GlobalDiagnosticsContext.Set("innerException", e.InnerException != null ? e.InnerException.Message : "None");
                GlobalDiagnosticsContext.Set("relatedField", "None");
                logger.Error(e.Message);

            }
            return new byte[0];
        }
        // api/InvestmentApplications/[InvestmentApplication]

        public async Task<InvestmentApplication> UpdateInvestmentApplicationFiles(RoboData.InvestmentApplication investmentApplication)
        {
            var InvestmentApplication = JsonConvert.SerializeObject(investmentApplication);
            var content = new StringContent(InvestmentApplication, Encoding.UTF8, "application/json");
            
            var fullUrl = url + "/SRengine/RecommendationDetail/ABSAlive,WeareA85A," + investmentApplication.ClientReferenceNumber;
            GlobalDiagnosticsContext.Set("uid", investmentApplication.ClientReferenceNumber);
            GlobalDiagnosticsContext.Set("section", "Request UpdateInvestmentApplicationFiles:Request");
            GlobalDiagnosticsContext.Set("exception", fullUrl);
            GlobalDiagnosticsContext.Set("innerException", "None");
            GlobalDiagnosticsContext.Set("relatedField", fullUrl);
            logger.Info("calling smart rand");

            try
            {

                // Do the actual request and await the response
                HttpResponseMessage responseMessage = await client.GetAsync(fullUrl);
                logger.Info("Smart rand check status");
                // If the response contains content we want to read it!
                if (responseMessage.IsSuccessStatusCode)
                {
                    logger.Info("Smart rand get content");
                    var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                    logger.Info("Success from Smart Rand :" + responseData);

                    var smr = JsonConvert.DeserializeObject<SmartRandResult>(responseData);
                    investmentApplication.RecordOfAdvice = GetFileData(smr.record);
                    investmentApplication.RiskReport = GetFileData(smr.profile);
                    investmentApplication.RecommendedProductCode = smr.RecommendedProductCode;
                    investmentApplication.RecommendedProductName = smr.RecommendedProductName;

                    return investmentApplication;
                }
                else
                {
                    logger.Info("Failed: Smart Rand :" + responseMessage.Content);

                }
    

            }
            catch (Exception ex)
            {
                GlobalDiagnosticsContext.Set("uid", investmentApplication.ClientReferenceNumber);
                GlobalDiagnosticsContext.Set("section", "UpdateInvestmentApplicationFiles:" + fullUrl);
                GlobalDiagnosticsContext.Set("exception", ex.Message);
                GlobalDiagnosticsContext.Set("innerException", ex.InnerException != null ? ex.ToString() : "None");
                GlobalDiagnosticsContext.Set("relatedField", fullUrl);
                logger.Error(ex.Message);
            }
            return investmentApplication;
        }

    }
}