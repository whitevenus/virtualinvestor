﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RoboData;

namespace Robo.API.Controllers
{/// <summary>
 /// Configurations
 /// </summary>
    //[IdentityBasicAuthentication] // Enable Basic authentication for this controller.
    [Authorize]
    public class ConfigurationsController : ApiController
    {
        private RoboDBEntities db = new RoboDBEntities();
        /// <summary>
        /// GetConfigurations()
        /// </summary>
        /// <returns></returns>
        // GET: api/Configurations
        public IQueryable<Configuration> GetConfigurations()
        {
            return db.Configurations;
        }
        /// <summary>
        /// GetConfiguration(int id)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Configurations/5
  
        [ResponseType(typeof(Configuration))]
        public async Task<IHttpActionResult> GetConfiguration(int id)
        {
            Configuration configuration = await db.Configurations.FindAsync(id);
            if (configuration == null)
            {
                return NotFound();
            }

            return Ok(configuration);
        }
        /// <summary>
        /// PutConfiguration(int id, Configuration configuration)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        // PUT: api/Configurations/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutConfiguration(int id, Configuration configuration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != configuration.Id)
            {
                return BadRequest();
            }

            db.Entry(configuration).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConfigurationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        /// <summary>
        /// PostConfiguration(Configuration configuration)
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        // POST: api/Configurations
        [ResponseType(typeof(Configuration))]
        public async Task<IHttpActionResult> PostConfiguration(Configuration configuration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Configurations.Add(configuration);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = configuration.Id }, configuration);
        }
        /// <summary>
        /// DeleteConfiguration(int id)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Configurations/5
        [ResponseType(typeof(Configuration))]
        public async Task<IHttpActionResult> DeleteConfiguration(int id)
        {
            Configuration configuration = await db.Configurations.FindAsync(id);
            if (configuration == null)
            {
                return NotFound();
            }

            db.Configurations.Remove(configuration);
            await db.SaveChangesAsync();

            return Ok(configuration);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ConfigurationExists(int id)
        {
            return db.Configurations.Count(e => e.Id == id) > 0;
        }
    }
}