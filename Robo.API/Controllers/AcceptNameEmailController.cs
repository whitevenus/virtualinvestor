﻿using Robo.API.Models;
using RoboData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Robo.API.Utilities;
using System.Security.Cryptography;
using NLog;
using System.Data.Entity.Validation;

namespace Robo.API.Controllers
{
    public class AcceptNameEmailController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private RoboDBEntities db = new RoboDBEntities();
        private static int LargeNumber = 93001531;        // PUT: api/AcceptNameEmail
        [ResponseType(typeof(string))]
        public async Task<IHttpActionResult> Put([FromBody] NameEmailView value)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            /*value = new NameEmailView();
            value.AcceptTsCs = 1;
            value.AcceptTsCsTimeStamp = DateTime.Now;
            value.Email = "vuyani.shabangu@gmail.com";
            value.Name = "Vuyani";
            value.ReadInvestInfo = 1;
            value.ReadInvestInfoTimeStamp = DateTime.Today;
            value.Id = 1;*/
            NameEmail data = new NameEmail
            {   AcceptTsCs = value.AcceptTsCs
            ,   AcceptTsCsTimeStamp =  value.AcceptTsCsTimeStamp
            ,   Email = value.Email
            //,   EmailSent = DateTime.Today
            ,   ClientReferenceNumber = "000000"
            ,   GenerateLead = true
            ,   Name = value.Name
            ,   ReadInvestInfo = value.ReadInvestInfo
            ,   ReadInvestInfoTimeStamp = value.ReadInvestInfoTimeStamp
            };
            try
            {
                logger.Info("Received NameEmail Request :" + value.Email);
                if (!NameEmailExists(value.Id))
                {
                    db.NameEmails.Add(data);
                    logger.Info("Boom 1 "+ data.AcceptTsCsTimeStamp.ToString());
                }
                else
                {
                    db.Entry(data).State = EntityState.Modified;
                    logger.Info("Boom 2");
                }
                logger.Info("Boom 3");
                db.SaveChanges();
                logger.Info("Boom 4");
                data.ClientReferenceNumber = ConvertEx.ToString(data.Id + 93001531, ConvertEx.Bases.Base36);
                db.Entry(data).State = EntityState.Modified;

                db.SaveChanges();

                //db.Database.ExecuteSqlCommand("UPDATE NameEmails SET AcceptTsCsTimeStamp = GETDATE() WHERE ClientReferenceNumber='" + data.ClientReferenceNumber + "';");
                //db.Database.ExecuteSqlCommand("UPDATE NameEmails SET EmailSent = GETDATE() WHERE ClientReferenceNumber='" + data.ClientReferenceNumber + "';");
                //db.Database.ExecuteSqlCommand("UPDATE NameEmails SET ReadInvestInfoTimeStamp = GETDATE() WHERE ClientReferenceNumber='" + data.ClientReferenceNumber + "';");
                db.SaveChanges();

                logger.Info("Boom 5");

                await new InvestmentApplicationsController().PostInitialInvestmentApplication(new InvestmentApplication
                {   ClientReferenceNumber=data.ClientReferenceNumber
                ,   RecommendedProductCode="00000"
                ,   RiskReport = new byte[0]
                ,   RecommendedProductName=""
                ,   RecordOfAdvice= new byte[0]
                ,   ApplicationRecieved = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day)
                ,   ClientAcceptTCsTimeStamp = null
                ,   ClientIndicatesSignatureTimeStamp = null
                ,   ApplicationSubmitted = null
                ,   BirthDate = null
                ,   PermitExpiryDate = null       
                ,   EMail = value.Email
                ,   Name = value.Name
                });
                logger.Info("Boom 6");

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                
                GlobalDiagnosticsContext.Set("uid", data.ClientReferenceNumber);
                GlobalDiagnosticsContext.Set("section", "None");
                GlobalDiagnosticsContext.Set("exception", e.Message + "BOO");
                GlobalDiagnosticsContext.Set("innerException", e.InnerException != null ? e.ToString() : "None");
                GlobalDiagnosticsContext.Set("relatedField", "None");
                logger.Error(e.Message);

                return BadRequest();
            }
     

            logger.Info("Completed NameEmail Request :" + data.ClientReferenceNumber);

            return Ok(data.ClientReferenceNumber);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NameEmailExists(int id)
        {
            return db.NameEmails.Count(e => e.Id == id) > 0;
        }
    }
}
