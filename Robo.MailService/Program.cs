﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robo.MailService
{
    class Program
    {
        static void Main(string[] args)
        {
            if (!File.Exists("log.txt"))
            {
                File.Create("log.txt").Dispose();
            }
            try
            {
   
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log("--Start Sending--", w);
                }
                Mailer _mailer = new Mailer();
                _mailer.SendLeads();
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log("--End Sending--", w);
                }
            }catch(Exception ex)
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log(ex.Message, w);
                }
            }

         
        }
        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            w.WriteLine("  :");
            w.WriteLine("  :{0}", logMessage);
            w.WriteLine("-------------------------------");
        }

        public static void DumpLog(StreamReader r)
        {
            string line;
            while ((line = r.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }
        }
    }
}
