﻿using RoboData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Robo.Leads.Service
{
    public class Mailer
    {
        System.Diagnostics.EventLog log;
        public Mailer(System.Diagnostics.EventLog log)
        {
            this.log = log;
        }
        public static string GenerateMail(string clientName, string referenceNumber, string mailAddress, string mailType)
        {
            string body = "";

            if (mailType == "SupportingDocuments")
            {
                body += "<h2>Absa Virtual Investor Investment Request</h2>";
                body += "<p>Please find the supporting documentation attached. The user's details are as follows:</p>";
                body += "<b>User E-mail Address: </b>" + mailAddress + "<br />";
                body += "<b>User Reference Number: </b>" + referenceNumber + "<br />";
            }
            else
            {
                body += "<h2>Absa Virtual Investor Pending Application</h2>";
                body += "<p>Generated lead:</p>";
                body += "<b>User Name: </b>" + clientName + "<br />";
                body += "<b>User E-mail Address: </b>" + mailAddress + "<br />";
            }

            body += "<br /><i>Absa Virtual Investor<i>";

            return body;
        }
        public bool SendMail(string emailFrom, string emailTo, string cc, string bcc, string subject, string body, string host, int port, string username, string password, bool enableSSL, List<string> files)
        {
            //declare objects
            MailMessage message = new MailMessage();
            SmtpClient smtp = new SmtpClient(host, port);

            //add try exception
            try
            {
                //Add email address, cc
                message.From = new MailAddress(emailFrom);
                message.To.Add(new MailAddress(emailTo));
                if (cc != string.Empty)
                {
                    message.CC.Add(new MailAddress(cc));
                }

                if (bcc != string.Empty)
                {
                    message.Bcc.Add(new MailAddress(bcc));
                }

                //Add subject, body and attachment
                message.Subject = subject;
                message.Body = body;


                foreach (var file in files)
                {
                    var attachment = new Attachment(file);
                    if (attachment != null)
                    {
                        message.Attachments.Add(attachment);
                    }
                }


                //Email Authentication
                if (username.Trim() != string.Empty && password.Trim() != string.Empty)
                {
                    NetworkCredential basicAuthentication = new NetworkCredential(username, password);
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = basicAuthentication;
                }

                //enable SSL
                smtp.EnableSsl = enableSSL;

                //smtp port
                if (port > 0) { smtp.Port = port; }

                //check if html format is needed
                message.IsBodyHtml = true;
                ServicePointManager.ServerCertificateValidationCallback =
                delegate (object s, X509Certificate certificate,
                         X509Chain chain, SslPolicyErrors sslPolicyErrors)
                {
                    if (sslPolicyErrors == SslPolicyErrors.None)
                        return true;
                    //log.WriteEntry("Warning Sending Mail even if certificate policy " );

                    return true;
                };
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls12;
                //Send the email
                smtp.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendMail Error: " + ex.Message.ToString());
                log.WriteEntry("Error (1) Sending Mail:" + ex.Message, EventLogEntryType.Error);
                if(ex.InnerException != null)
                    log.WriteEntry("Error (2:inner) Sending Mail:" + ex.InnerException.Message, EventLogEntryType.Error);
            }

            return false;
        }
        public async void SendLeads()
        {
            RoboDBEntities db = new RoboDBEntities();
            List<NameEmail> Leads = db.NameEmails.Where(l => l.GenerateLead == true).ToList();
            foreach (NameEmail lead in Leads)
            {
                string body = GenerateMail(lead.Name, lead.ClientReferenceNumber, lead.Email, "Lead");
                bool success = SendMail(System.Configuration.ConfigurationManager.AppSettings["SMTPFromMail"],
                    System.Configuration.ConfigurationManager.AppSettings["MailDestination"],
                    string.Empty,
                    string.Empty,
                    System.Configuration.ConfigurationManager.AppSettings["MailSubject"] + " - UID: "+ lead.ClientReferenceNumber,
                    body,
                    System.Configuration.ConfigurationManager.AppSettings["SMTPServer"],
                    Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["SMTPPort"]),
                    System.Configuration.ConfigurationManager.AppSettings["SMTPUser"],
                    System.Configuration.ConfigurationManager.AppSettings["SMTPPass"],
                    true,
                     new List<string>());
                if (success)
                {
                    lead.EmailSent = DateTime.Now;
                    lead.GenerateLead = false;
                }

            }

            await db.SaveChangesAsync();
        }


    }
}
