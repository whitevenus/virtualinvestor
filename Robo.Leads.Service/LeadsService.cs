﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Robo.Leads.Service
{
    public partial class LeadsSchduler: ServiceBase
    {
        private System.Diagnostics.EventLog eventLog1;

        void PerformScheduleWork()
        {
            try
            {

                Mailer _mailer = new Mailer(eventLog1);
                _mailer.SendLeads();
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("Error moving files:" + ex.Message, EventLogEntryType.Error);
            }


        }
        private Timer Scheduler;
        public LeadsSchduler()
        {
            InitializeComponent();
            string source = "RoboLeadsSource";
            string logName = "RoboLeadsNewLog";
            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists(source))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    source, logName);
            }
            eventLog1.Source = source;
            eventLog1.Log = logName;

        }
        public void ScheduleService()
        {
            try
            {
                Scheduler = new Timer(new TimerCallback(SchedularCallback));
                string mode = "DAILY"; // ConfigurationManager.AppSettings["Mode"].ToUpper();
                
                //Set the Default Time.
                DateTime scheduledTime = DateTime.MinValue;

                if (mode == "DAILY")
                {
                    PerformScheduleWork();
                    //Get the Scheduled Time from AppSettings.
                    scheduledTime = DateTime.Parse(System.Configuration.ConfigurationManager.AppSettings["ScheduledTime"]);
                    //scheduledTime = DateTime.Parse("16:00");
                    if (DateTime.Now > scheduledTime)
                    {
                        PerformScheduleWork();
                        //If Scheduled Time is passed set Schedule for the next day.
                        scheduledTime = scheduledTime.AddDays(1);
                    }
                }
                
                TimeSpan timeSpan = scheduledTime.Subtract(DateTime.Now);
                string schedule = string.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);

                eventLog1.WriteEntry("ROBO Leads scheduled to run after: " + schedule + " {0}");

                //Get the difference in Minutes between the Scheduled and Current Time.
                int dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);

                //Change the Timer's Due Time.
                Scheduler.Change(dueTime, Timeout.Infinite);
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("ROBO Leads Service Error : " + ex.Message + ex.StackTrace,EventLogEntryType.Error);

                //Stop the Windows Service.
                using (System.ServiceProcess.ServiceController serviceController = new System.ServiceProcess.ServiceController("SimpleService"))
                {
                    serviceController.Stop();
                }
            }
        }

        private void SchedularCallback(object e)
        {
            eventLog1.WriteEntry("ROBO Leads Start Service");
            this.ScheduleService();
        }
        protected override void OnStart(string[] args)
        {
            this.ScheduleService();
        }

        protected override void OnStop()
        {
            this.Scheduler.Dispose();
        }
        internal void TestStartupAndStop(string[] args)
        {
            this.OnStart(args);
            Console.ReadLine();
            this.OnStop();
        }
    }
}
