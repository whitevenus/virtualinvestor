﻿using Robo.API.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZipTester
{
    class Program
    {
        static void Main(string[] args)
        {
            string testRoot = "C:\\dev\\aims\\absa-virtual-advisor\\ZipTester\\TestContent";
            string outputRoot = "C:\\dev\\aims\\absa-virtual-advisor\\ZipTester\\TestOutput";
            DocZip appArchive = new DocZip();
            //Additional Investment information - WIP.PDF
            //app form.pdf
            //files.txt
            //ID.pdf
            //Proof of Residence.pdf
            //Record of Advice. Individual Investor.pdf
            //Risk Tolerance Report.pdf
            //ts and cs.pdf

            FileNames files = new FileNames
            {   ApplicationForm = testRoot + "\\Docs\\" + "app form.pdf"
            ,   CopyOfID = testRoot + "\\Docs\\ID.pdf"
            ,   InvestmentInfo = testRoot + "\\Docs\\Additional Investment information - WIP.PDF"
            ,   ProofofResidence = testRoot + "\\Docs\\Proof of Residence.pdf"
            ,   RecordOfAdvice = testRoot + "\\Docs\\Record of Advice. Individual Investor.pdf"
            ,   RiskReport = testRoot + "\\Docs\\Risk Tolerance Report.pdf"
            ,   TermsConditions = testRoot + "\\Docs\\ts and cs.pdf"
            };
            DocMeta docMeta = new DocMeta
            {   ApplicationReferenceNumber = "10V10C"
            ,   EmailAddress = "rest@assured"
            ,   IdentityNumber = "9009095353984"
            ,   Name = "J VON NEUMAN"
            };
            appArchive.zipApplicationDirectory(outputRoot, testRoot, docMeta, files);
        }
    }
}
