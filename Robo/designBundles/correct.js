

/**
 * Accordion
 */

/* global Modernizr */

(function ($) {
    'use strict';

    function Accordion($el) {
        this.$el = $el;
        this.$container = this.$el.find('.accordion-container');
        this.$slides = this.$container.children();
        this.$heads = this.$slides.children('.accordion-head');
        this.recountHeight = true;

        this.bindUIEvents();

    }

    Accordion.prototype.bindUIEvents = function () {
        var root = this;

        function getMarkerId($slide) {
            var id = $slide.find('.accordion-head').attr('id');
            return id ? id.replace('marker-', '') : null;
        }

        function hide($slide) {
            var $content = $slide.children('.accordion-content-wrapper');

            //$slide.removeClass('is-active');
            //$content.css('max-height', 0);
        }

        function resizeAccordionItems($items) {
            function resize() {
                $items.each(function () {
                    var $item = $(this),
                        height = $item[0].scrollHeight;

                    $item.css('max-height', height);
                });
            }

            setTimeout(resize, 0);
        }

        function show($slide) {
            var $inactivateSlide = $slide.siblings('.is-active'),
                $showContent = $slide.children('.accordion-content-wrapper');

            if ($slide.length === 0) {
                return null;
            }

            if (Modernizr.csstransitions) {
                $showContent.off('transitionend');
            }

            $slide.addClass('is-active');
            resizeAccordionItems($showContent);


            if ($inactivateSlide.length === 0) {
                $slide.addClass('is-animated');
            }
        }

        function onHeaderClick() {
            /*jshint validthis:true */

            var $header = $(this),
                $slide = $header.parent();

            if ($slide.hasClass('is-active')) {
                hide($slide);
            } else {
                show($slide);
            }

        }

        this.$container.on('click', '.accordion-head', onHeaderClick);
        this.$container.on('click', '.accordion-head > a', function (e) {
            e.preventDefault();
        });
        if (this.recountHeight) {
            var recountHeightTimer;
            var that = this;
            $(window).on('resize orientationchange', function () {
                if (recountHeightTimer) clearTimeout(recountHeightTimer);
                recountHeightTimer = setTimeout(function ()
                { show(that.$slides.filter('.is-active')); }
                , 300);
            });
        }

    };

    $('.accordion').each(function () {
        new Accordion($(this));
    });

})(jQuery);
