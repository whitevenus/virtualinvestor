﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Robo.Startup))]
namespace Robo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
