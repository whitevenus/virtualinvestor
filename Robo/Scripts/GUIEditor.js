﻿function tempResToggle() {
    if (document.getElementById("TempRes-Yes").checked === true) {
        document.getElementById("tempResNum").className = document.getElementById("tempResNum").className.replace(/(?:^|\s)hide(?!\S)/g, ' unhide');
        document.getElementById("permitExpiryDate").className = document.getElementById("permitExpiryDate").className.replace(/(?:^|\s)hide(?!\S)/g, ' unhide');
        document.getElementById("accordion-content-1-1").style.maxHeight = "inherit";
    } else if (document.getElementById("TempRes-No").checked === true) {
        document.getElementById("tempResNum").className = document.getElementById("tempResNum").className.replace(/(?:^|\s)unhide(?!\S)/g, ' hide');
        document.getElementById("permitExpiryDate").className = document.getElementById("permitExpiryDate").className.replace(/(?:^|\s)unhide(?!\S)/g, ' hide');
    }
}

function taxResToggle() {
    if (document.getElementById("TaxRes-No").checked === true) {
        document.getElementById("addPassNumField").className = document.getElementById("addPassNumField").className.replace(/(?:^|\s)hide(?!\S)/g, ' unhide');
    } else if (document.getElementById("TaxRes-Yes").checked === true) {
        document.getElementById("addPassNumField").className = document.getElementById("tempResNum").className.replace(/(?:^|\s)unhide(?!\S)/g, ' hide');
    }
}

function caretToggle(accordion) {
    var carets = document.getElementsByClassName("accordion-head-chevron");

    if (hasActiveClass(accordion)) {
        for(i = 0; i < carets.length; i++){
            if (carets[i].parentElement.id === accordion.id) {
                carets[i].src = "/icons/accordion-chevron.svg";
                break;
            }
        }
    } else {
        for (i = 0; i < carets.length; i++) {
            if (carets[i].parentElement.id === accordion.id) {
                carets[i].src = "/icons/accordion-chevron-up.svg";
                break;
            }
        }
    }
}

function hasActiveClass(accordion) {
    var classes = accordion.parentElement.className.split(" ");

    for (j = 0; j < classes.length; j++) {
        if (classes[j] === "is-active") {
            return true;
        }
    }
    return false;
}

function dropDownBlack(dropdown) {
    if (dropdown !== null) {
        var val = dropdown.options[dropdown.selectedIndex].value;

        if (val === "") {
            dropdown.style.color = "#919191";
        } else {
            dropdown.style.color = "#00395c";
        }
    }
}

function dateBlack(date) {
    if (date !== null) {
        date.style.color = "#00395C";
    }
}

function numberOnlyToggle() {
    var num = document.getElementById("IDPassNum");
    
    if (num.type === "number") {
        num.type = "";
        num.style.marginTop = "4.69px";
        num.style.fontSize = "16px";
    } else {
        num.type = "number";
        num.style.marginTop = "4.69px";
        num.style.fontSize = "16px";
    }
}

function numberOnly() {
    var num = document.getElementById("IDPassNum");
    num.type = "number";
    num.style.marginTop = "4.69px";
    num.style.fontSize = "16px";
}

function notNumberOnly() {
    var num = document.getElementById("IDPassNum");
    num.type = "";
    num.style.marginTop = "4.69px";
    num.style.fontSize = "16px";
}

function dropDownCaretSwitch(drop) {
    if (drop.style.backgroundImage === 'url("/icons/accordion-chevron.svg")') {
        drop.style.backgroundImage = "url(/icons/accordion-chevron-up.svg)";
    } else {
        drop.style.backgroundImage = "url(/icons/accordion-chevron.svg)";
    }
}