﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Robo.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using RoboData;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using NLog;
using Microsoft.Win32;

namespace Robo.Controllers
{
    public class ProcessController : Controller
    {
        private static string GetContactError()
        {
            return "An error occurred while processing your request. Please contact " + System.Configuration.ConfigurationManager.AppSettings["HelpDeskName"] + " on " + System.Configuration.ConfigurationManager.AppSettings["HelpDeskNumber"] + " for further assistance.<br />Return to the <a href='http://www.absa.co.za'>Absa Homepage</a>.";
        }
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private static bool isFutureDate(DateTime date)
        {
            int cmp = DateTime.Compare(DateTime.Now, date);
            return cmp < 0;
        }

        private static bool isValidString(string stringString)
        {
            if (stringString == null || stringString == string.Empty || stringString == "") return false;
            return true;
        }

        private static bool isValidNumber(string numberString)
        {
            if (numberString == null || numberString == string.Empty || numberString == "") return false;
            //numberString = numberString.Replace(",", CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator);
            decimal result;
            bool test = decimal.TryParse(numberString, out result);
            return test;

        }

        private static bool isValidNumber(Nullable<decimal> number)
        {
            if (number == null)
                return false;
            return isValidNumber(number.ToString());
        }
        private static bool isValidInteger(string numberString)
        {
            if (numberString == null || numberString == string.Empty || numberString == "") return false;
            long result;
            return long.TryParse(numberString, out result);

        }
        private static bool isValidDate(string dateString)
        {
            if (dateString == null || dateString == string.Empty || dateString == "") return false;
            CultureInfo provider = CultureInfo.InvariantCulture;
            DateTime birthDate;
            return DateTime.TryParse(dateString, provider, DateTimeStyles.None, out birthDate);
        }
        private static bool isValidDate(string dateString, out DateTime date)
        {
            date = DateTime.Now;
            if (dateString == null || dateString == string.Empty || dateString == "") return false;
            CultureInfo provider = CultureInfo.InvariantCulture;

            return DateTime.TryParse(dateString, provider, DateTimeStyles.None, out date);
        }
        // GET: Process
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetFile(int fileNumber, string fileType)
        {
            InvestmentApplication iva = (InvestmentApplication)Session["InvestmentApplication"];
            byte[] source = null;
            string fileName = "";
            if (fileNumber == 1)
            {
                source = iva.RiskReport;
                fileName = "RiskReport.pdf";
            }
            if (fileNumber == 2)
            {
                source = iva.RecordOfAdvice;
                fileName = "RecordOfAdvice.pdf";
            }
            Response.AppendHeader("Content-Disposition", "inline; filename=" + fileName);
            return File(source, fileType); //"application/pdf"
        }
        public FileStreamResult GetPDF()
        {
            FileStream fs = new FileStream(Server.MapPath(@"~\Content\Files\TC.pdf"), FileMode.Open, FileAccess.Read);
            Response.AppendHeader("Content-Disposition", "inline; filename=Terms and Conditions");
            return File(fs, "application/pdf");
        }

        // Determines the default extension for a given mime type:
        private static string GetFileExtension(string type)
        {
            string result;
            RegistryKey key;
            object value;

            key = Registry.ClassesRoot.OpenSubKey(@"MIME\Database\Content Type\" + type, false);
            value = key != null ? key.GetValue("Extension", null) : null;
            result = value != null ? value.ToString() : string.Empty;

            return result;
        }

        public async Task<JsonResult> Upload()
        {
            string retMessage = "";
            try
            {


                if (Request.Files.Count > 0)
                {
                    foreach (string file in Request.Files)
                    {
                        var _file = Request.Files[file];

                        InvestmentApplication ivas = (InvestmentApplication)Session["InvestmentApplication"];
                        try
                        {
                            var _file_type = _file.ContentType;
                            //var _file_extension = GetFileExtension(_file_type);
                            var _file_extension = Path.GetExtension(_file.FileName);
                            if (!(_file_extension == ".PDF" || _file_extension == ".pdf" || _file_extension == ".jpg" || _file_extension == ".jpeg" || _file_extension == ".png" || _file_extension == ".PNG"))
                            {
                                throw new Exception(" Please only upload PDF documents or images.");
                            }

                            if (!(_file_type == "application/pdf" || _file_type == "image/jpeg" || _file_type == "image/png"))
                            {
                                throw new Exception(" Please only upload PDF documents or images.");
                            }

                            RoboAPIController RoboAPI = new RoboAPIController();
                            InvestmentApplication iva = await RoboAPI.getInvestmentApplication(ivas.ClientReferenceNumber);

                            int mupload = 750;
                            int.TryParse(System.Configuration.ConfigurationManager.AppSettings["MaxUploadSize"], out mupload);
                            mupload *= 1024;
                            using (var binaryReader = new BinaryReader(_file.InputStream))
                            {
                                if (Request.Params["FileType"] == "ID_Document")
                                {
                                    if (_file.ContentLength > mupload)
                                    {
                                        throw new Exception("The ID Document is too large");
                                    }
                                    iva.IDCopy = binaryReader.ReadBytes(_file.ContentLength);
                                    iva.IDFileType = _file_extension;
                                }
                                if (Request.Params["FileType"] == "POA")
                                {
                                    if (_file.ContentLength > mupload)
                                    {
                                        throw new Exception("The Proof of Address Document is too large");
                                    }
                                    iva.ProofOfAdress = binaryReader.ReadBytes(_file.ContentLength);
                                    iva.POAFileType = _file_extension;
                                }
                            }

                            iva = await RoboAPI.putInvestmentApplication(iva);
                            if (iva != null)
                                Session["InvestmentApplication"] = iva;
                            retMessage = "File Uploaded!";
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.Message);
                            if (ex.Message.Contains("Maximum request length exceeded"))
                            {
                                retMessage = "Attempt to upload a document exceeding the maximum length";
                            }
                            else
                            {
                                retMessage = "ERROR:" + ex.Message;
                                GlobalDiagnosticsContext.Set("uid", ivas.ClientReferenceNumber);
                                GlobalDiagnosticsContext.Set("section", "File Upload.");
                                GlobalDiagnosticsContext.Set("exception", ex.Message);
                                GlobalDiagnosticsContext.Set("innerException", ex.ToString());
                                GlobalDiagnosticsContext.Set("relatedField", ex.Message);
                            }
                            return Json(retMessage);
                        }
                    }
                }
                else
                {
                    retMessage = "You have not specified a file.";
                }
            }
            catch (Exception ex)
            {

                logger.Error(ex.Message);
                if (ex.Message.Contains("Maximum request length exceeded"))
                {
                    retMessage = "Attempt to upload a document exceeding the maximum length";
                }
                else
                {
                    retMessage = "ERROR:" + ex.Message;
                }

                return Json(retMessage);
            }
            return Json(retMessage);


        }

        // GET: Process/Details/5

        private bool InValidateNull(string value)
        {
            if (value == null || value == string.Empty)
                return true;
            else
                return false;
        }
        public async Task<ActionResult> DetailsBack(FormCollection collection, string ClientReferenceNumber = "")
        {
            var ProcessStage = collection["ProcessStage"];
            DetailsViewModel CurrentProcess = new DetailsViewModel();
            try
            {
                int stage = int.Parse(ProcessStage);
                if (stage > 1) --stage;
                collection["ProcessStage"] = stage.ToString();
                return await Details(collection, ClientReferenceNumber);

            }
            catch (Exception ex)
            {
                CurrentProcess.ProcessStage = 99;
                CurrentProcess.ProcessError = GetContactError(); // "ERROR: " + ex.Message;
            }
            return View(CurrentProcess);
        }
        private string FormatUploadLimit()
        {
            Double mupload = 750.0;
            Double.TryParse(System.Configuration.ConfigurationManager.AppSettings["MaxUploadSize"], out mupload);
            Double v = mupload;
            return v.ToString("0");


        }
        private async Task UpdateFileStatuses(DetailsViewModel CurrentProcess, string ClientReferenceNumber)
        {
            RoboAPIController RoboAPI = new RoboAPIController();
            InvestmentApplication userapp = await RoboAPI.getInvestmentApplication(ClientReferenceNumber);
            if (CurrentProcess != null && userapp != null)
            {
                CurrentProcess.IDUploaded = userapp.IDCopy != null;
                CurrentProcess.PORUploaded = userapp.ProofOfAdress != null;

            }

        }

        // Method that captures the user's basic contact details, and then sends an e-mail containing those details to a specified email address:
        public async Task<JsonResult> Contact(FormCollection collection)
        {
            try
            {
                RoboAPIController RoboAPI = new RoboAPIController();

                Dictionary<string, string> contactInfo = new Dictionary<string, string>();
                contactInfo.Add("ClientReferenceNumber", collection["ClientReferenceNumber"]);
                contactInfo.Add("ProductName", collection["ProductName"]);
                contactInfo.Add("Name", collection["CallMeBack_Name"]);
                contactInfo.Add("Surname", collection["CallMeBack_Surname"]);
                contactInfo.Add("CountryCodeWork", collection["CallMeBack_CountryCodeWork"]);
                contactInfo.Add("PhoneWork", collection["CallMeBack_PhoneWork"]);
                contactInfo.Add("CountryCodeHome", collection["CallMeBack_CountryCodeHome"]);
                contactInfo.Add("PhoneHome", collection["CallMeBack_PhoneHome"]);
                contactInfo.Add("CountryCodeMobile", collection["CallMeBack_CountryCodeMobile"]);
                contactInfo.Add("Mobile", collection["CallMeBack_Mobile"]);
                contactInfo.Add("EMail", collection["CallMeBack_EMail"]);

                foreach (KeyValuePair<string, string> entry in contactInfo)
                {
                    if (entry.Key == "Name" && entry.Value == "") return Json("Please enter your name.");
                    if (entry.Key == "Surname" && entry.Value == "") return Json("Please enter your surname.");

                    if (entry.Key == "Mobile" && entry.Value == "") return Json("Please enter your mobile number.");
                    if (entry.Key == "Mobile" && entry.Value != "")
                    {
                        if (!isValidNumber(entry.Value) || entry.Value.Length < 10) return Json("Please ensure that your mobile number is ten digits long.");
                        if (contactInfo["CountryCodeMobile"] == "") return Json("Please enter a country code for your mobile number");
                    }

                    if (entry.Key == "PhoneWork" && entry.Value != "")
                    {
                        if (!isValidNumber(entry.Value) || entry.Value.Length < 10) return Json("Please ensure that your work number is ten digits long.");
                        if (contactInfo["CountryCodeWork"] == "") return Json("Please enter a country code for your work number");
                    }

                    if (entry.Key == "PhoneHome" && entry.Value != "")
                    {
                        if (!isValidNumber(entry.Value) || entry.Value.Length < 10) return Json("Please ensure that your home number is ten digits long.");
                        if (contactInfo["CountryCodeHome"] == "") return Json("Please enter a country code for your home number");
                    }

                    if (entry.Key == "EMail" && entry.Value != "")
                    {
                        if ((!entry.Value.Contains("@") || !entry.Value.Contains("."))) return Json("Please enter a valid email address.");
                    }

                    //if (entry.Value == "") return Json("Please complete all fields.");

                    //if (entry.Key == "PhoneWork" || entry.Key == "PhoneHome" || entry.Key == "Mobile")
                    //{
                    //    if (!isValidNumber(entry.Value) || entry.Value.Length < 10) return Json("Please ensure that all contact numbers are ten digits long.");
                    //}

                    //if (entry.Key == "EMail" && (!entry.Value.Contains("@") || !entry.Value.Contains("."))) return Json("Please enter a valid email address.");
                }

                RoboAPI.sendContactDetails(contactInfo);
                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }


        public async Task<ActionResult> Details(FormCollection collection, string ClientReferenceNumber = "", string otherStage = "")
        {
            if (ClientReferenceNumber == "")
            {
                ClientReferenceNumber = collection["ClientReferenceNumber"];
            }

            DetailsViewModel CurrentProcess = null;
            RoboAPIController RoboAPI = new RoboAPIController();
            if (otherStage != "")
            {
                int stage = int.Parse(otherStage);
                collection["ProcessStage"] = otherStage;
                CurrentProcess = await Transfer(collection, ClientReferenceNumber);
                CurrentProcess.ProcessStage = stage;
            }
            else
            {
                CurrentProcess = new DetailsViewModel();
                CurrentProcess.ClientReferenceNumber = ClientReferenceNumber;
            }
            CurrentProcess.UploadLimit = FormatUploadLimit();

            if (CurrentProcess.IncomeTaxDetails == null || CurrentProcess.IncomeTaxDetails.Count == 0)
            {
                CurrentProcess.IncomeTaxDetails = new List<TaxDetailsViewModel>();
                CurrentProcess.IncomeTaxDetails.Add(new TaxDetailsViewModel { IncomeTaxNumber = "", CountryOfIssue = "" });
            }

            if (CurrentProcess.AdditionalPassportDetails == null || CurrentProcess.AdditionalPassportDetails.Count == 0)
            {
                CurrentProcess.AdditionalPassportDetails = new List<AdditionalPassportDetailsModel>();
                CurrentProcess.AdditionalPassportDetails.Add(new AdditionalPassportDetailsModel { PassportNumber = "", CountryOfIssue = "", DateOfIssue = DateTime.Today });
            }

            var ID = collection["ID"];
            var ProcessStage = collection["ProcessStage"];
            if (ID != null && ID != string.Empty && ID != ClientReferenceNumber)
            {
                ClientReferenceNumber = ID;
            }
            if (Session["InvestmentApplication"] == null)
            {
                InvestmentApplication userapp = await RoboAPI.getInvestmentApplication(ClientReferenceNumber);
                if (userapp != null)
                {
                    Session["InvestmentApplication"] = userapp;
                    CurrentProcess.IDUploaded = userapp.IDCopy != null;
                    CurrentProcess.PORUploaded = userapp.ProofOfAdress != null;
                }

            }

            Dictionary<string, string> bankNames = await RoboAPI.getBankNames();

            List<string> bankOptions = bankNames.Select(c => c.Key).ToList();
            bankOptions.Insert(0, "Select Bank");
            List<SelectListItem> banks = (from c in bankOptions
                                          select new SelectListItem { Text = c, Value = c }).ToList();

            banks[0].Value = null;
  
            ViewBag.Banks = (IEnumerable<SelectListItem>)banks; //new SelectList(countries, "Value", "Text");



            Dictionary<string, string> countryCodes = await RoboAPI.getListOfCountries();

            // Creates list of countries for drop-down list:
            List<string> countryOptions = countryCodes.Select(c => c.Key).ToList();
            countryOptions.Remove("South Africa");
            countryOptions.Sort();
            countryOptions.Insert(0, "South Africa");
            List<SelectListItem> countries = (from c in countryOptions
                                              select new SelectListItem { Text = c, Value = c }).ToList();
            SelectListItem seperator = new SelectListItem { Text = "", Value = "", Disabled = true };
            countries.Insert(1, seperator);
            ViewBag.Countries = (IEnumerable<SelectListItem>)countries; //new SelectList(countries, "Value", "Text");

            // Creates list of calling codes for drop-down list:
            Dictionary<string, string> callingCodes = countryCodes.Where(c => c.Value != "0").OrderBy(c => c.Key).ToDictionary(c => c.Key, c => c.Value);
            callingCodes.Remove("South Africa");
            List<SelectListItem> codes = (from c in callingCodes
                                          select new SelectListItem { Text = c.Key + " (+" + c.Value + ")", Value = "(+" + c.Value + ")" }).ToList();
            codes.Insert(0, seperator);
            codes.Insert(0, new SelectListItem { Text = "South Africa (+27)", Value = "(+27)" });
            ViewBag.CallingCodes = (IEnumerable<SelectListItem>)codes;

            await UpdateFileStatuses(CurrentProcess, ClientReferenceNumber);
            try
            {
                // Removed for now:
                //if (otherStage != "")
                //{
                //    CurrentProcess.ProcessBtnMessage = "Next >>";
                //    return View(CurrentProcess);
                //}

                if (ProcessStage != null)
                {
                    CurrentProcess.ProcessStage = int.Parse(ProcessStage);
                }

                //Process Validation
                bool processValid = true;

                if (CurrentProcess.ProcessStage == 1)
                {

                    CurrentProcess.ProcessError = "";
                    InvestmentApplication userapp = await RoboAPI.getInvestmentApplication(ClientReferenceNumber);
                    userapp.ClientAcceptTCs = true;
                    userapp.ClientAcceptTCsTimeStamp = null; //DateTime.Today;
                    userapp.ClientIndicatesSignature = true;
                    userapp.ClientIndicatesSignatureTimeStamp = null; //DateTime.Now;
                    userapp.ClientFullName = collection["ClientFullName"];
                    userapp.FundManagersConsent = false;
                    userapp.AcceptSignature = false;
                    userapp.AcceptTerms = false;
                    userapp.DeclarationAccept = false;
                    userapp = await RoboAPI.putInvestmentApplication(userapp);

                }

                if (CurrentProcess.ProcessStage == 2)
                {
                    var validationList = new List<string>
                    {
                        //"ClientType",
                        "Title2",
                        "Name",
                        "Surname",
                        "Gender",
                        "MaritalStatus",
                        "IDPassportNumber",
                        "IDType",
                        "BirthDate",
                        "TempResident",
                        "TaxResident",
                        "Adress",
                        //"PostResAddress",
                        "City",
                        "Province",
                        "PostalCode",
                        //"CountryCodeWork",
                        "PhoneWork",
                        //"CountryCodeMobile",
                        "Mobile",
                        "EMail",
                        "KinName",
                        "KinSurname",
                        //"CountryCodeKin",
                        "KinMobile"
                    };
                    string s = "er";
                    if (Regex.IsMatch("dd2#", @"^[a-zA-Z0-9]+$", RegexOptions.ECMAScript))
                        logger.Info("GGG: Good string!");
                    else
                        logger.Info("GGG: Bad string!");

                    var invalidList = validationList.Where(x => InValidateNull(collection[x])).ToList();
                    if (invalidList.Count > 0)
                    {
                        processValid = false;
                        CurrentProcess.ProcessError = $"Please fill in all the required fields";
                        CurrentProcess.ProcessError = $"";

                        //Loop through the invalidList to give a more specific error:
                        foreach ( string item in invalidList)
                        {
                            switch(item)
                            {
                                case "Title2":
                                    CurrentProcess.ProcessError += $"Please fill in your title@";
                                    break;

                                case "Name":
                                    CurrentProcess.ProcessError += $"Please fill in your Name@";
                                    break;

                                case "Surname":
                                    CurrentProcess.ProcessError += $"Please fill in your Surname@";
                                    break;

                                case "MaritalStatus":
                                    CurrentProcess.ProcessError += $"Please fill in your Marital Status@";
                                    break;

                                case "Gender":
                                    CurrentProcess.ProcessError += $"Please fill in your Gender@";
                                    break;

                                case "IDPassportNumber":
                                    CurrentProcess.ProcessError += $"Please fill in your ID or Passport number@";
                                    break;

                                case "IDType":
                                    CurrentProcess.ProcessError += $"Please indicate which type of ID you will providing@";
                                    break;

                                case "BirthDate":
                                    CurrentProcess.ProcessError += $"Please fill in your Date of Birth@";
                                    break;

                                case "TempResident":
                                    CurrentProcess.ProcessError += $"Please indicate whether of not you are a temporary resident@";
                                    break;

                                case "TaxResident":
                                    CurrentProcess.ProcessError += $"Please indicate whether of not you are a tax resident@";
                                    break;

                                case "Adress":
                                    CurrentProcess.ProcessError += $"Please fill in your phyiscal/residential address@";
                                    break;

                                case "City":
                                    CurrentProcess.ProcessError += $"Please fill in your Town/City@";
                                    break;

                                case "Province":
                                    CurrentProcess.ProcessError += $"Please fill in your Province@";
                                    break;

                                case "PostalCode":
                                    CurrentProcess.ProcessError += $"Please fill in your Postal Code@";
                                    break;

                                case "PhoneWork":
                                    CurrentProcess.ProcessError += $"Please fill in your Telephone Number@";
                                    break;

                                case "Mobile":
                                    CurrentProcess.ProcessError += $"Please fill in your Mobile Number@";
                                    break;

                                case "EMail":
                                    CurrentProcess.ProcessError += $"Please fill in your Email Address@";
                                    break;

                                case "KinName":
                                    CurrentProcess.ProcessError += $"Please fill in your Next of Kin Name@";
                                    break;

                                case "KinSurname":
                                    CurrentProcess.ProcessError += $"Please fill in your Next of Kin Surname @";
                                    break;

                                case "KinMobile":
                                    CurrentProcess.ProcessError += $"Please fill in your Next of Kin Mobile Number @";
                                    break;
                            }
                        }

                        CurrentProcess.ProcessError = CurrentProcess.ProcessError.Replace("@", "</br>");
                    }




                    CurrentProcess.ClientType = "Individual";//collection["ClientType"];
                    CurrentProcess.Title2 = collection["Title2"];
                    CurrentProcess.Name = collection["Name"];
                    CurrentProcess.Surname = collection["Surname"];
                    CurrentProcess.Gender = collection["Gender"];
                    CurrentProcess.MaritalStatus = collection["MaritalStatus"];
                    CurrentProcess.IDType = collection["IDType"];
                    CurrentProcess.IDPassportNumber = collection["IDPassportNumber"];
                    CurrentProcess.BirthDate = vOr(CurrentProcess.BirthDate, collection["BirthDate"]);
                    CurrentProcess.TempResident = vOr(CurrentProcess.TempResident, collection["TempResident"]);
                    CurrentProcess.TaxResident = vOr(CurrentProcess.TaxResident, collection["TaxResident"]);
                    CurrentProcess.PostResAddress = vOr(CurrentProcess.PostResAddress, collection["PostresAddress"]);
                    CurrentProcess.Adress = collection["Adress"];
                    CurrentProcess.PostalCode = collection["PostalCode"];
                    CurrentProcess.City = collection["City"];
                    CurrentProcess.Province = collection["Province"];
                    CurrentProcess.PostAdress = collection["PostAdress"];
                    CurrentProcess.PostPostalCode = collection["PostPostalCode"];
                    CurrentProcess.PostCity = collection["PostCity"];
                    CurrentProcess.PostProvince = collection["PostProvince"];
                    collection["CountryCodeWork"] = "";
                    CurrentProcess.CountryCodeWork = collection["CountryCodeWork"];
                    CurrentProcess.PhoneWork = collection["PhoneWork"];
                    collection["CountryCodeHome"] = "";
                    CurrentProcess.CountryCodeHome = collection["CountryCodeHome"];
                    CurrentProcess.PhoneHome = collection["PhoneHome"];
                    collection["CountryCodeMobile"] = "";
                    CurrentProcess.CountryCodeMobile = collection["CountryCodeMobile"];
                    CurrentProcess.Mobile = collection["Mobile"];
                    CurrentProcess.EMail = collection["EMail"];
                    CurrentProcess.SecondaryEMail = collection["SecondaryEMail"];
                    CurrentProcess.KinName = collection["KinName"];
                    CurrentProcess.KinSurname = collection["KinSurname"];
                    collection["CountryCodeKin"] = "";
                    CurrentProcess.CountryCodeKin = collection["CountryCodeKin"];
                    CurrentProcess.CountryCodeKin = "";
                    CurrentProcess.KinMobile = collection["KinMobile"];

                    //Secuity Validations:
                    if (!Regex.IsMatch(CurrentProcess.IDPassportNumber, @"^[a-zA-Z0-9]+$", RegexOptions.ECMAScript))
                    {
                        processValid = false;
                        CurrentProcess.ProcessError += $"Please use only alphabets and decimals in your ID/Passport Number </br>";
                    }

                    if (!Regex.IsMatch(CurrentProcess.Name, @"^[a-zA-Z\s]+$", RegexOptions.ECMAScript))
                    {
                        processValid = false;
                        CurrentProcess.ProcessError += $"Please use only alphabets in your Name </br>";
                    }

                    if (!Regex.IsMatch(CurrentProcess.Surname, @"^[a-zA-Z\s]+$", RegexOptions.ECMAScript))
                    {
                        processValid = false;
                        CurrentProcess.ProcessError += $"Please use only alphabets and decimals in your Surname </br>";
                    }

                    if (!Regex.IsMatch(CurrentProcess.KinName, @"^[a-zA-Z\s]+$", RegexOptions.ECMAScript))
                    {
                        processValid = false;
                        CurrentProcess.ProcessError += $"Please use only alphabets in your Next of Kin Name </br>";
                    }

                    if (!Regex.IsMatch(CurrentProcess.KinSurname, @"^[a-zA-Z\s]+$", RegexOptions.ECMAScript))
                    {
                        processValid = false;
                        CurrentProcess.ProcessError += $"Please use only alphabets in your Next of Kin Surname </br>";
                    }

                    if (!Regex.IsMatch(CurrentProcess.Adress, @"^[a-zA-Z0-9,\s]+$", RegexOptions.ECMAScript))
                    {
                        processValid = false;
                        CurrentProcess.ProcessError += $"Please use only alphabets and decimals in your Physical/Residential address </br>";
                    }

                    if (!Regex.IsMatch(CurrentProcess.City, @"^[a-zA-Z\s]+$", RegexOptions.ECMAScript))
                    {
                        processValid = false;
                        CurrentProcess.ProcessError += $"Please use only alphabets Town/City </br>";
                    }

                    if (!Regex.IsMatch(CurrentProcess.PostAdress, @"^[a-zA-Z0-9,\s]+$", RegexOptions.ECMAScript))
                    {
                        processValid = false;
                        CurrentProcess.ProcessError += $"Please use only alphabets and decimals in your Postal address </br>";
                    }

                    if (!Regex.IsMatch(CurrentProcess.PostCity, @"^[a-zA-Z\s]+$", RegexOptions.ECMAScript))
                    {
                        processValid = false;
                        CurrentProcess.ProcessError += $"Please use only alphabets in your Postal City </br>";
                    }


                    // Save and validate temporary residence details if user indicates temporary residence, set empty otherwise:
                    if (CurrentProcess.TempResident == true)
                    {
                        CurrentProcess.TempResidentNumber = collection["TempResidentNumber"];
                        CurrentProcess.PermitExpiryDate = vOr(CurrentProcess.PermitExpiryDate, collection["PermitExpiryDate"]);
                        DateTime pexp;

                        if (!isValidString(collection["TempResidentNumber"]) || !isValidString(collection["PermitExpiryDate"]))
                        {
                            CurrentProcess.ProcessError = $"Temporary Residence details have not been completed.";
                            processValid = false;
                        }
                        else if (!isValidDate(collection["PermitExpiryDate"], out pexp))
                        {
                            CurrentProcess.ProcessError = $"Please enter a valid date for temporary residence.";
                            processValid = false;
                        }
                        else if (!isFutureDate(pexp))
                        {
                            CurrentProcess.ProcessError = $"Please enter a future date for temporary residence.";
                            processValid = false;
                        }




                    }
                    else
                    {
                        CurrentProcess.TempResidentNumber = "";
                        CurrentProcess.PermitExpiryDate = null;
                    }

                    // Validation for income tax details:
                    int counter = 0;
                    while (collection["IncomeTaxDetails[" + counter + "].IncomeTaxNumber"] != null)
                    {
                        var taxNumber = collection["IncomeTaxDetails[" + counter + "].IncomeTaxNumber"];
                        var country = collection["IncomeTaxDetails[" + counter + "].CountryOfIssue"];

                        if (CurrentProcess.IncomeTaxDetails.Count > counter)
                            CurrentProcess.IncomeTaxDetails[counter] = new TaxDetailsViewModel { IncomeTaxNumber = taxNumber, CountryOfIssue = country };
                        else
                            CurrentProcess.IncomeTaxDetails.Add(new TaxDetailsViewModel { IncomeTaxNumber = taxNumber, CountryOfIssue = country });
                        ++counter;

                        //if (!isValidNumber(taxNumber) || !isValidString(country))
                        //{
                        //    CurrentProcess.ProcessError = $"Income tax details have not been completed.";
                        //    processValid = false;
                        //}
                    }

                    // Validation for additional passports
                    int counter2 = 0;
                    while (collection["AdditionalPassportDetails[" + counter2 + "].PassportNumber"] != null)
                    {
                        var PassportNumber = collection["AdditionalPassportDetails[" + counter2 + "].PassportNumber"];
                        var CountryOfIssue = collection["AdditionalPassportDetails[" + counter2 + "].CountryOfIssue"];
                        var DateOfIssue = Convert.ToDateTime(collection["AdditionalPassportDetails[" + counter2 + "].DateOfIssue"]);

                        if (CurrentProcess.AdditionalPassportDetails.Count > counter2)
                            CurrentProcess.AdditionalPassportDetails[counter2] = new AdditionalPassportDetailsModel { PassportNumber = PassportNumber, CountryOfIssue = CountryOfIssue, DateOfIssue = DateOfIssue };
                        else
                            CurrentProcess.AdditionalPassportDetails.Add(new AdditionalPassportDetailsModel { PassportNumber = PassportNumber, CountryOfIssue = CountryOfIssue, DateOfIssue = DateOfIssue });
                        ++counter2;

                        DateTime pexp;
                        if (isValidDate(collection["DateOfIssue"], out pexp) || isFutureDate(DateOfIssue))
                        {
                            CurrentProcess.ProcessError += $"The passport Date Of Issue is invalid </br>";
                            processValid = false;
                        }
                        if (!isValidString(PassportNumber) || !isValidString(CountryOfIssue))
                        {
                            CurrentProcess.ProcessError += $"Additional Passport details have not been completed. </br>";
                            processValid = false;
                        }
                        if (!Regex.IsMatch(CurrentProcess.IDPassportNumber, @"^[a-zA-Z0-9]+$", RegexOptions.ECMAScript))
                        {
                            processValid = false;
                            CurrentProcess.ProcessError += $"Please use only alphabets and decimals in your Additonal Passport Number. </br>";
                        }
                        else if (PassportNumber.Length < 7)
                        {
                            CurrentProcess.ProcessError += $"Additional Passport must have atleast 7 characters. </br>";
                            processValid = false;
                        }
                    }

                    //CurrentProcess.PermitExpiryDate = collection["PermitExpiryDate"];
                    while (processValid)
                    {
                        DateTime minDate = new DateTime(1900, 1, 1);
                        int cmp = DateTime.Compare(Convert.ToDateTime(CurrentProcess.BirthDate), minDate);
                        processValid = isValidDate(collection["BirthDate"]) && (cmp >= 0) && (!isFutureDate(Convert.ToDateTime(CurrentProcess.BirthDate)));
                        if (!processValid)
                        {
                            CurrentProcess.ProcessError = $"Your date of birth cannot be a future date. Please enter a valid date of birth.";
                            break;
                        }

                        // Validation for ID / Passport number:
                        Regex alphaNum = new Regex("^[a-zA-Z0-9]*$");
                        Regex alpha = new Regex("^[0-9]*$");
                        string passport = collection["IDPassportNumber"];
                        string name = "ID or Passport";

                        if (CurrentProcess.IDType == "ID")
                        {
                            processValid = alpha.IsMatch(passport) && passport.Length == 13;
                            name = "ID";
                        }
                        else
                        {
                            processValid = alphaNum.IsMatch(passport) && passport.Length > 6 && passport.Length < 14;
                            name = "Passport";
                        }

                        if (!processValid)
                        {
                            CurrentProcess.ProcessError = $"The provided {name} number is invalid";
                            break;
                        }

                        // Validation for DOB against ID number:
                        string IdString = CurrentProcess.IDPassportNumber.Substring(0, 6);
                        string birthDateString = CurrentProcess.BirthDate.Replace("-", "").Substring(2);

                        if (CurrentProcess.IDType == "ID" && IdString != birthDateString)
                        {
                            CurrentProcess.ProcessError = $"Date of birth provided does not match date indicated by identity number.";
                            processValid = false;
                            break;
                        }

                        // Validation for telephone numbers:

                        if (CurrentProcess.PhoneWork.Length < 10)
                        {
                            CurrentProcess.ProcessError = $"Your work telephone number must be ten digits long.";
                            processValid = false;
                            break;
                        }

                        if (CurrentProcess.PhoneHome.Length > 0 && CurrentProcess.PhoneHome.Length < 10)
                        {
                            CurrentProcess.ProcessError = $"Your home telephone number must be ten digits long.";
                            processValid = false;
                            break;
                        }

                        if (CurrentProcess.Mobile.Length < 10)
                        {
                            CurrentProcess.ProcessError = $"Your mobile number must be ten digits long.";
                            processValid = false;
                            break;
                        }

                        // Checking whether 'Postal Address' fields have been completed if not the same as physical address:
                        if (CurrentProcess.PostResAddress == false)
                        {
                            if (!isValidString(CurrentProcess.PostAdress) || !isValidString(CurrentProcess.PostPostalCode) || !isValidString(CurrentProcess.PostCity) || !isValidString(CurrentProcess.PostProvince))
                            {
                                CurrentProcess.ProcessError = $"Postal address details have not been completed.";
                                processValid = false;
                                break;
                            }
                        }

                        InvestmentApplication testFiles = await RoboAPI.getInvestmentApplication(ClientReferenceNumber);

                        CurrentProcess.IDUploaded = testFiles.IDCopy != null;
                        CurrentProcess.PORUploaded = testFiles.ProofOfAdress != null;

                        if (testFiles.IDCopy == null || testFiles.ProofOfAdress == null)
                        {
                            CurrentProcess.ProcessError = "Please upload all the required files.";
                            processValid = false;
                            break;
                        }

                        //End of checks
                        break;
                    }

                    await Save(collection, ClientReferenceNumber, CurrentProcess);
                }

                if (CurrentProcess.ProcessStage == 3)
                {
                    var validationList = new List<string>
                    {
                        //"BAGLContact"
                    };

                    var invalidList = validationList.Where(x => InValidateNull(collection[x])).ToList();
                    CurrentProcess.FundManagersConsent = vOr(CurrentProcess.FundManagersConsent, collection["FundManagersConsent"] ?? "false");
                    if (CurrentProcess.FundManagersConsent != true)
                    {
                        processValid = false;
                        CurrentProcess.ProcessError = $"You must agree to allow Absa Fund Managers to share your personal information with other entities within Barclays Africa Group Limited </br>";
                    }

                    CurrentProcess.income_salaryWages = vOr(CurrentProcess.income_salaryWages, collection["income_salaryWages"] ?? "false");
                    CurrentProcess.income_commission = vOr(CurrentProcess.income_commission, collection["income_commission"] ?? "false");
                    CurrentProcess.income_bonus = vOr(CurrentProcess.income_bonus, collection["income_bonus"] ?? "false");
                    CurrentProcess.income_maintenance = vOr(CurrentProcess.income_maintenance, collection["income_maintenance"] ?? "false");
                    CurrentProcess.income_pension = vOr(CurrentProcess.income_pension, collection["income_pension"] ?? "false");
                    CurrentProcess.income_investments = vOr(CurrentProcess.income_investments, collection["income_investments"] ?? "false");
                    CurrentProcess.income_insuranceClaim = vOr(CurrentProcess.income_insuranceClaim, collection["income_insuranceClaim"] ?? "false");
                    CurrentProcess.income_allowance = vOr(CurrentProcess.income_allowance, collection["income_allowance"] ?? "false");
                    CurrentProcess.income_donationGift = vOr(CurrentProcess.income_donationGift, collection["income_donationGift"] ?? "false");
                    CurrentProcess.income_inheritance = vOr(CurrentProcess.income_inheritance, collection["income_inheritance"] ?? "false");
                    CurrentProcess.income_socialGrant = vOr(CurrentProcess.income_socialGrant, collection["income_socialGrant"] ?? "false");
                    CurrentProcess.income_retirementAnnuity = vOr(CurrentProcess.income_retirementAnnuity, collection["income_retirementAnnuity"] ?? "false");
                    CurrentProcess.income_otherCheckBox = vOr(CurrentProcess.income_otherCheckBox, collection["income_otherCheckBox"] ?? "false");
                    CurrentProcess.income_other = collection["income_other"];


                    if (CurrentProcess.income_salaryWages != true &&
                        CurrentProcess.income_commission != true &&
                        CurrentProcess.income_bonus != true &&
                        CurrentProcess.income_maintenance != true &&
                        CurrentProcess.income_pension != true &&
                        CurrentProcess.income_investments != true &&
                        CurrentProcess.income_insuranceClaim != true &&
                        CurrentProcess.income_allowance != true &&
                        CurrentProcess.income_donationGift != true &&
                        CurrentProcess.income_inheritance != true &&
                        CurrentProcess.income_socialGrant != true &&
                        CurrentProcess.income_retirementAnnuity != true &&
                        CurrentProcess.income_otherCheckBox != true)
                    {
                        processValid = false;
                        CurrentProcess.ProcessError += $"Please select the sources of your income.</br>";
                    }

                    if (CurrentProcess.income_otherCheckBox && !isValidString(CurrentProcess.income_other))
                    {
                        processValid = false;
                        CurrentProcess.ProcessError += $"Please describe the nature of your other income sources. </br>";
                    }

                    CurrentProcess.funds_salaryWages = vOr(CurrentProcess.funds_salaryWages, collection["funds_salaryWages"] ?? "false");
                    CurrentProcess.funds_commission = vOr(CurrentProcess.funds_commission, collection["funds_commission"] ?? "false");
                    CurrentProcess.funds_bonus = vOr(CurrentProcess.funds_bonus, collection["funds_bonus"] ?? "false");
                    CurrentProcess.funds_maintenance = vOr(CurrentProcess.funds_maintenance, collection["funds_maintenance"] ?? "false");
                    CurrentProcess.funds_pension = vOr(CurrentProcess.funds_pension, collection["funds_pension"] ?? "false");
                    CurrentProcess.funds_investments = vOr(CurrentProcess.funds_investments, collection["funds_investments"] ?? "false");
                    CurrentProcess.funds_insuranceClaim = vOr(CurrentProcess.funds_insuranceClaim, collection["funds_insuranceClaim"] ?? "false");
                    CurrentProcess.funds_allowance = vOr(CurrentProcess.funds_allowance, collection["funds_allowance"] ?? "false");
                    CurrentProcess.funds_donationGift = vOr(CurrentProcess.funds_donationGift, collection["funds_donationGift"] ?? "false");
                    CurrentProcess.funds_inheritance = vOr(CurrentProcess.funds_inheritance, collection["funds_inheritance"] ?? "false");
                    CurrentProcess.funds_socialGrant = vOr(CurrentProcess.funds_socialGrant, collection["funds_socialGrant"] ?? "false");
                    CurrentProcess.funds_retirementAnnuity = vOr(CurrentProcess.funds_retirementAnnuity, collection["funds_retirementAnnuity"] ?? "false");
                    CurrentProcess.funds_otherCheckBox = vOr(CurrentProcess.funds_otherCheckBox, collection["funds_otherCheckBox"] ?? "false");
                    CurrentProcess.funds_other = collection["funds_other"];


                    if (CurrentProcess.funds_salaryWages != true &&
                        CurrentProcess.funds_commission != true &&
                        CurrentProcess.funds_bonus != true &&
                        CurrentProcess.funds_maintenance != true &&
                        CurrentProcess.funds_pension != true &&
                        CurrentProcess.funds_investments != true &&
                        CurrentProcess.funds_insuranceClaim != true &&
                        CurrentProcess.funds_allowance != true &&
                        CurrentProcess.funds_donationGift != true &&
                        CurrentProcess.funds_inheritance != true &&
                        CurrentProcess.funds_socialGrant != true &&
                        CurrentProcess.funds_retirementAnnuity != true &&
                        CurrentProcess.funds_otherCheckBox != true)
                    {
                        processValid = false;
                        CurrentProcess.ProcessError += $"Please select the sources of your funds. </br>";
                    }

                    if (CurrentProcess.funds_otherCheckBox && !isValidString(CurrentProcess.funds_other))
                    {
                        processValid = false;
                        CurrentProcess.ProcessError += $"Please describe the nature of your other sources of funds. </br>";
                    }

                    CurrentProcess.sourceOfWealth = collection["sourceOfWealth"];
                    if(CurrentProcess.sourceOfWealth != null && CurrentProcess.sourceOfWealth != "")
                    {
                        if (!Regex.IsMatch(CurrentProcess.sourceOfWealth, @"^[a-zA-Z0-9\s]+$", RegexOptions.ECMAScript))
                        {
                            processValid = false;
                            CurrentProcess.ProcessError += $"Please use only alphabets and decimals in your source of wealth. </br>";
                        }
                    }

                    CurrentProcess.BAGLContact = vOr(CurrentProcess.BAGLContact, collection["BAGLContact"]);
                    CurrentProcess.POPI = vOr(CurrentProcess.BAGLContact, collection["POPI"]);
                    CurrentProcess.PostConsent = vOr(CurrentProcess.PostConsent, collection["PostConsent"] ?? "false");
                    CurrentProcess.PhoneConsent = vOr(CurrentProcess.PhoneConsent, collection["PhoneConsent"] ?? "false");
                    CurrentProcess.SMSConsent = vOr(CurrentProcess.SMSConsent, collection["SMSConsent"] ?? "false");
                    CurrentProcess.EMailConsent = vOr(CurrentProcess.EMailConsent, collection["EMailConsent"] ?? "false");
                    CurrentProcess.IranConfirm = vOr(CurrentProcess.IranConfirm, collection["IranConfirm"]);
                    CurrentProcess.NorthKoreaConfirm = vOr(CurrentProcess.NorthKoreaConfirm, collection["NorthKoreaConfirm"]);
                    CurrentProcess.SyriaConfirm = vOr(CurrentProcess.SyriaConfirm, collection["SyriaConfirm"]);
                    CurrentProcess.NorthSudanConfirm = vOr(CurrentProcess.NorthSudanConfirm, collection["NorthSudanConfirm"]);
                    CurrentProcess.SouthSudanConfirm = vOr(CurrentProcess.SouthSudanConfirm, collection["SouthSudanConfirm"]);
                    CurrentProcess.CubaConfirm = vOr(CurrentProcess.CubaConfirm, collection["CubaConfirm"]);
                    CurrentProcess.CrimeaRegionConfirm = vOr(CurrentProcess.CrimeaRegionConfirm, collection["CrimeaRegionConfirm"]);



                    if (CurrentProcess.BAGLContact)
                    {
                        if (CurrentProcess.PostConsent != true && CurrentProcess.PhoneConsent != true && CurrentProcess.SMSConsent != true && CurrentProcess.EMailConsent != true)
                        {
                            processValid = false;
                            CurrentProcess.ProcessError += $"Please select ways in which Barclays Africa Group Limited may contact you. </br>";
                        }
                    }


                    if (
                        CurrentProcess.IranConfirm == null ||
                        CurrentProcess.NorthKoreaConfirm == null ||
                        CurrentProcess.SyriaConfirm == null ||
                        CurrentProcess.NorthSudanConfirm == null ||
                        CurrentProcess.SouthSudanConfirm == null ||
                        CurrentProcess.CubaConfirm == null ||
                        CurrentProcess.CrimeaRegionConfirm == null
                    )
                    {
                        processValid = false;
                        CurrentProcess.ProcessError += $"Please confirm whether you have a relationship with any of the mentioned sanctioned nations. </br>";
                    }
                    if (invalidList.Count > 0)
                    {
                        processValid = false;
                        CurrentProcess.ProcessError += $"Please fill in all the required fields. </br>";
                    }
                    await Save(collection, ClientReferenceNumber, CurrentProcess);
                }

                if (CurrentProcess.ProcessStage == 4)
                {
                    var validationList = new List<string>
                    {
                        "UnitInvestConfirm",
                        "RedemptionAccountHolder",
                        "RedemptionBank",
                        "RedemptionAccountNumber",
                        "RedemptionBranchCode",
                        "RedemptionAccountType"
                    };
                    CurrentProcess.LumpSumInvestment = vOr(CurrentProcess.LumpSumInvestment, collection["LumpSumInvestment"]);

                    InvestmentApplication userapp = await RoboAPI.getInvestmentApplication(ClientReferenceNumber);
                    string fullName = userapp.Name + " " + userapp.Surname;

                    CurrentProcess.RedemptionAccountHolder = collection["RedemptionAccountHolder"];
                    CurrentProcess.RedemptionBank = collection["RedemptionBank"];
                    CurrentProcess.RedemptionAccountNumber = collection["RedemptionAccountNumber"];
                    CurrentProcess.RedemptionBranchCode = collection["RedemptionBranchCode"];
                    CurrentProcess.RedemptionAccountType = collection["RedemptionAccountType"];
                    CurrentProcess.CashflowplanConfirm = vOr(CurrentProcess.CashflowplanConfirm, collection["CashflowplanConfirm"] ?? "false");

                    if (CurrentProcess.RedemptionAccountHolder != null && CurrentProcess.RedemptionAccountHolder != "")
                    {
                        if (!Regex.IsMatch(CurrentProcess.RedemptionAccountHolder, @"^[a-zA-Z0-9\s]+$", RegexOptions.ECMAScript))
                        {
                            processValid = false;
                            CurrentProcess.ProcessError += $"Please use only alphabets and decimals in Redemption Account Holder. </br>";
                        }
                    }

                    if (CurrentProcess.CashflowplanConfirm == true)
                    {
                        validationList.Add("CashFlowPlanDate");
                        validationList.Add("CashFlowPlanAmount");
                        //CurrentProcess.CashFlowPlanDate = vOr(CurrentProcess.CashFlowPlanDate, collection["CashFlowPlanDate"]);
                        //CurrentProcess.CashFlowPlanAmount = vOr(CurrentProcess.CashFlowPlanAmount, collection["CashFlowPlanAmount"]);
                    }

                    CurrentProcess.CashFlowPlanDate = vOr(CurrentProcess.CashFlowPlanDate, collection["CashFlowPlanDate"]);
                    CurrentProcess.CashFlowPlanAmount = vOr(CurrentProcess.CashFlowPlanAmount, collection["CashFlowPlanAmount"]);

                    if (CurrentProcess.CashflowplanConfirm == true)
                    {

                        DateTime minDate = new DateTime(1900, 1, 1);

                        if (!isValidDate(collection["CashFlowPlanDate"]))
                        {
                            processValid = false;
                            CurrentProcess.ProcessError = $"Please select a recurring payment date that is valid.";
                        }

                        else if (!isFutureDate(Convert.ToDateTime(CurrentProcess.CashFlowPlanDate)))
                        {
                            processValid = false;
                            CurrentProcess.ProcessError = $"Please select a recurring payment date that is in the future.";
                        }

                    }

                    CurrentProcess.MonthlyDebitOrder = vOr(CurrentProcess.MonthlyDebitOrder, collection["MonthlyDebitOrder"]);
                    CurrentProcess.UnitInvestConfirm = vOr(CurrentProcess.UnitInvestConfirm, collection["UnitInvestConfirm"]);
                    CurrentProcess.ReinvestAccountHolder = collection["ReinvestAccountHolder"];
                    CurrentProcess.ReinvestBank = collection["ReinvestBank"];
                    CurrentProcess.ReinvestAccount = collection["ReinvestAccount"];
                    CurrentProcess.ReinvestBranchCode = collection["ReinvestBranchCode"];
                    CurrentProcess.ReinvestAccountType = collection["ReinvestAccountType"];
                    CurrentProcess.DebitOrderAccountHolder = collection["DebitOrderAccountHolder"];
                    CurrentProcess.DebitOrderBank = collection["DebitOrderBank"];
                    CurrentProcess.DebitOrderAccountNumber = vOr(CurrentProcess.DebitOrderAccountNumber, collection["DebitOrderAccountNumber"]);
                    CurrentProcess.DebitOrderBranchCode = collection["DebitOrderBranchCode"];
                    CurrentProcess.DebitOrderAmount = vOr(CurrentProcess.DebitOrderAmount, collection["DebitOrderAmount"]);
                    CurrentProcess.DebitOrderDay = vOr(CurrentProcess.DebitOrderDay, collection["DebitOrderDay"]);
                    CurrentProcess.DebitOrderAccountType = collection["DebitOrderAccountType"];

                    if (CurrentProcess.ReinvestAccountHolder != null && CurrentProcess.ReinvestAccountHolder != "")
                    {
                        if (!Regex.IsMatch(CurrentProcess.ReinvestAccountHolder, @"^[a-zA-Z0-9\s]+$", RegexOptions.ECMAScript))
                        {
                            processValid = false;
                            CurrentProcess.ProcessError += $"Please use only alphabets and decimals in Reinvestment Account Holder. </br>";
                        }
                    }

                    if (CurrentProcess.DebitOrderAccountHolder != null && CurrentProcess.DebitOrderAccountHolder != "")
                    {
                        if (!Regex.IsMatch(CurrentProcess.DebitOrderAccountHolder, @"^[a-zA-Z0-9\s]+$", RegexOptions.ECMAScript))
                        {
                            processValid = false;
                            CurrentProcess.ProcessError += $"Please use only alphabets and decimals in Debit Account Holder. </br>";
                        }
                    }

                    DateTime temp = new DateTime();
                    bool success = DateTime.TryParse(collection["DebitOrderFromMonth"], CultureInfo.InvariantCulture, DateTimeStyles.None, out temp);
                    if (success)
                        CurrentProcess.DebitOrderFromMonth = temp;


                    CurrentProcess.IncreaseInvestment = collection["IncreaseInvestment"];
                    CurrentProcess.Increase0 = false;
                    CurrentProcess.Increase10 = false;
                    CurrentProcess.Increase15 = false;
                    CurrentProcess.Increase20 = false;
                    if (CurrentProcess.IncreaseInvestment == "10%")
                    {
                        CurrentProcess.Increase10 = true;
                    }
                    else if (CurrentProcess.IncreaseInvestment == "15%")
                    {
                        CurrentProcess.Increase15 = true;
                    }
                    else if (CurrentProcess.IncreaseInvestment == "20%")
                    {
                        CurrentProcess.Increase20 = true;
                    }
                    else if (CurrentProcess.IncreaseInvestment == "0%")
                    {
                        CurrentProcess.Increase0 = true;
                    }

                    // Adds required fields for income instruction payment details if unit re-invest not selected:
                    if (CurrentProcess.UnitInvestConfirm == false)
                    {
                        validationList.Add("ReinvestAccountHolder");
                        validationList.Add("ReinvestBank");
                        validationList.Add("ReinvestAccount");
                        validationList.Add("ReinvestBranchCode");
                        validationList.Add("ReinvestAccountType");
                    }


                    if (isValidNumber(CurrentProcess.MonthlyDebitOrder) || isValidNumber(CurrentProcess.LumpSumInvestment))
                    {
                        if (isValidNumber(CurrentProcess.MonthlyDebitOrder))
                        {
                            validationList.Add("MonthlyDebitOrder");
                            validationList.Add("DebitOrderAccountHolder");
                            validationList.Add("DebitOrderBank");
                            validationList.Add("DebitOrderAccountNumber");
                            validationList.Add("DebitOrderBranchCode");
                            validationList.Add("DebitOrderAmount");
                            validationList.Add("DebitOrderDay");
                            validationList.Add("DebitOrderFromMonth");
                            validationList.Add("DebitOrderAccountType");
                        }

                        if (isValidNumber(CurrentProcess.LumpSumInvestment))
                        {
                            validationList.Add("LumpSumInvestment");
                        }
                    }
                    else
                    {
                        validationList.Add("MonthlyDebitOrder");
                        validationList.Add("LumpSumInvestment");
                    }

                    var invalidList = validationList.Where(x => InValidateNull(collection[x])).ToList();

                    if (invalidList.Count > 0)
                    {
                        processValid = false;
                        //CurrentProcess.ProcessError = $"Please fill in all the required fields";



                        foreach (string item in invalidList)
                        {
                            switch (item)
                            {
                                case "UnitInvestConfirm":
                                    CurrentProcess.ProcessError += $"Please indicate your Income Instruction. </br>";
                                    break;

                                case "RedemptionAccountHolder":
                                    CurrentProcess.ProcessError += $"Please fill in the Account Holder for Deposits into Investments. </br>";
                                    break;

                                case "RedemptionBank":
                                    CurrentProcess.ProcessError += $"Please fill in the Bank Name for Deposits into Investments. </br>";
                                    break;
                                case "RedemptionAccountNumber":
                                    CurrentProcess.ProcessError += $"Please fill in the Account Number for Deposits into Investments. </br>";
                                    break;
                                case "RedemptionBranchCode":
                                    CurrentProcess.ProcessError += $"Please fill in the Branch Code for Deposits into Investments. </br>";
                                    break;
                                case "RedemptionAccountType":
                                    CurrentProcess.ProcessError += $"Please fill in the Account Type for Deposits into Investments. </br>";
                                    break;
                                case "CashFlowPlanDate":
                                    CurrentProcess.ProcessError += $"Please fill in the date of your recurring payment. </br>";
                                    break;
                                case "CashFlowPlanAmount":
                                    CurrentProcess.ProcessError += $"Please fill in the amount of your recurring payment. </br>";
                                    break;
                                case "ReinvestAccountHolder":
                                    CurrentProcess.ProcessError += $"Please fill in the Account Holder that income should be paid into. </br>";
                                    break;
                                case "ReinvestBank":
                                    CurrentProcess.ProcessError += $"Please fill in the Bank Name that income should be paid into. </br>";
                                    break;
                                case "ReinvestAccount":
                                    CurrentProcess.ProcessError += $"Please fill in the Account Number that income should be paid into. </br>";
                                    break;
                                case "ReinvestBranchCode":
                                    CurrentProcess.ProcessError += $"Please fill in the Branch Code that income should be paid into. </br>";
                                    break;
                                case "ReinvestAccountType":
                                    CurrentProcess.ProcessError += $"Please fill in the Account Type that income should be paid into. </br>";
                                    break;


                                case "DebitOrderAccountHolder":
                                    CurrentProcess.ProcessError += $"Please fill in the Account Holder for Debit Orders. </br>";
                                    break;
                                case "DebitOrderBank":
                                    CurrentProcess.ProcessError += $"Please fill in the Bank Name for Debit Orders. </br>";
                                    break;
                                case "DebitOrderAccountNumber":
                                    CurrentProcess.ProcessError += $"Please fill in the Account Number for Debit Orders. </br>";
                                    break;
                                case "DebitOrderBranchCode":
                                    CurrentProcess.ProcessError += $"Please fill in the Branch Code for Debit Orders. </br>";
                                    break;
                                case "DebitOrderAmount":
                                    CurrentProcess.ProcessError += $"Please fill in the Amount for Debit Orders. </br>";
                                    break;
                                case "DebitOrderDay":
                                    CurrentProcess.ProcessError += $"Please fill in the Day of the Month for Debit Orders. </br>";
                                    break;
                                case "DebitOrderFromMonth":
                                    CurrentProcess.ProcessError += $"Please fill in the Date when Debit Orders should start. </br>";
                                    break;
                                case "DebitOrderAccountType":
                                    CurrentProcess.ProcessError += $"Please fill in the Account Type for Debit Orders. </br>";
                                    break;


                            }
                        }


                    }

                    if (!isValidNumber(CurrentProcess.LumpSumInvestment) && !isValidNumber(CurrentProcess.MonthlyDebitOrder))
                    {
                        processValid = false;
                        CurrentProcess.ProcessError = $"Please note that either Lump Sum Investment or Monthly Debit Order fields must be filled in";
                    }

                    if (processValid && isValidNumber(CurrentProcess.MonthlyDebitOrder))
                    {
                        string debitDay = collection["DebitOrderDay"];
                        var debitDayValid = isValidInteger(debitDay) && int.Parse(debitDay) > 0 && int.Parse(debitDay) <= 31;
                        if (!debitDayValid)
                        {
                            CurrentProcess.ProcessError = $"The Debit Order day is invalid";
                            processValid = false;
                        }

                        DateTime dateStart = (DateTime)CurrentProcess.DebitOrderFromMonth.Value;

                        var dateStartValid = isFutureDate(dateStart);

                        if (!dateStartValid)
                        {
                            CurrentProcess.ProcessError = $"The Debit Order starting date should be a date in the future";
                            processValid = false;
                        }
                        if (processValid)
                        {
                            if (dateStart.Subtract(DateTime.Now).TotalDays > 31)
                            {
                                CurrentProcess.ProcessError = $"The Debit Order starting date should be a date in the future, no more than 31 days into the future";
                                processValid = false;
                            }
                        }

                    }

                    // Check that name of account holder for bank account details matches name given in Step 1:

                    


                    if (CurrentProcess.UnitInvestConfirm == false)
                    {
                        /*if (CurrentProcess.ReinvestAccountHolder.ToLower().Replace(" ", "") != fullName.ToLower().Replace(" ", ""))
                        {
                            CurrentProcess.ProcessError = $"The name of the Account Holder does not match the name you provided under Client Details in Step 1. Please ensure that they match.";
                            processValid = false;
                        }*/
                    }


                    if (isValidNumber(CurrentProcess.MonthlyDebitOrder))
                    {
                        decimal minDebitOrder = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["MinimumDebitOrderAmount"]);
                        if (CurrentProcess.MonthlyDebitOrder < minDebitOrder)
                        {
                            CurrentProcess.ProcessError = $"The minimum amount for a Monthly Debit Order is R {minDebitOrder},00.";
                            processValid = false;
                        }

                        /*if (CurrentProcess.DebitOrderAccountHolder != null)
                        {
                            if (CurrentProcess.DebitOrderAccountHolder.ToLower().Replace(" ", "") != fullName.ToLower().Replace(" ", ""))
                            {
                                CurrentProcess.ProcessError = $"The name of the Account Holder does not match the name you provided under Client Details in Step 1. Please ensure that they match.";
                                processValid = false;
                            }
                        }*/
                    }

                    if (isValidNumber(CurrentProcess.LumpSumInvestment))
                    {
                        decimal minLumpSum = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["MinimumLumpSumAmount"]);
                        if (CurrentProcess.LumpSumInvestment < minLumpSum)
                        {
                            CurrentProcess.ProcessError = $"The minimum amount for a Lump Sum Investment is R {minLumpSum},00.";
                            processValid = false;
                        }
                    }

                    /*if (CurrentProcess.RedemptionAccountHolder != null)
                    {
                        if (CurrentProcess.RedemptionAccountHolder.ToLower().Replace(" ", "") != fullName.ToLower().Replace(" ", ""))
                        {
                            CurrentProcess.ProcessError = $"The name of the Account Holder does not match the name you provided under Client Details in Step 1. Please ensure that they match.";
                            processValid = false;
                        }
                    }*/

                    await Save(collection, ClientReferenceNumber, CurrentProcess);
                    if(processValid)
                        await TransferModel(CurrentProcess, collection, ClientReferenceNumber);

                }





                if (CurrentProcess.ProcessStage == 5)
                {
                    if (collection["DeclarationAccept"]?.Split(',')[0] != null)
                    {
                        processValid = bool.Parse(collection["DeclarationAccept"].Split(',')[0]);
                    }
                    else
                    {
                        processValid = false;
                    }
                    if (!processValid)
                    {
                        CurrentProcess.ProcessError = "Please accept the declaration to continue.";
                    }
                    /*else
                    {
                        CurrentProcess.ProcessError = "";
                        RoboAPIController RoboAPI = new RoboAPIController();
                        InvestmentApplication userapp = await RoboAPI.getInvestmentApplication(ClientReferenceNumber);
                    }*/
                }


                if (CurrentProcess.ProcessStage == 6)
                {
                    CurrentProcess.AcceptTerms = vOr(CurrentProcess.AcceptTerms, collection["AcceptTerms"] ?? "false");
                    CurrentProcess.AcceptSignature = vOr(CurrentProcess.AcceptSignature, collection["AcceptSignature"] ?? "false");
                    CurrentProcess.SignatureFullName = vOr(CurrentProcess.SignatureFullName, collection["SignatureFullName"] ?? "");

                    //InvestmentApplication testFiles = await RoboAPI.getInvestmentApplication(ClientReferenceNumber);

                    //CurrentProcess.IDUploaded = testFiles.IDCopy != null;
                    //CurrentProcess.PORUploaded = testFiles.ProofOfAdress != null;
                    //string fullName = testFiles.Name + " " + testFiles.Surname;


                    processValid = CurrentProcess.AcceptTerms && CurrentProcess.AcceptSignature;//&& nameTest;
                    if (!processValid)
                    {
                        processValid = false;
                        CurrentProcess.ProcessError = "Please Confirm all the Conditions Below.";
                    }
                    else if (!isValidString(CurrentProcess.SignatureFullName))
                    {
                        processValid = false;
                        CurrentProcess.ProcessError = "Please confirm your full name.";
                    }
                    else if (!Regex.IsMatch(CurrentProcess.SignatureFullName, @"^[a-zA-Z0-9\s]+$", RegexOptions.ECMAScript))
                    {
                            processValid = false;
                            CurrentProcess.ProcessError += $"Please use only alphabets and decimals in your signature. </br>";
                    }
                        /*else if ( CurrentProcess.SignatureFullName.ToLower().Replace(" ", "") != fullName.ToLower().Replace(" ", "") )   
                        {
                            processValid = false;
                            CurrentProcess.ProcessError = "The name you entered does not match the name entered in Step 1.";
                        }*/
                       /* else
                    {
                        InvestmentApplication userapp = await RoboAPI.getInvestmentApplication(ClientReferenceNumber);
                        CurrentProcess.IDUploaded = userapp.IDCopy != null;
                        CurrentProcess.PORUploaded = userapp.ProofOfAdress != null;

                        if (userapp.IDCopy == null || userapp.ProofOfAdress == null)
                        {
                            CurrentProcess.ProcessError = "Please upload all the required files!";
                            processValid = false;
                        }
                    }*/
                }



                if (processValid)
                {
                    CurrentProcess.ProcessStage += 1;
                    CurrentProcess.LastStepCount = 1;
                }
                else
                {
                    CurrentProcess.LastStepCount = int.Parse(collection["LastStepCount"]) + 1;
                }
                ViewBag.LastStepCount = CurrentProcess.LastStepCount;

                switch (CurrentProcess.ProcessStage)
                {
                    case 1:
                        {
                            // InvestmentApplication userapp = await Tester();
                            //Configuration config = await RoboAPI.getConfiguration(1);
                            InvestmentApplication userapp = await RoboAPI.getInvestmentApplication(ClientReferenceNumber);
                            if (userapp == null)
                            {
                                //testing
                                //userapp = await Tester();
                                //ClientReferenceNumber = userapp.ClientReferenceNumber;

                                userapp = new InvestmentApplication();
                                CurrentProcess.ID = userapp.ClientReferenceNumber;
                                CurrentProcess.ProcessComplete = 0;
                                CurrentProcess.ProcessPercentage = CurrentProcess.ProcessComplete.ToString() + "%";
                                CurrentProcess.ProcessError = GetContactError(); // "An error occurred while processing your request. Please contact " + System.Configuration.ConfigurationManager.AppSettings["HelpDeskName"] + " on " + System.Configuration.ConfigurationManager.AppSettings["HelpDeskNumber"] + " for further assistance.<br />Return to the <a href='http://www.absa.co.za'>Absa Homepage</a>.";
                                CurrentProcess.ProcessHeader = "Error";
                                CurrentProcess.ProcessStage = 99;
                                if (CurrentProcess.ProcessError == "")
                                {
                                    CurrentProcess.ProcessMessage = CurrentProcess.ProcessError;
                                }
                                else
                                {
                                    CurrentProcess.ProcessMessage = GetContactError(); // "An error occurred while processing your request. Please contact " + System.Configuration.ConfigurationManager.AppSettings["HelpDeskName"] + " on " + System.Configuration.ConfigurationManager.AppSettings["HelpDeskNumber"] + " for further assistance.<br />Return to the <a href='http://www.absa.co.za'>Absa Homepage</a>.";
                                }

                                CurrentProcess.ProcessBtnMessage = "<<>>";
                                Session["InvestmentApplication"] = userapp;
                                
                            }
                            else
                            {
                                if (DateTime.Compare(DateTime.Parse(userapp.ApplicationSubmitted == null ? DateTime.MinValue.ToString() : userapp.ApplicationSubmitted.ToString()),
                                    DateTime.Parse(userapp.ApplicationRecieved.ToString())) > 0)
                                {
                                    Session["InvestmentApplication"] = userapp;
                                    CurrentProcess.ID = userapp.ClientReferenceNumber;
                                    CurrentProcess.ProcessComplete = 100;
                                    CurrentProcess.ProcessPercentage = CurrentProcess.ProcessComplete.ToString() + "%";
                                    CurrentProcess.ProcessHeader = "The process with reference number:[" + userapp.ClientReferenceNumber + "] has already been completed.";
                                    CurrentProcess.ProductName = userapp.RecommendedProductName;
                                    CurrentProcess.ProcessMessage = "The process with reference number:[" + userapp.ClientReferenceNumber + "] has already been completed."; ;

                                }
                                else
                                {
                                    Session["InvestmentApplication"] = userapp;
                                    CurrentProcess.ID = userapp.ClientReferenceNumber;
                                    CurrentProcess.ProcessComplete = 15;
                                    CurrentProcess.ProcessPercentage = CurrentProcess.ProcessComplete.ToString() + "%";
                                    CurrentProcess.ProcessHeader = "Wealth and Investment Management Application";
                                    CurrentProcess.ProductName = userapp.RecommendedProductName;
                                    //CurrentProcess.EMail = userapp.EMail;
                                    //CurrentProcess.Name = userapp.Name;

                                    if (CurrentProcess.ProcessError == "")
                                    {
                                        CurrentProcess.ProcessMessage = CurrentProcess.ProcessError;

                                    }
                                    else
                                    {
                                        CurrentProcess.ProcessMessage = "Wealth and Investment Management Application";
                                    }

                                }

                                CurrentProcess.ProcessBtnMessage = "Next >>";
                            }

                            break;
                        }
                    case 2:
                        {


                            CurrentProcess.ID = collection["ID"];
                            CurrentProcess.ProductName = collection["ProductName"];
                            CurrentProcess.ProcessComplete = 30;
                            CurrentProcess.ProcessPercentage = CurrentProcess.ProcessComplete.ToString() + "%";
                            CurrentProcess.ProcessHeader = "Wealth and Investment Management Application";
                            CurrentProcess.ProcessMessage = "Please complete the questions";
                            CurrentProcess.ProcessBtnMessage = "Next >>";
                            //Configuration config = await RoboAPI.getConfiguration(1);
                            InvestmentApplication userapp = await RoboAPI.getInvestmentApplication(ClientReferenceNumber);
                            if (userapp != null)
                            {
                                //CurrentProcess.EMail = userapp.EMail;
                                //CurrentProcess.Name = userapp.Name;
                            }


                            if (CurrentProcess.ProcessError == "")
                            {
                                CurrentProcess.ProcessMessage = CurrentProcess.ProcessError;

                            }
                            else
                            {
                                CurrentProcess.ProcessMessage = "";
                            }

                            break;
                        }
                    case 3:
                        {


                            CurrentProcess.ID = collection["ID"];
                            CurrentProcess.ProductName = collection["ProductName"];
                            CurrentProcess.ProcessComplete = 45;
                            CurrentProcess.ProcessPercentage = CurrentProcess.ProcessComplete.ToString() + "%";
                            CurrentProcess.ProcessHeader = "Wealth and Investment Management Application";
                            CurrentProcess.ProcessMessage = "Please complete the questions";
                            CurrentProcess.ProcessBtnMessage = "Next >>";
                            if (CurrentProcess.ProcessError == "")
                            {
                                CurrentProcess.ProcessMessage = CurrentProcess.ProcessError;

                            }
                            else
                            {
                                CurrentProcess.ProcessMessage = "";
                            }
                            await Save(collection, ClientReferenceNumber, CurrentProcess);
                            break;
                        }
                    case 4:
                        {

                            InvestmentApplication userapp = (InvestmentApplication)Session["InvestmentApplication"];
                            CurrentProcess.ID = collection["ID"];
                            if (userapp != null)
                            {
                                CurrentProcess.ProductName = userapp.RecommendedProductName;

                            }
                            else
                            {
                                CurrentProcess.ProductName = collection["ProductName"];

                            }
                            CurrentProcess.ProcessComplete = 60;
                            CurrentProcess.ProcessPercentage = CurrentProcess.ProcessComplete.ToString() + "%";
                            CurrentProcess.ProcessHeader = "Wealth and Investment Management Application";
                            CurrentProcess.ProcessMessage = "Please complete the questions";
                            CurrentProcess.ProcessBtnMessage = "Next >>";
                            if (CurrentProcess.ProcessError == "")
                            {
                                CurrentProcess.ProcessMessage = CurrentProcess.ProcessError;

                            }
                            else
                            {
                                CurrentProcess.ProcessMessage = "";
                            }
                            await Save(collection, ClientReferenceNumber, CurrentProcess);
                            break;
                        }
                    case 5:
                        {


                            CurrentProcess.ID = collection["ID"];
                            CurrentProcess.ProductName = collection["ProductName"];
                            CurrentProcess.ProcessComplete = 70;
                            CurrentProcess.ProcessPercentage = CurrentProcess.ProcessComplete.ToString() + "%";
                            CurrentProcess.ProcessHeader = "Wealth and Investment Management Application";
                            CurrentProcess.ProcessMessage = "Please complete the questions";
                            CurrentProcess.ProcessBtnMessage = "Next >>";
                            if (CurrentProcess.ProcessError == "")
                            {
                                CurrentProcess.ProcessMessage = CurrentProcess.ProcessError;

                            }
                            else
                            {
                                CurrentProcess.ProcessMessage = "";
                            }
                            await Save(collection, ClientReferenceNumber, CurrentProcess);
                            break;
                        }
                    case 6:
                        {


                            CurrentProcess.ID = collection["ID"];
                            CurrentProcess.ProductName = collection["ProductName"];
                            CurrentProcess.ProcessComplete = 75;
                            CurrentProcess.ProcessPercentage = CurrentProcess.ProcessComplete.ToString() + "%";
                            CurrentProcess.ProcessHeader = "Upload your FICA Documents";
                            CurrentProcess.ProcessMessage = "In order to successfully complete your application, you need to provide us with some documentation.";
                            CurrentProcess.ProcessBtnMessage = "Submit";
                            if (CurrentProcess.ProcessError == "")
                            {
                                CurrentProcess.ProcessMessage = CurrentProcess.ProcessError;

                            }
                            else
                            {
                                CurrentProcess.ProcessMessage = "Please see the upload warning.";
                            }
                            await Save(collection, ClientReferenceNumber, CurrentProcess);
                            break;
                        }

                    case 7:
                        {
                            CurrentProcess.ID = collection["ID"];
                            CurrentProcess.ProcessComplete = 100;
                            CurrentProcess.ProductName = collection["ProductName"];
                            CurrentProcess.ProcessPercentage = CurrentProcess.ProcessComplete.ToString() + "%";
                            CurrentProcess.ProcessHeader = "We're done";
                            CurrentProcess.ProcessMessage = "Thank you for your business! Your reference number is noted below for any further interaction with Absa.";
                            CurrentProcess.ProcessBtnMessage = "Done";

                            await Save(collection, ClientReferenceNumber, CurrentProcess);
                            InvestmentApplication userapp = await RoboAPI.getInvestmentApplication(ClientReferenceNumber);
                            userapp.ApplicationSubmitted = DateTime.Now;
                            userapp = await RoboAPI.postFinalInvestmentApplication(userapp);

                            //TODO: Send system confirmation

                            break;
                        }
                    default:
                        {
                            CurrentProcess.ID = collection["ID"];
                            CurrentProcess.ProcessComplete = 0;
                            CurrentProcess.ProcessPercentage = CurrentProcess.ProcessComplete.ToString() + "%";
                            CurrentProcess.ProcessHeader = "";
                            CurrentProcess.ProcessMessage = "";
                            CurrentProcess.ProcessBtnMessage = "<End>";
                            //CurrentProcess.InvestmentApplication = (InvestmentApplication)Session["InvestmentApplication"];
                            //RoboAPIController RoboAPI = new RoboAPIController();
                            //await RoboAPI.putInvestmentApplication(CurrentProcess.InvestmentApplication);
                            break;
                        }

                }
            }
            catch (Exception ex)
            {
                CurrentProcess.ProcessStage = 99;
                CurrentProcess.ProcessError = "ERROR: " + ex.Message+"</br>Data: " + ex.Data + "</br></br>Source: "+ex.Source+"</br></br> StackTrace: "+ex.StackTrace +"</br>Help:"+ex.HelpLink;

                GlobalDiagnosticsContext.Set("uid", ClientReferenceNumber);
                GlobalDiagnosticsContext.Set("section", collection["ProcessStage"]);
                GlobalDiagnosticsContext.Set("exception", ex.Message);
                GlobalDiagnosticsContext.Set("innerException", ex.InnerException != null ? ex.InnerException.Message : "None");
                GlobalDiagnosticsContext.Set("relatedField", "None"+ ex.Source);
                logger.Error(ex.Message);
            }


            return View(CurrentProcess);
        }
        private static string vOr(string or, string v)
        {
            if (v == null || v == String.Empty || v == "") return or;
            return v;
        }
        private static DateTime? vOr(DateTime? or, string v)
        {
            if (v == null || v == String.Empty || v == "") return or;
            return DateTime.Parse(v);
        }
        private static DateTime vOr(DateTime or, string v)
        {
            if (v == null || v == String.Empty || v == "") return or;
            return DateTime.Parse(v);
        }
        private static bool? vOr(bool? or, string v)
        {
            //if (or.HasValue) return or.Value; 
            if (v == null || v == String.Empty || v == "") return or;
            if (v == "on") return true;
            if (v == "off") return false;
            if (v == "true") return true;
            if (v == "false") return false;
            if (v.Split(',').Length > 1)
            {
                return true;
            }
            bool r = false;
            if (!bool.TryParse(v, out r)) return true;
            return r;
        }

        private static bool vOr(bool or, string v)
        {
            if (v == null || v == String.Empty || v == "") return or;
            if (v == "on") return true;
            if (v == "off") return false;
            if (v == "true") return true;
            if (v == "false") return false;
            if (v.Split(',').Length > 1)
            {
                return true;
            }
            bool r = false;
            if (!bool.TryParse(v, out r)) return true;
            return r;
        }

        private static decimal? vOr(decimal? or, string v)
        {
            if (v == null || v == String.Empty || v == "") return or;
            v = v.Replace(",", CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator);
            return decimal.Parse(v, NumberStyles.Currency, CultureInfo.InvariantCulture);
        }
        private static decimal vOr(decimal or, string v)
        {
            if (v == null || v == String.Empty || v == "") return or;
            v = v.Replace(",", CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator);
            return decimal.Parse(v, NumberStyles.Currency, CultureInfo.InvariantCulture);
        }
        private static int? vOr(int? or, string v)
        {
            if (v == null || v == String.Empty || v == "") return or;

            return int.Parse(v);
        }
        private static int vOr(int or, string v)
        {
            if (v == null || v == String.Empty || v == "") return or;
            return int.Parse(v);
        }

        private static List<TaxDetailsViewModel> vOr(List<IncomeTaxDetail> or, FormCollection v)
        {
            int counter = 0;
            List<TaxDetailsViewModel> details = new List<TaxDetailsViewModel>();

            while (v["IncomeTaxDetails[" + counter + "].IncomeTaxNumber"] != null && v["IncomeTaxDetails[" + counter + "].IncomeTaxNumber"] != "")
            {
                var taxNumber = v["IncomeTaxDetails[" + counter + "].IncomeTaxNumber"];
                var country = v["IncomeTaxDetails[" + counter + "].CountryOfIssue"];

                if (!isValidNumber(taxNumber) || !isValidString(country))
                {
                    return (from i in or
                            select new TaxDetailsViewModel { IncomeTaxNumber = taxNumber, CountryOfIssue = country }).ToList();
                }
                //Stops adding of duplicate tax numbers
                if (!or.Any(x => x.IncomeTaxNumber == taxNumber && x.CounntryOfIssue == country))
                {
                    details.Add(new TaxDetailsViewModel { IncomeTaxNumber = taxNumber, CountryOfIssue = country });
                }

                ++counter;
            }

            return details;
        }

        private static List<AdditionalPassportDetailsModel> vOr(List<AdditionalPassportDetail> or, FormCollection v)
        {
            int counter = 0;
            List<AdditionalPassportDetailsModel> details = new List<AdditionalPassportDetailsModel>();

            while (v["AdditionalPassportDetails[" + counter + "].PassportNumber"] != null && v["AdditionalPassportDetails[" + counter + "].PassportNumber"] != "")
            {
                var passportNumber = v["AdditionalPassportDetails[" + counter + "].PassportNumber"];
                var countryOfIssue = v["AdditionalPassportDetails[" + counter + "].CountryOfIssue"];
                var dateOfIssue = Convert.ToDateTime(v["AdditionalPassportDetails[" + counter + "].DateOfIssue"]);

                DateTime pexp;
                if ((isValidDate(v["DateOfIssue"], out pexp) && isFutureDate(pexp)) || !isValidNumber(passportNumber) || !isValidString(countryOfIssue))
                {
                    return (from i in or
                            select new AdditionalPassportDetailsModel { PassportNumber = passportNumber, CountryOfIssue = countryOfIssue, DateOfIssue = dateOfIssue }).ToList();
                }

                //Stops adding of duplicate tax numbers
                if (!or.Any(x => x.PassportNumber == passportNumber && x.CountryOfIssue == countryOfIssue && x.DateOfIssue == dateOfIssue))
                {
                    details.Add(new AdditionalPassportDetailsModel { PassportNumber = passportNumber, CountryOfIssue = countryOfIssue, DateOfIssue = dateOfIssue });
                }

                ++counter;
            }

            return details;
        }
        private static async Task<DetailsViewModel> TransferModel(DetailsViewModel model,FormCollection collection, string ClientReferenceNumber)
        {
            try
            {
                RoboAPIController RoboAPI = new RoboAPIController();
                InvestmentApplication userapp = await RoboAPI.getInvestmentApplication(ClientReferenceNumber);
                DateTime SQLMinDate = new DateTime(1901, 01, 01);

                model.ClientType = vOr(userapp.ClientType, collection["ClientType"]);
                model.Title2 = vOr(userapp.Title, collection["Title2"]);

                model.Name = vOr(userapp.Name, collection["Name"]);
                model.Surname = vOr(userapp.Surname, collection["Surname"]);
                model.Gender = vOr(userapp.Gender, collection["Gender"]);
                model.MaritalStatus = vOr(userapp.MaritalStatus, collection["MaritalStatus"]);
                model.IDPassportNumber = vOr(userapp.IDPassportNumber, collection["IDPassportNumber"]);
                if (vOr(userapp.BirthDate, collection["BirthDate"]).Value != null)
                {
                    model.BirthDate = vOr(userapp.BirthDate, collection["BirthDate"]).Value.ToShortDateString();
                }

                model.IDType = vOr(userapp.IDType, collection["IDType"]);
                var detailsBefore = userapp.IncomeTaxDetails;
                if(userapp.IncomeTaxDetails==null || userapp.IncomeTaxDetails.Count == 0)
                    model.IncomeTaxDetails = vOr(userapp.IncomeTaxDetails.ToList(), collection);
                if (userapp.AdditionalPassportDetails == null || userapp.AdditionalPassportDetails.Count == 0)
                    model.AdditionalPassportDetails = vOr(userapp.AdditionalPassportDetails.ToList(), collection);
                if (model.IncomeTaxDetails == null || model.IncomeTaxDetails.Count == 0)
                {
                    model.IncomeTaxDetails = new List<TaxDetailsViewModel>();
                    model.IncomeTaxDetails.Add(new TaxDetailsViewModel { IncomeTaxNumber = "", CountryOfIssue = "" });
                }

                if (model.AdditionalPassportDetails == null || model.AdditionalPassportDetails.Count == 0)
                {
                    model.AdditionalPassportDetails = new List<AdditionalPassportDetailsModel>();
                    model.AdditionalPassportDetails.Add(new AdditionalPassportDetailsModel { PassportNumber = "", CountryOfIssue = "", DateOfIssue = DateTime.Today });
                }
                model.TaxResident = vOr(userapp.SATaxResidentOnly, collection["TaxResident"]);
                model.TempResident = vOr(userapp.TempResident, collection["TempResident"]);

                if (model.TempResident == true)
                {
                    model.TempResidentNumber = vOr(userapp.TempResidentNumber, collection["TempResidentNumber"]);
                    //if (vOr(userapp.BirthDate, collection["PermitExpiryDate"]).Value != null)
                    model.PermitExpiryDate = vOr(model.PermitExpiryDate, collection["PermitExpiryDate"]);
                }
                else
                {
                    model.TempResidentNumber = "";
                    model.PermitExpiryDate = null;
                }

                model.PostResAddress = vOr((Nullable<bool>)null, collection["PostResAdress"]);
                model.Adress = vOr(userapp.StreetAdressLine1, collection["Adress"]);
                model.PostalCode = vOr(userapp.StreetAdressLine2, collection["PostalCode"]);
                model.City = vOr(userapp.City, collection["City"]);
                model.Province = vOr(userapp.Province, collection["Province"]);
                model.PostAdress = vOr(userapp.PostAddress, collection["PostAdress"]);
                model.PostPostalCode = vOr(userapp.PostPostalCode, collection["PostPostalCode"]);
                model.PostCity = vOr(userapp.PostCity, collection["PostCity"]);
                model.PostProvince = vOr(userapp.PostProvince, collection["PostProvince"]);
                model.PhoneHome = vOr(userapp.PhoneHome, collection["PhoneHome"]);
                model.PhoneWork = vOr(userapp.PhoneWork, collection["PhoneWork"]);
                model.Mobile = vOr(userapp.Mobile, collection["Mobile"]);
                model.EMail = vOr(userapp.EMail, collection["EMail"]);
                model.SecondaryEMail = vOr(userapp.SecondaryEMail, collection["SecondaryEMail"]);
                model.KinName = vOr(userapp.KinName, collection["KinName"]);
                model.KinSurname = vOr(userapp.KinSurname, collection["KinSurname"]);
                model.KinMobile = vOr(userapp.KinMobile, collection["KinMobile"]);
                model.FundManagersConsent = vOr(userapp.FundManagersConsent, collection["FundManagersConsent"]) ?? false;
                model.PostConsent = vOr(userapp.PostConsent, collection["PostConsent"]) ?? false;
                model.SMSConsent = vOr(userapp.SMSConsent, collection["SMSConsent"]) ?? false;
                model.PhoneConsent = vOr(userapp.PhoneConsent, collection["PhoneConsent"]) ?? false;
                model.EMailConsent = vOr(userapp.EMailConsent, collection["EMailConsent"]) ?? false;
                model.IranConfirm = vOr(userapp.IranConfirm, collection["IranConfirm"]);
                model.SouthSudanConfirm = vOr(userapp.SouthSudanConfirm, collection["SouthSudanConfirm"]);
                model.NorthKoreaConfirm = vOr(userapp.NorthKoreaConfirm, collection["NorthKoreaConfirm"]);
                model.CubaConfirm = vOr(userapp.CubaConfirm, collection["CubaConfirm"]);
                model.SyriaConfirm = vOr(userapp.SyriaConfirm, collection["SyriaConfirm"]);
                model.CrimeaRegionConfirm = vOr(userapp.CrimeaRegionConfirm, collection["CrimeaRegionConfirm"]);
                model.NorthSudanConfirm = vOr(userapp.NorthSudanConfirm, collection["NorthSudanConfirm"]);
                model.LumpSumInvestment = vOr(userapp.LumpSumInvestment, collection["LumpSumInvestment"]);
                model.MonthlyDebitOrder = vOr(userapp.MonthlyDebitOrder, collection["MonthlyDebitOrder"]);
                model.UnitInvestConfirm = vOr(userapp.UnitInvestConfirm, collection["UnitInvestConfirm"]);
                model.ReinvestAccountHolder = vOr(userapp.ReinvestAccountHolder, collection["ReinvestAccountHolder"]);
                model.ReinvestBank = vOr(userapp.ReinvestBank, collection["ReinvestBank"]);
                model.ReinvestAccount = vOr(userapp.ReinvestAccount, collection["ReinvestAccount"]);
                model.ReinvestAccountType = vOr(userapp.ReinvestAccountType, collection["ReinvestAccountType"]);
                model.ReinvestBranchCode = vOr(userapp.ReinvestBranchCode, collection["ReinvestBranchCode"]);
                model.Increase0 = vOr(userapp.Increase0, collection["IncreaseInvestment"] == "0%" ? "true" : "false");
                model.Increase10 = vOr(userapp.Increase10, collection["IncreaseInvestment"] == "10%" ? "true" : "false");
                model.Increase15 = vOr(userapp.Increase15, collection["IncreaseInvestment"] == "15%" ? "true" : "false");
                model.Increase20 = vOr(userapp.Increase20, collection["IncreaseInvestment"] == "20%" ? "true" : "false");

                if (model.Increase0 == true)
                    model.IncreaseInvestment = "0%";
                if (model.Increase10 == true)
                    model.IncreaseInvestment = "10%";
                else if (model.Increase15 == true)
                    model.IncreaseInvestment = "15%";
                else if (model.Increase20 == true)
                    model.IncreaseInvestment = "20%";
                else
                    model.IncreaseInvestment = null;

                model.DebitOrderAccountHolder = vOr(userapp.DebitOrderAccountHolder, collection["DebitOrderAccountHolder"]);
                model.DebitOrderBank = vOr(userapp.DebitOrderBank, collection["DebitOrderBank"]);
                model.DebitOrderAccountNumber = vOr(userapp.DebitOrderAccountNumber, collection["DebitOrderAccountNumber"]);
                model.DebitOrderBranchCode = vOr(userapp.DebitOrderBranchCode, collection["DebitOrderBranchCode"]);
                model.DebitOrderAmount = vOr(userapp.DebitOrderAmount, collection["DebitOrderAmount"]);
                model.DebitOrderDay = vOr(userapp.DebitOrderDay, collection["DebitOrderDay"]);
                model.DebitOrderAccountType = vOr(userapp.DebitOrderAccountType, collection["DebitOrderAccountType"]);
                if (isValidDate(userapp.DebitOrderFromMonth))
                {
                    model.DebitOrderFromMonth = vOr(DateTime.Parse(userapp.DebitOrderFromMonth), collection["DebitOrderFromMonth"]);
                }
                model.RedemptionAccountHolder = vOr(userapp.RedemptionAccountHolder, collection["RedemptionAccountHolder"]);
                model.RedemptionBank = vOr(userapp.RedemptionBank, collection["RedemptionBank"]);
                model.RedemptionAccountNumber = vOr(userapp.RedemptionAccountNumber, collection["RedemptionAccountNumber"]);
                model.RedemptionBranchCode = vOr(userapp.RedemptionBranchCode, collection["RedemptionBranchCode"]);
                model.RedemptionAccountType = vOr(userapp.RedemptionAccountType, collection["RedemptionAccountType"]);
                model.CashflowplanConfirm = vOr(userapp.CashflowplanConfirm, collection["CashflowplanConfirm"] ?? "false");

                model.income_salaryWages = vOr(userapp.Income_SalaryWages, collection["income_salaryWages"]) ?? false;
                model.income_commission = vOr(userapp.Income_Commission, collection["income_commission"]) ?? false;
                model.income_bonus = vOr(userapp.Income_Bonus, collection["income_bonus"]) ?? false;
                model.income_maintenance = vOr(userapp.Income_Maintenance, collection["income_maintenance"]) ?? false;
                model.income_pension = vOr(userapp.Income_Pension, collection["income_pension"]) ?? false;
                model.income_investments = vOr(userapp.Income_Investments, collection["income_investments"]) ?? false;
                model.income_insuranceClaim = vOr(userapp.Income_InsuranceClaim, collection["income_insuranceClaim"]) ?? false;
                model.income_allowance = vOr(userapp.Income_Allowance, collection["income_allowance"]) ?? false;
                model.income_donationGift = vOr(userapp.Income_DonationGift, collection["income_donationGift"]) ?? false;
                model.income_inheritance = vOr(userapp.Income_Inheritance, collection["income_inheritance"]) ?? false;
                model.income_socialGrant = vOr(userapp.Income_SocialGrant, collection["income_socialGrant"]) ?? false;
                model.income_retirementAnnuity = vOr(userapp.Income_RetirementAnnuity, collection["income_retirementAnnuity"]) ?? false;
                model.income_otherCheckBox = vOr(userapp.Income_Other, collection["income_otherCheckBox"]) ?? false;
                model.income_other = vOr(userapp.Income_OtherDescription, collection["income_other"]);
                model.sourceOfWealth = vOr(userapp.SourceOfWealth, collection["sourceOfWealth"]);

                model.funds_salaryWages = vOr(userapp.Funds_SalaryWages, collection["funds_salaryWages"]) ?? false;
                model.funds_commission = vOr(userapp.Funds_Commission, collection["funds_commission"]) ?? false;
                model.funds_bonus = vOr(userapp.Funds_Bonus, collection["funds_bonus"]) ?? false;
                model.funds_maintenance = vOr(userapp.Funds_Maintenance, collection["funds_maintenance"]) ?? false;
                model.funds_pension = vOr(userapp.Funds_Pension, collection["funds_pension"]) ?? false;
                model.funds_investments = vOr(userapp.Funds_Investments, collection["funds_investments"]) ?? false;
                model.funds_insuranceClaim = vOr(userapp.Funds_InsuranceClaim, collection["funds_insuranceClaim"]) ?? false;
                model.funds_allowance = vOr(userapp.Funds_Allowance, collection["funds_allowance"]) ?? false;
                model.funds_donationGift = vOr(userapp.Funds_DonationGift, collection["funds_donationGift"]) ?? false;
                model.funds_inheritance = vOr(userapp.Funds_Inheritance, collection["funds_inheritance"]) ?? false;
                model.funds_socialGrant = vOr(userapp.Funds_SocialGrant, collection["funds_socialGrant"]) ?? false;
                model.funds_retirementAnnuity = vOr(userapp.Funds_RetirementAnnuity, collection["funds_retirementAnnuity"]) ?? false;
                model.funds_otherCheckBox = vOr(userapp.Funds_Other, collection["funds_otherCheckBox"]) ?? false;
                model.funds_other = vOr(userapp.Funds_OtherDescription, collection["funds_other"]);
            }
            catch (Exception ex)
            {
                string exs = ex.Message;
                Console.WriteLine("Transfer:" + exs);

                GlobalDiagnosticsContext.Set("uid", ClientReferenceNumber);
                GlobalDiagnosticsContext.Set("section", collection["ProcessStage"]);
                GlobalDiagnosticsContext.Set("exception", ex.Message);
                GlobalDiagnosticsContext.Set("innerException", ex.InnerException != null ? ex.InnerException.Message : "None");
                GlobalDiagnosticsContext.Set("relatedField", "None");
                logger.Error(ex.Message);
            }
            return model;
        }
        private static async Task<DetailsViewModel> Transfer(FormCollection collection, string ClientReferenceNumber)
        {
            DetailsViewModel model = new DetailsViewModel();
            
            await TransferModel(model, collection, ClientReferenceNumber);
            return model;    

        }
        private async Task Save(FormCollection collection, string ClientReferenceNumber, DetailsViewModel CurrentProcess)
        {
            try
            {
                logger.Info("Trying to Save " + ClientReferenceNumber);

                RoboAPIController RoboAPI = new RoboAPIController();
                InvestmentApplication userapp = await RoboAPI.getInvestmentApplication(ClientReferenceNumber);
                DateTime SQLMinDate = new DateTime(1901, 01, 01);
                if (CurrentProcess != null)
                {
                    CurrentProcess.IDUploaded = userapp.IDCopy != null;
                    CurrentProcess.PORUploaded = userapp.ProofOfAdress != null;

                }

                userapp.ClientType = "Individual";//vOr(userapp.ClientType, collection["ClientType"]);
                userapp.Title = vOr(userapp.Title, collection["Title2"]);
                userapp.ClientFullName = vOr(userapp.ClientFullName, collection["SignatureFullName"] ?? "");
                userapp.Name = vOr(userapp.Name, collection["Name"]);
                userapp.Surname = vOr(userapp.Surname, collection["Surname"]);
                userapp.Gender = vOr(userapp.Gender, collection["Gender"]);
                userapp.MaritalStatus = vOr(userapp.MaritalStatus, collection["MaritalStatus"]);
                userapp.IDPassportNumber = vOr(userapp.IDPassportNumber, collection["IDPassportNumber"]);
                userapp.BirthDate = vOr(userapp.BirthDate, collection["BirthDate"]);
                userapp.IDType = vOr(userapp.IDType, collection["IDType"]);
                userapp.SATaxResidentOnly = vOr(userapp.SATaxResidentOnly, collection["TaxResident"]);
                userapp.TempResident = vOr(userapp.TempResident, collection["TempResident"]);

                if (userapp.TempResident == true)
                {
                    userapp.TempResidentNumber = vOr(userapp.TempResidentNumber, collection["TempResidentNumber"]);
                    userapp.PermitExpiryDate = vOr(userapp.PermitExpiryDate, collection["PermitExpiryDate"]);
                }
                else
                {
                    userapp.TempResidentNumber = "";
                    userapp.PermitExpiryDate = null;
                }

                userapp.IncomeTaxDetails = (from i in vOr(userapp.IncomeTaxDetails.ToList(), collection)
                                            select new IncomeTaxDetail
                                            {
                                                //ClientReferenceNumber = ClientReferenceNumber,
                                                IncomeTaxNumber = i.IncomeTaxNumber,
                                                CounntryOfIssue = i.CountryOfIssue,
                                                ClientApplicationId = userapp.Id
                                            }).ToList();

                userapp.AdditionalPassportDetails = (from i in vOr(userapp.AdditionalPassportDetails.ToList(), collection)
                                                     select new AdditionalPassportDetail
                                                     {
                                                         PassportNumber = i.PassportNumber,
                                                         CountryOfIssue = i.CountryOfIssue,
                                                         DateOfIssue = i.DateOfIssue.Date,
                                                         ClientApplicationId = userapp.Id,
                                                     }).ToList();

                userapp.StreetAdressLine1 = vOr(userapp.StreetAdressLine1, collection["Adress"]);
                userapp.StreetAdressLine2 = vOr(userapp.StreetAdressLine2, collection["PostalCode"]);
                userapp.City = vOr(userapp.City, collection["City"]);
                userapp.Province = vOr(userapp.Province, collection["Province"]);
                userapp.PostAddress = vOr(userapp.PostAddress, collection["PostAdress"]);
                userapp.PostPostalCode = vOr(userapp.PostPostalCode, collection["PostPostalCode"]);
                userapp.PostCity = vOr(userapp.PostCity, collection["PostCity"]);
                userapp.PostProvince = vOr(userapp.PostProvince, collection["PostProvince"]);

                if (collection["PhoneHome"] != null && collection["PhoneHome"] != "")
                    userapp.PhoneHome = collection["PhoneHome"].StartsWith(collection["CountryCodeHome"])? collection["PhoneHome"] : collection["CountryCodeHome"] + " " + collection["PhoneHome"];

                if (collection["PhoneWork"] != null && collection["PhoneWork"] != "")
                    userapp.PhoneWork = collection["PhoneWork"].StartsWith(collection["CountryCodeWork"]) ? collection["PhoneWork"] : collection["CountryCodeWork"] + " " + collection["PhoneWork"];
                
                if (collection["Mobile"] != null && collection["Mobile"] != "")
                    userapp.Mobile = collection["Mobile"].StartsWith(collection["CountryCodeMobile"]) ? collection["Mobile"] : collection["CountryCodeMobile"] + " " + collection["Mobile"];

                userapp.EMail = vOr(userapp.EMail, collection["EMail"]);
                userapp.SecondaryEMail = vOr(userapp.SecondaryEMail, collection["SecondaryEMail"]);
                userapp.KinName = vOr(userapp.KinName, collection["KinName"]);
                userapp.KinSurname = vOr(userapp.KinSurname, collection["KinSurname"]);

                if (collection["KinMobile"] != null && collection["KinMobile"] != "")
                    userapp.KinMobile = collection["CountryCodeKin"] + " " + collection["KinMobile"];

                userapp.FundManagersConsent = vOr(userapp.FundManagersConsent, collection["FundManagersConsent"] ?? "false");
                userapp.PostConsent = vOr(userapp.PostConsent, collection["PostConsent"] ?? "false");
                userapp.SMSConsent = vOr(userapp.SMSConsent, collection["SMSConsent"] ?? "false");
                userapp.PhoneConsent = vOr(userapp.PhoneConsent, collection["PhoneConsent"] ?? "false");
                userapp.EMailConsent = vOr(userapp.EMailConsent, collection["EMailConsent"] ?? "false");
                userapp.POPIConsent = vOr(CurrentProcess.PhoneConsent, collection["POPI"]);
                userapp.IranConfirm = vOr(userapp.IranConfirm, collection["IranConfirm"]);
                userapp.SouthSudanConfirm = vOr(userapp.SouthSudanConfirm, collection["SouthSudanConfirm"]);
                userapp.NorthKoreaConfirm = vOr(userapp.NorthKoreaConfirm, collection["NorthKoreaConfirm"]);
                userapp.CubaConfirm = vOr(userapp.CubaConfirm, collection["CubaConfirm"]);
                userapp.SyriaConfirm = vOr(userapp.SyriaConfirm, collection["SyriaConfirm"]);
                userapp.CrimeaRegionConfirm = vOr(userapp.CrimeaRegionConfirm, collection["CrimeaRegionConfirm"]);
                userapp.NorthSudanConfirm = vOr(userapp.NorthSudanConfirm, collection["NorthSudanConfirm"]);

                userapp.LumpSumInvestment = vOr(userapp.LumpSumInvestment, collection["LumpSumInvestment"]);
                userapp.MonthlyDebitOrder = vOr(userapp.MonthlyDebitOrder, collection["MonthlyDebitOrder"]);
                userapp.UnitInvestConfirm = vOr(userapp.UnitInvestConfirm, collection["UnitInvestConfirm"]);
                userapp.ReinvestAccountHolder = vOr(userapp.ReinvestAccountHolder, collection["ReinvestAccountHolder"]);
                userapp.ReinvestBank = vOr(userapp.ReinvestBank, collection["ReinvestBank"]);
                userapp.ReinvestAccount = vOr(userapp.ReinvestAccount, collection["ReinvestAccount"]);
                userapp.ReinvestAccountType = vOr(userapp.ReinvestAccountType, collection["ReinvestAccountType"]);
                userapp.ReinvestBranchCode = vOr(userapp.ReinvestBranchCode, collection["ReinvestBranchCode"]);

                userapp.Increase10 = vOr(userapp.Increase10, collection["IncreaseInvestment"] == "10%" ? "true" : "false");
                userapp.Increase15 = vOr(userapp.Increase15, collection["IncreaseInvestment"] == "15%" ? "true" : "false");
                userapp.Increase20 = vOr(userapp.Increase20, collection["IncreaseInvestment"] == "20%" ? "true" : "false");
                userapp.Increase0 = vOr(userapp.Increase0, collection["IncreaseInvestment"] == "0%" ? "true" : "false");

                userapp.DebitOrderAccountHolder = vOr(userapp.DebitOrderAccountHolder, collection["DebitOrderAccountHolder"]);
                userapp.DebitOrderBank = vOr(userapp.DebitOrderBank, collection["DebitOrderBank"]);
                userapp.DebitOrderAccountNumber = vOr(userapp.DebitOrderAccountNumber, collection["DebitOrderAccountNumber"]);
                userapp.DebitOrderBranchCode = vOr(userapp.DebitOrderBranchCode, collection["DebitOrderBranchCode"]);
                userapp.DebitOrderAmount = vOr(userapp.DebitOrderAmount, collection["DebitOrderAmount"]);
                userapp.DebitOrderDay = vOr(userapp.DebitOrderDay, collection["DebitOrderDay"]);
                userapp.DebitOrderFromMonth = vOr(userapp.DebitOrderFromMonth, collection["DebitOrderFromMonth"]);
                userapp.DebitOrderAccountType = vOr(userapp.DebitOrderAccountType, collection["DebitOrderAccountType"]);
                userapp.RedemptionAccountHolder = vOr(userapp.RedemptionAccountHolder, collection["RedemptionAccountHolder"]);
                userapp.RedemptionBank = vOr(userapp.RedemptionBank, collection["RedemptionBank"]);
                userapp.RedemptionAccountNumber = vOr(userapp.RedemptionAccountNumber, collection["RedemptionAccountNumber"]);
                userapp.RedemptionBranchCode = vOr(userapp.RedemptionBranchCode, collection["RedemptionBranchCode"]);
                userapp.RedemptionAccountType = vOr(userapp.RedemptionAccountType, collection["RedemptionAccountType"]);
                userapp.CashflowplanConfirm = vOr(userapp.CashflowplanConfirm, collection["CashflowplanConfirm"] ?? "false");
                userapp.CashFlowPlanDate = vOr(userapp.CashFlowPlanDate, collection["CashFlowPlanDate"]);
                userapp.CashFlowPlanAmount = vOr(userapp.CashFlowPlanAmount, collection["CashFlowPlanAmount"]);
                /// N.B.!! DeclarationAccept
                userapp.AcceptTerms = vOr(userapp.AcceptTerms, collection["AcceptTerms"]);
                userapp.AcceptSignature = vOr(userapp.AcceptSignature, collection["AcceptSignature"]);
                userapp.DeclarationAccept = vOr(userapp.DeclarationAccept, collection["DeclarationAccept"]);

                userapp.Income_SalaryWages = vOr(userapp.Income_SalaryWages, collection["income_salaryWages"] ?? "false");
                userapp.Income_Commission = vOr(userapp.Income_Commission, collection["income_commission"] ?? "false");
                userapp.Income_Bonus = vOr(userapp.Income_Bonus, collection["income_bonus"] ?? "false");
                userapp.Income_Maintenance = vOr(userapp.Income_Maintenance, collection["income_maintenance"] ?? "false");
                userapp.Income_Pension = vOr(userapp.Income_Pension, collection["income_pension"] ?? "false");
                userapp.Income_Investments = vOr(userapp.Income_Investments, collection["income_investments"] ?? "false");
                userapp.Income_InsuranceClaim = vOr(userapp.Income_InsuranceClaim, collection["income_insuranceClaim"] ?? "false");
                userapp.Income_Allowance = vOr(userapp.Income_Allowance, collection["income_allowance"] ?? "false");
                userapp.Income_DonationGift = vOr(userapp.Income_DonationGift, collection["income_donationGift"] ?? "false");
                userapp.Income_Inheritance = vOr(userapp.Income_Inheritance, collection["income_inheritance"] ?? "false");
                userapp.Income_SocialGrant = vOr(userapp.Income_SocialGrant, collection["income_socialGrant"] ?? "false");
                userapp.Income_RetirementAnnuity = vOr(userapp.Income_RetirementAnnuity, collection["income_retirementAnnuity"] ?? "false");
                userapp.Income_Other = vOr(userapp.Income_Other, collection["income_otherCheckBox"] ?? "false");
                if (userapp.Income_Other == true)
                    userapp.Income_OtherDescription = vOr(userapp.Income_OtherDescription, collection["income_other"]);
                else
                    userapp.Income_OtherDescription = "";
                userapp.SourceOfWealth = vOr(userapp.SourceOfWealth, collection["sourceOfWealth"]);

                userapp.Funds_SalaryWages = vOr(userapp.Funds_SalaryWages, collection["funds_salaryWages"] ?? "false");
                userapp.Funds_Commission = vOr(userapp.Funds_Commission, collection["funds_commission"] ?? "false");
                userapp.Funds_Bonus = vOr(userapp.Funds_Bonus, collection["funds_bonus"] ?? "false");
                userapp.Funds_Maintenance = vOr(userapp.Funds_Maintenance, collection["funds_maintenance"] ?? "false");
                userapp.Funds_Pension = vOr(userapp.Funds_Pension, collection["funds_pension"] ?? "false");
                userapp.Funds_Investments = vOr(userapp.Funds_Investments, collection["funds_investments"] ?? "false");
                userapp.Funds_InsuranceClaim = vOr(userapp.Funds_InsuranceClaim, collection["funds_insuranceClaim"] ?? "false");
                userapp.Funds_Allowance = vOr(userapp.Funds_Allowance, collection["funds_allowance"] ?? "false");
                userapp.Funds_DonationGift = vOr(userapp.Funds_DonationGift, collection["funds_donationGift"] ?? "false");
                userapp.Funds_Inheritance = vOr(userapp.Funds_Inheritance, collection["funds_inheritance"] ?? "false");
                userapp.Funds_SocialGrant = vOr(userapp.Funds_SocialGrant, collection["funds_socialGrant"] ?? "false");
                userapp.Funds_RetirementAnnuity = vOr(userapp.Funds_RetirementAnnuity, collection["funds_retirementAnnuity"] ?? "false");
                userapp.Funds_Other = vOr(userapp.Funds_Other, collection["funds_otherCheckBox"] ?? "false");
                if (userapp.Funds_Other == true)
                    userapp.Funds_OtherDescription = vOr(userapp.Funds_OtherDescription, collection["funds_other"]);
                else
                    userapp.Funds_OtherDescription = "";
                /// 
                logger.Info("Saving " + userapp.ClientReferenceNumber);
                userapp = await RoboAPI.putInvestmentApplication(userapp);
                logger.Info("Saved " + userapp.ClientReferenceNumber);

                Session["InvestmentApplication"] = userapp;

            }

            catch (Exception ex)
            {
                string exs = ex.Message;
                GlobalDiagnosticsContext.Set("uid", ClientReferenceNumber);
                GlobalDiagnosticsContext.Set("section", collection["ProcessStage"]);
                GlobalDiagnosticsContext.Set("exception", ex.Message);
                GlobalDiagnosticsContext.Set("innerException", ex.InnerException != null ? ex.InnerException.Message : "None");
                GlobalDiagnosticsContext.Set("relatedField", "None");
                logger.Error(ex.Message);
            }

        }



        private static async Task<InvestmentApplication> Tester()
        {
            //test API
            RoboAPIController RoboAPI = new RoboAPIController();
            //Configuration config = await RoboAPI.getConfiguration(1);
            //InvestmentApplication iva = await RoboAPI.getInvestmentApplication("0X1Y2Z");
            InvestmentApplication iva = new InvestmentApplication();

            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[6];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            var finalString = new String(stringChars); ;

            iva.ClientReferenceNumber = finalString;
            iva.RecommendedProductCode = "AA";
            iva.RecommendedProductName = "AAAA";
            string filename = @"C:\Users\Rene Pretorius\Downloads\Black_Cat.pdf";

            iva.RiskReport = System.IO.File.ReadAllBytes(filename);
            iva.RecordOfAdvice = System.IO.File.ReadAllBytes(filename);

            iva.ApplicationRecieved = DateTime.Now;
            iva.ClientAcceptTCsTimeStamp = DateTime.Now;
            iva.ClientIndicatesSignatureTimeStamp = DateTime.Now;
            iva.ApplicationSubmitted = DateTime.Now;

            iva = await RoboAPI.postInvestmentApplication(iva);


            return iva;
        }

        // GET: Process/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Process/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Process/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Submit(DetailsViewModel currentProcess)
        {

            return View(currentProcess);
        }

        // POST: Process/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Process/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Process/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
