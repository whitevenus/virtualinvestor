﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Robo.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using RoboData;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web.Script.Serialization;
using System.Net;

namespace Robo.Controllers
{
    public class RoboAPIController : Controller
    {
        HttpClient client;
        //The URL of the WEB API Service
        string url = System.Configuration.ConfigurationManager.AppSettings["RoboAPIBaseUri"];


        public RoboAPIController()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var credentials = Encoding.ASCII.GetBytes(System.Configuration.ConfigurationManager.AppSettings["RoboAPIusername"] + ":" + System.Configuration.ConfigurationManager.AppSettings["RoboAPIpassword"]);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));
            client.Timeout = TimeSpan.FromMinutes(30);
        }

        public async Task<Configuration> getConfiguration(int id)
        {
            HttpResponseMessage responseMessage = await client.GetAsync(url + "/Configurations/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                var Configuration = JsonConvert.DeserializeObject<Configuration>(responseData);

                return Configuration;
            }
            return null;
        }
        //private static async Task<string> GetAPIToken(string userName, string password)
        //{
        //    using (var client = new HttpClient())
        //    {
        //        //setup client
        //        client.BaseAddress = new Uri(baseUri);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        //setup login data
        //        var formContent = new FormUrlEncodedContent(new[]
        //        {
        //            new KeyValuePair<string, string>("grant_type", "password"),
        //            new KeyValuePair<string, string>("username", userName),
        //            new KeyValuePair<string, string>("password", password),
        //        });

        //        //send request
        //        HttpResponseMessage responseMessage = await client.PostAsync("/Token", formContent);

        //        //get access token from response body
        //        var responseJson = await responseMessage.Content.ReadAsStringAsync();
        //        var jObject = JObject.Parse(responseJson);
        //        return jObject.GetValue("access_token").ToString();
        //    }
        //}
        //static async Task<string> GetRequest(string token, string requestPath)
        //{
        //    using (var client = new HttpClient())
        //    {
        //        //setup client
        //        client.BaseAddress = new Uri(baseUri);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

        //        //make request
        //        HttpResponseMessage response = await client.GetAsync(requestPath);
        //        var responseString = await response.Content.ReadAsStringAsync();
        //        return responseString;
        //    }
        //}
        // api/InvestmentApplications/0X1Y2Z

        public async Task<bool> sendContactDetails(Dictionary<string, string> contactInfo)
        {
            var responseContent = "";
            var applicationInfo = JsonConvert.SerializeObject(contactInfo);
            var content = new StringContent(applicationInfo, Encoding.UTF8, "application/json");

     
                var httpResponse = await client.PostAsync(url + "/SendContactDetails/", content);
    
            

            if (httpResponse.Content != null)
            {
                responseContent = await httpResponse.Content.ReadAsStringAsync();
                httpResponse.EnsureSuccessStatusCode();
                return true;
            }
            return false;
        }
        
        public async Task<InvestmentApplication> getInvestmentApplication(string clientRef)
        {
            HttpResponseMessage responseMessage = await client.GetAsync(url + "/InvestmentApplications/" + clientRef);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                var InvestmentApplication = JsonConvert.DeserializeObject<InvestmentApplication>(responseData);

                return InvestmentApplication;
            }
            return null;
        }
        // api/InvestmentApplications/[InvestmentApplication]

        public async Task<InvestmentApplication> postInvestmentApplication(InvestmentApplication investmentApplication)
        {
            var responseContent = "";
            var InvestmentApplication = JsonConvert.SerializeObject(investmentApplication);
            var content = new StringContent(InvestmentApplication, Encoding.UTF8, "application/json");        
           

            // Do the actual request and await the response
            var httpResponse = await client.PostAsync(url + "/PostInvestmentApplications/", content);

            // If the response contains content we want to read it!
            if (httpResponse.Content != null)
            {
                    responseContent = await httpResponse.Content.ReadAsStringAsync();
                httpResponse.EnsureSuccessStatusCode();
              
            }
            InvestmentApplication returnIVA = new RoboData.InvestmentApplication();
            JavaScriptSerializer js = new JavaScriptSerializer();
          
            returnIVA = JsonConvert.DeserializeObject<InvestmentApplication>(responseContent);
            
            return returnIVA;
        }

        public async Task<InvestmentApplication> postFinalInvestmentApplication(InvestmentApplication investmentApplication)
        {
            try
            {
                var responseContent = "";
                var InvestmentApplication = JsonConvert.SerializeObject(investmentApplication);
                var content = new StringContent(InvestmentApplication, Encoding.UTF8, "application/json");

                // Do the actual request and await the response
                var httpResponse = await client.PostAsync(url + "/FinalInvestmentApplications/", content);

                // If the response contains content we want to read it!
                if (httpResponse.Content != null)
                {
                    responseContent = await httpResponse.Content.ReadAsStringAsync();
                    httpResponse.EnsureSuccessStatusCode();

                }
                InvestmentApplication returnIVA = new RoboData.InvestmentApplication();
                JavaScriptSerializer js = new JavaScriptSerializer();

                returnIVA = JsonConvert.DeserializeObject<InvestmentApplication>(responseContent);
                
                return returnIVA;
            }catch(Exception ex)
            {
                throw ex;
            }
        }
        // api/InvestmentApplications/[InvestmentApplication]

        public async Task<InvestmentApplication> putInvestmentApplication(InvestmentApplication investmentApplication)
        {
            try
            {
                var responseContent = "";
                var InvestmentApplication = JsonConvert.SerializeObject(investmentApplication);
                var iva = new StringContent(InvestmentApplication, Encoding.UTF8, "application/json");
                //var id = new StringContent(investmentApplication.Id.ToString(), Encoding.UTF8, "application/json");
                //var parameters = new Dictionary<string, string> { { "id", investmentApplication.Id.ToString() }, { "InvestmentApplication", InvestmentApplication } };
                //var encodedContent = new StringContent(JsonConvert.SerializeObject( parameters), Encoding.UTF8, "application/json");


                // Do the actual request and await the response
                var httpResponse = await client.PostAsync(url + "/PostInvestmentApplications", iva);

                // If the response contains content we want to read it!
                if (httpResponse.Content != null)
                {
                    responseContent = await httpResponse.Content.ReadAsStringAsync();
                    httpResponse.EnsureSuccessStatusCode();

                }
                InvestmentApplication returnIVA = new RoboData.InvestmentApplication();
                JavaScriptSerializer js = new JavaScriptSerializer();

                returnIVA = JsonConvert.DeserializeObject<InvestmentApplication>(responseContent);


                return returnIVA;
            }catch(Exception ee)
            {
                throw ee;
            }
        }

        public async Task<Dictionary<string, string>> getListOfCountries()
        {
            HttpResponseMessage responseMessage = await client.GetAsync(url + "/CountryList");

            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                var countries = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseData);

                return countries;
            }

            return null;
        }

        public async Task<Dictionary<string, string>> getBankNames()
        {
            HttpResponseMessage responseMessage = await client.GetAsync(url + "/BankNames");

            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                var banks = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseData);

                return banks;
            }

            return null;
        }
    }
}