﻿using RoboData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Robo.Models
{
    public class DetailsViewModel
    {
        public string ID;
        public int ProcessStage;
        public int ProcessComplete;
        public string ProcessHeader;
        public string ProcessPercentage;
        public string ProcessMessage;
        public string ProcessBtnMessage;
        public string ProcessError;
        public bool AcceptTerms;
        public bool AcceptSignature;
        public string SignatureFullName;
        public string AcceptName;
        public string ProductName;
        public string ClientFullName;
        public string UploadLimit;
        public int LastStepCount;
        public string ClientReferenceNumber { get; set; }
        public bool BAGLContact { get; set; }
        public bool POPI { get; set; }
        public string ClientType { get; set; }
        public string Title2 { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public string IDType { get; set; }
        public Nullable<bool> TaxResident { get; set; }
        public Nullable<bool> TempResident { get; set; }
        public Nullable<bool> PostResAddress { get; set; }
        [StringLength(250, ErrorMessage = "Cannot be longer than 250 characters.")]
        public string Name { get; set; }
        [StringLength(250, ErrorMessage = "Cannot be longer than 250 characters.")]
        public string Surname { get; set; }

        [Display(Name = "ID or Passport Number", Prompt = "Please enter a Valid SA ID or Passport Number")]
        [StringLength(50, ErrorMessage = "Cannot be longer than 50 characters.")]
        public string IDPassportNumber { get; set; }

        public string BirthDate { get; set; }
        [StringLength(50, ErrorMessage = "Cannot be longer than 50 characters.")]
        public string TempResidentNumber { get; set; }
        public String PermitExpiryDate { get; set; }
        [StringLength(250, ErrorMessage = "Cannot be longer than 250 characters.")]
        public string Adress { get; set; }
        [StringLength(250, ErrorMessage = "Cannot be longer than 250 characters.")]
        public string PostalCode { get; set; }
        [StringLength(50, ErrorMessage = "Cannot be longer than 250 characters.")]
        public string City { get; set; }

        [StringLength(50, ErrorMessage = "Cannot be longer than 50 characters.")]
        public string Province { get; set; }
        [StringLength(10, ErrorMessage = "Cannot be longer than 10 charactsers.")]
        public string PostAdress { get; set; }
        [StringLength(250, ErrorMessage = "Cannot be longer than 250 characters.")]
        public string PostPostalCode { get; set; }
        public string PostCity { get; set; }
        public string PostProvince { get; set; }
        public string CountryCodeHome { get; set; }
        [StringLength(10, ErrorMessage = "Cannot be longer than 10 characters.")]
        public string PhoneHome { get; set; }
        public string CountryCodeWork { get; set; }
        [StringLength(10, ErrorMessage = "Cannot be longer than 10 characters.")]
        public string PhoneWork { get; set; }
        public string CountryCodeMobile { get; set; }
        [StringLength(10, ErrorMessage = "Cannot be longer than 10 characters.")]
        public string Mobile { get; set; }
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string EMail { get; set; }
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string SecondaryEMail { get; set; }
        [StringLength(250, ErrorMessage = "Cannot be longer than 250 characters.")]
        public string KinName { get; set; }
        [StringLength(250, ErrorMessage = "Cannot be longer than 250 characters.")]
        public string KinSurname { get; set; }
        [StringLength(50, ErrorMessage = "Cannot be longer than 50 characters.")]
        public string CountryCodeKin { get; set; }
        public string KinMobile { get; set; }
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [StringLength(500, ErrorMessage = "Cannot be longer than 500 characters.")]
        public bool FundManagersConsent { get; set; }
        public bool PostConsent { get; set; }
        public bool SMSConsent { get; set; }
        public bool PhoneConsent { get; set; }
        public bool EMailConsent { get; set; }
        public Nullable<bool> IranConfirm { get; set; }
        public Nullable<bool> SouthSudanConfirm { get; set; }
        public Nullable<bool> NorthKoreaConfirm { get; set; }
        public Nullable<bool> CubaConfirm { get; set; }
        public Nullable<bool> SyriaConfirm { get; set; }
        public Nullable<bool> CrimeaRegionConfirm { get; set; }
        public Nullable<bool> NorthSudanConfirm { get; set; }
        /// currency formatting is applied too late by the mvc framework - I leave it here as warning
        ///[DataType(DataType.Currency)]
        ///[DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> LumpSumInvestment { get; set; }
        ///[DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = true)]
        ///[DataType(DataType.Currency)]
        public Nullable<decimal> MonthlyDebitOrder { get; set; }
        public Nullable<bool> UnitInvestConfirm { get; set; }
        [StringLength(250, ErrorMessage = "Cannot be longer than 250 characters.")]
        public string ReinvestAccountHolder { get; set; }
        [StringLength(50, ErrorMessage = "Cannot be longer than 50 characters.")]
        public string ReinvestBank { get; set; }
        [StringLength(50, ErrorMessage = "Cannot be longer than 50 characters.")]
        public string ReinvestAccount { get; set; }
        [StringLength(50, ErrorMessage = "Cannot be longer than 50 characters.")]
        public string ReinvestBranchCode { get; set; }
        public string ReinvestAccountType { get; set; }
        public string IncreaseInvestment { get; set; }
        public bool? Increase0 { get; set; }
        public bool? Increase10 { get; set; }
        public bool? Increase15 { get; set; }
        public bool? Increase20 { get; set; }
        public String CashFlowPlanDate { get; set; }
        public Nullable<decimal> CashFlowPlanAmount { get; set; }
        [StringLength(250, ErrorMessage = "Cannot be longer than 250 characters.")]
        public string DebitOrderAccountHolder { get; set; }
        [StringLength(50, ErrorMessage = "Cannot be longer than 50 characters.")]
        public string DebitOrderBank { get; set; }
        [StringLength(50, ErrorMessage = "Cannot be longer than 50 characters.")]
        public string DebitOrderAccountNumber { get; set; }
        [StringLength(10, ErrorMessage = "Cannot be longer than 10 characters.")]
        public string DebitOrderBranchCode { get; set; }
        //[DataType(DataType.Currency)]
        //[DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> DebitOrderAmount { get; set; }
        public string DebitOrderAccountType { get; set; }
        public Nullable<int> DebitOrderDay { get; set; }
        [StringLength(50, ErrorMessage = "Cannot be longer than 50 characters.")]
        public Nullable<System.DateTime> DebitOrderFromMonth { get; set; }
        public string DeclarationAccept { get; set; }

        [StringLength(250, ErrorMessage = "Cannot be longer than 250 characters.")]
        public string RedemptionAccountHolder { get; set; }
        [StringLength(50, ErrorMessage = "Cannot be longer than 50 characters.")]
        public string RedemptionBank { get; set; }
        [StringLength(50, ErrorMessage = "Cannot be longer than 50 characters.")]
        public string RedemptionAccountNumber { get; set; }
        [StringLength(10, ErrorMessage = "Cannot be longer than 10 characters.")]
        public string RedemptionBranchCode { get; set; }
        [StringLength(50, ErrorMessage = "Cannot be longer than 50 characters.")]
        public string RedemptionAccountType { get; set; }
        public Nullable<bool> CashflowplanConfirm { get; set; }
        public bool IDUploaded { get; set; }
        public bool PORUploaded { get; set; }
        
        //Source of Income Bools 
        public bool income_salaryWages { get; set; }
        public bool income_commission { get; set; }
        public bool income_bonus { get; set; }
        public bool income_maintenance { get; set; }
        public bool income_pension { get; set; }
        public bool income_investments { get; set; }
        public bool income_insuranceClaim { get; set; }
        public bool income_allowance { get; set; }
        public bool income_donationGift { get; set; }
        public bool income_inheritance { get; set; }
        public bool income_socialGrant { get; set; }
        public bool income_retirementAnnuity { get; set; }
        public bool income_otherCheckBox { get; set; }
        public string income_other { get; set; }

        //Source of Funds Bools
        public bool funds_salaryWages { get; set; }
        public bool funds_commission { get; set; }
        public bool funds_bonus { get; set; }
        public bool funds_maintenance { get; set; }
        public bool funds_pension { get; set; }
        public bool funds_investments { get; set; }
        public bool funds_insuranceClaim { get; set; }
        public bool funds_allowance { get; set; }
        public bool funds_donationGift { get; set; }
        public bool funds_inheritance { get; set; }
        public bool funds_socialGrant { get; set; }
        public bool funds_retirementAnnuity { get; set; }
        public bool funds_otherCheckBox { get; set; }
        public string funds_other { get; set; }

        //Source of Wealth
        public string sourceOfWealth { get; set; }
        public List<TaxDetailsViewModel> IncomeTaxDetails { get; set; }
        public List<AdditionalPassportDetailsModel> AdditionalPassportDetails { get; set; }

        //Boolean pertaining to the Personal Information Consent within the Absa Fund Managers sectionA
        public Nullable<bool> PIS { get; set; }

        // Fields for 'Call Me Back' functionality:
        public string CallMeBack_Name { get; set; }
        public string CallMeBack_Surname { get; set; }
        public string CallMeBack_CountryCodeHome { get; set; }
        public string CallMeBack_PhoneHome { get; set; }
        public string CallMeBack_CountryCodeWork { get; set; }
        public string CallMeBack_PhoneWork { get; set; }
        public string CallMeBack_CountryCodeMobile { get; set; }
        public string CallMeBack_Mobile { get; set; }
        public string CallMeBack_EMail { get; set; }
     }

    public class TaxDetailsViewModel
    {
        public string IncomeTaxNumber { get; set; }
        public string CountryOfIssue { get; set; }

    }

    public class AdditionalPassportDetailsModel
    {
        public string PassportNumber { get; set; }
        public string CountryOfIssue { get; set; }
        public DateTime DateOfIssue { get; set; }
    }
}
